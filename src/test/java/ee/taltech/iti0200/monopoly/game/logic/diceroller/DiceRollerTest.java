package ee.taltech.iti0200.monopoly.game.logic.diceroller;

import ee.taltech.iti0200.monopoly.game.dice.Dice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DiceRollerTest {
    private IDiceRoller iDiceRoller;
    private Dice dice;

    @BeforeEach
    void setUp() {
        iDiceRoller = new DiceRoller();
        dice = new Dice();
    }

    @Test
    void rollAndReturnDice() {
        iDiceRoller.rollAndReturnDice(dice);
        assertTrue(dice.getDie1() + dice.getDie2() <= 12);
    }

    @Test
    void rollAndReturnDiceDouble() {
        iDiceRoller.rollAndReturnDice(dice);
        while (dice.getDie1() != dice.getDie2()) {
            iDiceRoller.rollAndReturnDice(dice);
        }
        assertTrue(dice.isDoubles());
    }
}