package ee.taltech.iti0200.monopoly.game.board;

import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {
    private Board board;
    private List<Tile> tileList;

    @BeforeEach
    void setUp() {
        board = new Board();
        tileList = new ArrayList<>();
        board.setTiles(tileList);
    }

    @Test
    void getTiles() {
        assertEquals(new ArrayList<>(), board.getTiles());
    }

    @Test
    void setTiles() {
        board.setTiles(null);
        assertNull(board.getTiles());
    }
}