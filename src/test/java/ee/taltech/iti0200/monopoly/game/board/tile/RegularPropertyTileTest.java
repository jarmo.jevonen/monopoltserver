package ee.taltech.iti0200.monopoly.game.board.tile;

import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RegularPropertyTileTest {
    private RegularPropertyTile regularPropertyTile;
    private Player owner;

    @BeforeEach
    void setUp() {
        regularPropertyTile = new RegularPropertyTile();
        owner = new Player();
        owner.setName("Foo Owner");

        regularPropertyTile.setTileName("FooBar");
        regularPropertyTile.setPosition(3);
        regularPropertyTile.setOwnable(true);

        regularPropertyTile.setOwner(owner);
        regularPropertyTile.setPropertyColor(PropertyColor.GREEN);
        regularPropertyTile.setPrice(10);
        regularPropertyTile.setMortgageValue(5);
        regularPropertyTile.setMortgaged(true);

        regularPropertyTile.setBaseRent(1);
        regularPropertyTile.setRentWithOneHouse(2);
        regularPropertyTile.setRentWithTwoHouses(3);
        regularPropertyTile.setRentWithThreeHouses(4);
        regularPropertyTile.setRentWithFourHouses(5);
        regularPropertyTile.setRentWithAHotel(6);
        regularPropertyTile.setBuildingsCount(3);
        regularPropertyTile.setUpgradePrice(10);
    }


    @Test
    void testToString() {
        assertEquals("3) FooBar, GREEN", regularPropertyTile.toString());
    }

    @Test
    void testToStringAllArgsConstructor() {
        regularPropertyTile = new RegularPropertyTile(1d, 2d, 3d, 4d, 5d, 6d, 0, 10);
        regularPropertyTile.setTileName("FooBar");
        regularPropertyTile.setPosition(3);
        regularPropertyTile.setPropertyColor(PropertyColor.GREEN);
        assertEquals("3) FooBar, GREEN", regularPropertyTile.toString());
    }

    @Test
    void getTileName() {
        assertEquals("FooBar", regularPropertyTile.getTileName());
    }

    @Test
    void getPosition() {
        assertEquals(3, regularPropertyTile.getPosition());
    }

    @Test
    void getOwnable() {
        assertTrue(regularPropertyTile.isOwnable());
    }

    @Test
    void getOwner() {
        assertEquals(owner, regularPropertyTile.getOwner());
    }

    @Test
    void getPropertyColor() {
        assertEquals(PropertyColor.GREEN, regularPropertyTile.getPropertyColor());
    }

    @Test
    void getPrice() {
        assertEquals(10, regularPropertyTile.getPrice());
    }

    @Test
    void getMortgageValue() {
        assertEquals(5, regularPropertyTile.getMortgageValue());
    }

    @Test
    void getMortgaged() {
        assertTrue(regularPropertyTile.isMortgaged());
    }


    @Test
    void getBaseRent() {
        assertEquals(1, regularPropertyTile.getBaseRent());
    }

    @Test
    void getRentWithOneHouse() {
        assertEquals(2, regularPropertyTile.getRentWithOneHouse());
    }

    @Test
    void getRentWithTwoHouses() {
        assertEquals(3, regularPropertyTile.getRentWithTwoHouses());
    }

    @Test
    void getRentWithThreeHouses() {
        assertEquals(4, regularPropertyTile.getRentWithThreeHouses());
    }

    @Test
    void getRentWithFourHouses() {
        assertEquals(5, regularPropertyTile.getRentWithFourHouses());
    }

    @Test
    void getRentWithAHotel() {
        assertEquals(6, regularPropertyTile.getRentWithAHotel());
    }

    @Test
    void getBuildingsCount() {
        assertEquals(3, regularPropertyTile.getBuildingsCount());
    }

    @Test
    void getUpgradePrice() {
        assertEquals(10, regularPropertyTile.getUpgradePrice());
    }

    @Test
    void setTileName() {
        regularPropertyTile.setTileName("BarFoo");
        assertEquals("BarFoo", regularPropertyTile.getTileName());
    }

    @Test
    void setPosition() {
        regularPropertyTile.setPosition(5);
        assertEquals(5, regularPropertyTile.getPosition());
    }

    @Test
    void setOwnable() {
        regularPropertyTile.setOwnable(false);
        assertFalse(regularPropertyTile.isOwnable());
    }

    @Test
    void setBaseRent() {
        regularPropertyTile.setBaseRent(9);
        assertEquals(9, regularPropertyTile.getBaseRent());
    }

    @Test
    void setRentWithOneHouse() {
        regularPropertyTile.setRentWithOneHouse(10);
        assertEquals(10, regularPropertyTile.getRentWithOneHouse());
    }

    @Test
    void setRentWithTwoHouses() {
        regularPropertyTile.setRentWithTwoHouses(11);
        assertEquals(11, regularPropertyTile.getRentWithTwoHouses());
    }

    @Test
    void setRentWithThreeHouses() {
        regularPropertyTile.setRentWithThreeHouses(12);
        assertEquals(12, regularPropertyTile.getRentWithThreeHouses());
    }

    @Test
    void setRentWithFourHouses() {
        regularPropertyTile.setRentWithFourHouses(13);
        assertEquals(13, regularPropertyTile.getRentWithFourHouses());
    }

    @Test
    void setRentWithAHotel() {
        regularPropertyTile.setRentWithAHotel(14);
        assertEquals(14, regularPropertyTile.getRentWithAHotel());
    }

    @Test
    void setBuildingsCount() {
        regularPropertyTile.setBuildingsCount(1);
        assertEquals(1, regularPropertyTile.getBuildingsCount());
    }

    @Test
    void setUpgradePrice() {
        regularPropertyTile.setUpgradePrice(2);
        assertEquals(2, regularPropertyTile.getUpgradePrice());
    }

    @Test
    void setOwner() {
        Player tempPlayer = new Player();
        tempPlayer.setName("Bar Owner");
        regularPropertyTile.setOwner(tempPlayer);
        assertEquals(tempPlayer, regularPropertyTile.getOwner());
    }

    @Test
    void setPropertyColor() {
        regularPropertyTile.setPropertyColor(PropertyColor.MAGENTA);
        assertEquals(PropertyColor.MAGENTA, regularPropertyTile.getPropertyColor());
    }

    @Test
    void setPrice() {
        regularPropertyTile.setPrice(8);
        assertEquals(8, regularPropertyTile.getPrice());
    }

    @Test
    void setMortgageValue() {
        regularPropertyTile.setMortgageValue(4);
        assertEquals(4, regularPropertyTile.getMortgageValue());
    }

    @Test
    void setMortgaged() {
        regularPropertyTile.setMortgaged(false);
        assertFalse(regularPropertyTile.isMortgaged());
    }
}