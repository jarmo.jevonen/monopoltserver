package ee.taltech.iti0200.monopoly.game.board.tile;

import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PropertyTileGroupTest {
    private PropertyTileGroup propertyTileGroup;
    private UtilityPropertyTile utilityPropertyTile;
    private Player owner;

    @BeforeEach
    void setUp() {
        propertyTileGroup = new PropertyTileGroup();
        utilityPropertyTile = new UtilityPropertyTile();

        owner = new Player();
        owner.setName("Foo Owner");

        utilityPropertyTile.setTileName("FooBar");
        utilityPropertyTile.setPosition(3);
        utilityPropertyTile.setOwnable(true);

        propertyTileGroup.setPropertyTiles(new ArrayList<>(List.of(utilityPropertyTile)));
        propertyTileGroup.setPropertyColor(PropertyColor.GREEN);
        propertyTileGroup.setHousePrice(50);

        utilityPropertyTile.setOwner(owner);
        utilityPropertyTile.setPropertyColor(PropertyColor.GREEN);
        utilityPropertyTile.setPrice(10);
        utilityPropertyTile.setMortgageValue(5);
        utilityPropertyTile.setMortgaged(true);

        utilityPropertyTile.setCoefficientWithOneUtility(2);
        utilityPropertyTile.setCoefficientWithBothUtilities(4);
        utilityPropertyTile.setPropertyTileGroup(propertyTileGroup);

    }

    @Test
    void getPropertyColor() {
        assertEquals(PropertyColor.GREEN, propertyTileGroup.getPropertyColor());
    }

    @Test
    void getPropertyTiles() {
        assertEquals(List.of(utilityPropertyTile), propertyTileGroup.getPropertyTiles());
    }

    @Test
    void getHousePrice() {
        assertEquals(50, propertyTileGroup.getHousePrice());
    }

    @Test
    void setPropertyColor() {
        propertyTileGroup.setPropertyColor(PropertyColor.BROWN);
        assertEquals(PropertyColor.BROWN, propertyTileGroup.getPropertyColor());
    }

    @Test
    void setPropertyTiles() {
        propertyTileGroup.setPropertyTiles(new ArrayList<>());
        assertEquals(new ArrayList<>(), propertyTileGroup.getPropertyTiles());
    }

    @Test
    void setHousePrice() {
        propertyTileGroup.setHousePrice(20);
        assertEquals(20, propertyTileGroup.getHousePrice());
    }
}