package ee.taltech.iti0200.monopoly.game.logic.game;

import ee.taltech.iti0200.monopoly.game.board.tile.PropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardAction;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.jail.Jail;
import ee.taltech.iti0200.monopoly.game.logic.gameenigne.GameNetworkingServerLogic;
import ee.taltech.iti0200.monopoly.game.logic.initializer.PlayersInitializer;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.serv.MpServer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CpuPlayerLogicTest {
    private CpuPlayerLogic cpuPlayerLogic;
    private static int port = 24200;
    private GameNetworkingServerLogic gameNetworkingServerLogic;
    private Game game;
    private Player player1;

    @BeforeEach
    void setUp() {
        port++;
        gameNetworkingServerLogic = new GameNetworkingServerLogic(new MpServer(port), "FooLobbyId");
        game = new Game(gameNetworkingServerLogic);

        player1 = new Player();
        player1.setName("Alice");
        player1.setConnectionId(1234);
        player1.setPlayerId("dcddc87f-f7b7-44ba-87b2-a4075809232f");
        PlayersInitializer.initializeAndReturnPlayer(player1);
        player1.setPlayerId("abcde");

        List<Player> players = new ArrayList<>(List.of(
                player1
        ));
        game.initializeGame(players);
        gameNetworkingServerLogic.setPlayers(players);
        game.initializeGame(players);
        cpuPlayerLogic = game.getCpuPlayerLogic();
    }

    @Test
    void doAIAction() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        PropertyTile propertyTile9 = (PropertyTile) game.getBoard().getTiles().get(9);
        propertyTile6.setOwner(player1);
        propertyTile8.setOwner(player1);
        propertyTile9.setOwner(player1);
        player1.setOwnedPropertyTiles(List.of(propertyTile6, propertyTile8, propertyTile9));
        player1.setPosition(8);
        cpuPlayerLogic.doAIAction(player1);
    }

    @Test
    void doAIActionUpgrade() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        PropertyTile propertyTile9 = (PropertyTile) game.getBoard().getTiles().get(9);
        game.setCanCurrentTurnPlayerRollTheDice(false);
        propertyTile6.setOwner(player1);
        propertyTile8.setOwner(player1);
        propertyTile9.setOwner(player1);
        player1.setOwnedPropertyTiles(List.of(propertyTile6, propertyTile8, propertyTile9));
        player1.setPosition(8);
        cpuPlayerLogic.doAIAction(player1);
    }

    @Test
    void doAIActionUpgradeAlreadyMaxed() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        PropertyTile propertyTile9 = (PropertyTile) game.getBoard().getTiles().get(9);
        game.setCanCurrentTurnPlayerRollTheDice(false);
        propertyTile6.setOwner(player1);
        propertyTile8.setOwner(player1);
        propertyTile9.setOwner(player1);
        ((RegularPropertyTile) propertyTile6).setBuildingsCount(5);
        ((RegularPropertyTile) propertyTile8).setBuildingsCount(5);
        ((RegularPropertyTile) propertyTile9).setBuildingsCount(5);
        player1.setOwnedPropertyTiles(List.of(propertyTile6, propertyTile8, propertyTile9));
        player1.setPosition(8);
        cpuPlayerLogic.doAIAction(player1);
    }

    @Test
    void doAIActionPurchaseNotEnoughMoney() {
        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        propertyTile8.setPrice(1000);
        player1.setPosition(8);
        cpuPlayerLogic.doAIAction(player1);
    }

    @Test
    void doAIActionPurchaseHasEnoughMoney() {
        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        propertyTile8.setPrice(10);
        player1.setPosition(8);
        cpuPlayerLogic.doAIAction(player1);
    }

    @Test
    void doAIActionkeepUseCard() {
        player1.setPosition(10);
        player1.setInJail(true);
        Jail jail = new Jail();
        Map<String, Integer> jailMap = new HashMap<>(Map.of("abcde", 2));
        jail.setPlayersInJailMap(jailMap);
        game.setJail(jail);
        Card card = new Card();
        card.setAction(CardAction.GET_OUT_OF_JAIL);
        player1.setOwnedCards(List.of(card));
        cpuPlayerLogic.doAIAction(player1);
    }

    @Test
    void doAIActionKeepRollToGetOutOfJail() {
        player1.setPosition(10);
        player1.setInJail(true);
        Jail jail = new Jail();
        Map<String, Integer> jailMap = new HashMap<>(Map.of("abcde", 2));
        jail.setPlayersInJailMap(jailMap);
        game.setJail(jail);
        cpuPlayerLogic.doAIAction(player1);
    }

    @Test
    void doAIActionKeepCardSwitch() {
        Card card = new Card();
        card.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(card);
        cpuPlayerLogic.doAIAction(player1);
    }

    @Test
    void doAIActionUpgradeDecision() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        PropertyTile propertyTile9 = (PropertyTile) game.getBoard().getTiles().get(9);
        game.setCanCurrentTurnPlayerRollTheDice(false);
        propertyTile6.setOwner(player1);
        propertyTile8.setOwner(player1);
        propertyTile9.setOwner(player1);
        ((RegularPropertyTile) propertyTile6).setBuildingsCount(5);
        ((RegularPropertyTile) propertyTile8).setBuildingsCount(0);
        ((RegularPropertyTile) propertyTile9).setBuildingsCount(5);
        player1.setOwnedPropertyTiles(List.of(propertyTile6, propertyTile8, propertyTile9));
        player1.setPosition(8);

        Player player2 = new Player();
        player2.setName("Bob");
        player2.setConnectionId(6789);
        PlayersInitializer.initializeAndReturnPlayer(player2);
        player2.setPlayerId("aaaaa");
        player2.setOwnedPropertyTiles(new ArrayList<>());
        game.getPlayers().add(player2);
        player2.setPosition(3);
        cpuPlayerLogic.doAIAction(player1);
    }

    @Test
    void doAIActionRoll() {
        game.setCurrentTurnPlayer(player1);
        game.setCanCurrentTurnPlayerRollTheDice(true);
        cpuPlayerLogic.doAIAction(player1);
    }



    @Test
    void handleGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn() {
        player1.setPosition(10);
        player1.setInJail(true);
        Jail jail = new Jail();
        Map<String, Integer> jailMap = new HashMap<>(Map.of("abcde", 2));
        jail.setPlayersInJailMap(jailMap);
        game.setJail(jail);
        cpuPlayerLogic.handleGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(player1, true, true, true);
    }

    @Test
    void handleCardSwitchACardOrPay() {
        Card card = new Card();
        card.setType(CardType.CHANCE);
        card.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(card);
        game.setCurrentTurnPlayer(player1);
        cpuPlayerLogic.handleCardSwitchACardOrPay(player1, card.getType(), CardType.COMMUNITY_CHEST, 10);
    }

    @Test
    void handlePossibleBankruptcy() {
        player1.setBalance(5);
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        PropertyTile propertyTile9 = (PropertyTile) game.getBoard().getTiles().get(9);
        propertyTile6.setOwner(player1);
        propertyTile8.setOwner(player1);
        propertyTile9.setOwner(player1);
        player1.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile6, propertyTile8, propertyTile9)));
        cpuPlayerLogic.handlePossibleBankruptcy(player1);
    }

    @Test
    void handlePossibleBankruptcyNoMoney() {
        player1.setBalance(-1000);
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        PropertyTile propertyTile9 = (PropertyTile) game.getBoard().getTiles().get(9);
        propertyTile6.setOwner(player1);
        propertyTile8.setOwner(player1);
        propertyTile9.setOwner(player1);
        player1.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile6, propertyTile8, propertyTile9)));
        cpuPlayerLogic.handlePossibleBankruptcy(player1);
    }

    @Test
    void otherPlayerWithinRangeOfTileTest1() {
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(8);
        player1.setPosition(1);
        assertTrue(cpuPlayerLogic.otherPlayerWithinRangeOfTile(propertyTile, 12));
    }

    @Test
    void otherPlayerWithinRangeOfTileTest2() {
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(9);
        player1.setPosition(0);
        assertTrue(cpuPlayerLogic.otherPlayerWithinRangeOfTile(propertyTile, 12));
    }

    @Test
    void otherPlayerWithinRangeOfTileTest3() {
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(9);
        player1.setPosition(39);
        assertTrue(cpuPlayerLogic.otherPlayerWithinRangeOfTile(propertyTile, 12));
    }

    @Test
    void otherPlayerWithinRangeOfTileTest4() {
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(6);
        player1.setPosition(35);
        assertTrue(cpuPlayerLogic.otherPlayerWithinRangeOfTile(propertyTile, 12));
    }

    @Test
    void otherPlayerWithinRangeOfTileTest5() {
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(14);
        player1.setPosition(2);
        assertTrue(cpuPlayerLogic.otherPlayerWithinRangeOfTile(propertyTile, 12));
    }

    @Test
    void otherPlayerWithinRangeOfTileTest6() {
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(29);
        player1.setPosition(31);
        assertFalse(cpuPlayerLogic.otherPlayerWithinRangeOfTile(propertyTile, 12));
    }

    @Test
    void otherPlayerWithinRangeOfTileTest7() {
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(14);
        player1.setPosition(1);
        assertFalse(cpuPlayerLogic.otherPlayerWithinRangeOfTile(propertyTile, 12));
    }

    @Test
    void otherPlayerWithinRangeOfTileTest8() {
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(1);
        player1.setPosition(3);
        assertFalse(cpuPlayerLogic.otherPlayerWithinRangeOfTile(propertyTile, 12));
    }

    @Test
    void getGame() {
        assertEquals(game, cpuPlayerLogic.getGame());
    }

    @Test
    void getCpuPlayerActions() {
        List<CpuPlayerAction> actions = cpuPlayerLogic.getCpuPlayerActions();
        assertEquals(actions, cpuPlayerLogic.getCpuPlayerActions());
    }

    @Test
    void setGame() {
        cpuPlayerLogic.setGame(null);
        assertNull(cpuPlayerLogic.getGame());
    }

    @Test
    void setCpuPlayerActions() {
        cpuPlayerLogic.setCpuPlayerActions(new ArrayList<>());
        assertEquals(new ArrayList<>(), cpuPlayerLogic.getCpuPlayerActions());
    }

    @Test
    void sendPropertyOfferToBuyPlayerOwnsOverHalfOfTheSameGroup() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        game.setCurrentTurnPlayer(player1);
        Player player2 = new Player();
        player2.setName("Bob");
        player2.setConnectionId(6789);
        PlayersInitializer.initializeAndReturnPlayer(player2);
        player2.setPlayerId("aaaaa");
        player2.setOwnedPropertyTiles(new ArrayList<>());
        game.getPlayers().add(player2);

        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        propertyTile8.setOwner(player1);
        PropertyTile propertyTile9 = (PropertyTile) game.getBoard().getTiles().get(9);
        propertyTile9.setOwner(player1);
        player1.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile8, propertyTile9)));

        cpuPlayerLogic.sendPropertyOfferToBuy(player1, propertyTile6);
    }

    @Test
    void sendPropertyOfferToBuyOtherPlayerOwnsOverHalfOfTheSameGroup() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        game.setCurrentTurnPlayer(player1);
        Player player2 = new Player();
        player2.setName("Bob");
        player2.setConnectionId(6789);
        PlayersInitializer.initializeAndReturnPlayer(player2);
        player2.setPlayerId("aaaaa");
        player2.setOwnedPropertyTiles(new ArrayList<>());
        game.getPlayers().add(player2);

        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        propertyTile8.setOwner(player2);
        PropertyTile propertyTile9 = (PropertyTile) game.getBoard().getTiles().get(9);
        propertyTile9.setOwner(player2);
        player2.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile8, propertyTile9)));

        cpuPlayerLogic.sendPropertyOfferToBuy(player1, propertyTile6);
    }

    @Test
    void sendPropertyOfferToBuyTwoOtherPlayersOwnOverHalfOfTheSameGroup() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        game.setCurrentTurnPlayer(player1);
        Player player2 = new Player();
        player2.setName("Bob");
        player2.setConnectionId(6789);
        PlayersInitializer.initializeAndReturnPlayer(player2);
        player2.setPlayerId("aaaaa");
        player2.setOwnedPropertyTiles(new ArrayList<>());
        game.getPlayers().add(player2);

        Player player3 = new Player();
        player3.setName("John");
        player3.setConnectionId(6789);
        PlayersInitializer.initializeAndReturnPlayer(player3);
        player3.setPlayerId("qwerty");
        player3.setOwnedPropertyTiles(new ArrayList<>());
        game.getPlayers().add(player3);

        PropertyTile propertyTile8 = (PropertyTile) game.getBoard().getTiles().get(8);
        propertyTile8.setOwner(player2);
        PropertyTile propertyTile9 = (PropertyTile) game.getBoard().getTiles().get(9);
        propertyTile9.setOwner(player3);
        player2.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile8)));
        player3.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile9)));

        cpuPlayerLogic.sendPropertyOfferToBuy(player1, propertyTile6);
    }

    @Test
    void sendPropertyOfferToBuyBalanceIsLessThanHalfOfThePrice() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        game.setCurrentTurnPlayer(player1);
        propertyTile6.setPrice(1000);
        cpuPlayerLogic.sendPropertyOfferToBuy(player1, propertyTile6);
    }

    @Test
    void sendPropertyOfferToBuyPlayerPosMoreThan30() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        game.setCurrentTurnPlayer(player1);
        player1.setPosition(35);
        cpuPlayerLogic.sendPropertyOfferToBuy(player1, propertyTile6);
    }

    @Test
    void sendPropertyOfferToBuySomeoneHasATileWithHighRent() {
        PropertyTile propertyTile6 = (PropertyTile) game.getBoard().getTiles().get(6);
        game.setCurrentTurnPlayer(player1);
        player1.setPosition(6);

        Player player2 = new Player();
        player2.setName("Bob");
        player2.setConnectionId(6789);
        PlayersInitializer.initializeAndReturnPlayer(player2);
        player2.setPlayerId("aaaaa");
        player2.setOwnedPropertyTiles(new ArrayList<>());
        game.getPlayers().add(player2);

        PropertyTile propertyTile37 = (PropertyTile) game.getBoard().getTiles().get(37);
        PropertyTile propertyTile39 = (PropertyTile) game.getBoard().getTiles().get(39);
        propertyTile37.setOwner(player2);
        propertyTile39.setOwner(player2);
        player2.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile37, propertyTile39)));
        ((RegularPropertyTile) propertyTile37).setBuildingsCount(5);
        ((RegularPropertyTile) propertyTile39).setBuildingsCount(5);

        cpuPlayerLogic.sendPropertyOfferToBuy(player1, propertyTile6);
    }
}