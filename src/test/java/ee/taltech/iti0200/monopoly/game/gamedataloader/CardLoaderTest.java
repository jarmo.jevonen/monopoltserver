package ee.taltech.iti0200.monopoly.game.gamedataloader;

import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CardLoaderTest {

    @Test
    void loadCardsFromFilesChance() {
        List<Card> cards = CardLoader.loadCardsFromFiles(CardType.CHANCE);
        assertEquals(5, cards.get(5).getCardIndex());
    }

    @Test
    void loadCardsFromFilesCommunityChest() {
        List<Card> cards = CardLoader.loadCardsFromFiles(CardType.COMMUNITY_CHEST);
        assertEquals(5, cards.get(5).getCardIndex());
    }

    @Test
    void loadCardsFromFilesUnknownType() {
        assertThrows(RuntimeException.class, () -> CardLoader.loadCardsFromFiles(null));
    }

    @Test
    void getCardsStringFromFilesChance() {
        String cardsString = CardLoader.getCardsStringFromFiles(CardType.CHANCE);
        assertEquals("{\"type\":\"CHANCE", cardsString.substring(0, 15));
    }

    @Test
    void getCardsStringFromFilesCommunityChest() {
        String cardsString = CardLoader.getCardsStringFromFiles(CardType.COMMUNITY_CHEST);
        assertEquals("{\"type\":\"COMMUN", cardsString.substring(0, 15));
    }

    @Test
    void getCardsStringFromFilesUnknownType() {
        assertThrows(RuntimeException.class, () -> CardLoader.loadCardsFromFiles(null));
    }

}