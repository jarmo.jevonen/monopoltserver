package ee.taltech.iti0200.monopoly.game.logic.initializer;

import ee.taltech.iti0200.monopoly.game.card.Deck;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardDeckInitializerTest {
    private CardDeckInitializer cardDeckInitializer;

    @BeforeEach
    void setUp() {
        cardDeckInitializer = new CardDeckInitializer();
    }

    @Test
    void initializeAndReturnDeck() {
        assertEquals(16, CardDeckInitializer.initializeAndReturnDeck().getChanceCards().size());
        assertEquals(16, CardDeckInitializer.initializeAndReturnDeck().getCommunityChestCards().size());
        assertTrue(CardDeckInitializer.initializeAndReturnDeck() instanceof Deck);
    }
}