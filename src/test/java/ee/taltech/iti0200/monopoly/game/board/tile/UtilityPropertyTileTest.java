package ee.taltech.iti0200.monopoly.game.board.tile;

import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class UtilityPropertyTileTest {
    private UtilityPropertyTile utilityPropertyTile;
    private Player owner;
    private PropertyTileGroup propertyTileGroup;

    @BeforeEach
    void setUp() {
        utilityPropertyTile = new UtilityPropertyTile();
        propertyTileGroup = new PropertyTileGroup();

        owner = new Player();
        owner.setName("Foo Owner");

        utilityPropertyTile.setTileName("FooBar");
        utilityPropertyTile.setPosition(3);
        utilityPropertyTile.setOwnable(true);

        propertyTileGroup.setPropertyTiles(new ArrayList<>());
        propertyTileGroup.setPropertyColor(PropertyColor.MAGENTA);
        propertyTileGroup.setHousePrice(50);

        utilityPropertyTile.setOwner(owner);
        utilityPropertyTile.setPropertyColor(PropertyColor.GREEN);
        utilityPropertyTile.setPrice(10);
        utilityPropertyTile.setMortgageValue(5);
        utilityPropertyTile.setMortgaged(true);

        utilityPropertyTile.setCoefficientWithOneUtility(2);
        utilityPropertyTile.setCoefficientWithBothUtilities(4);
        utilityPropertyTile.setPropertyTileGroup(propertyTileGroup);
    }

    @Test
    void testToString() {
        assertEquals("3) FooBar, GREEN", utilityPropertyTile.toString());
    }

    @Test
    void getPropertyTileGroup() {
        assertEquals(propertyTileGroup, utilityPropertyTile.getPropertyTileGroup());
    }

    @Test
    void getCoefficientWithOneUtility() {
        assertEquals(2, utilityPropertyTile.getCoefficientWithOneUtility());
    }

    @Test
    void getCoefficientWithBothUtilities() {
        assertEquals(4, utilityPropertyTile.getCoefficientWithBothUtilities());
    }

    @Test
    void setPropertyTileGroup() {
        utilityPropertyTile.setPropertyTileGroup(null);
        assertNull(utilityPropertyTile.getPropertyTileGroup());
    }

    @Test
    void setCoefficientWithOneUtility() {
        utilityPropertyTile.setCoefficientWithOneUtility(3);
        assertEquals(3, utilityPropertyTile.getCoefficientWithOneUtility());
    }

    @Test
    void setCoefficientWithBothUtilities() {
        utilityPropertyTile.setCoefficientWithBothUtilities(5);
        assertEquals(5, utilityPropertyTile.getCoefficientWithBothUtilities());
    }
}