package ee.taltech.iti0200.monopoly.game.board.tile;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SpecialTileTest {
    private SpecialTile specialTile;

    @BeforeEach
    void setUp() {
        specialTile = new SpecialTile();
        specialTile.setTileName("FooBar");
        specialTile.setPosition(3);
        specialTile.setOwnable(true);

        specialTile.setSpecialTileType(SpecialTileType.FREE_PARKING);
    }

    @Test
    void testToString() {
        assertEquals("3) FooBar, FREE_PARKING", specialTile.toString());
    }

    @Test
    void getSpecialTileType() {
        assertEquals(SpecialTileType.FREE_PARKING, specialTile.getSpecialTileType());
    }

    @Test
    void setSpecialTileType() {
        specialTile.setSpecialTileType(SpecialTileType.GO_TO_JAIL);
        assertEquals(SpecialTileType.GO_TO_JAIL, specialTile.getSpecialTileType());
    }

    @Test
    void AllArgsConstructor() {
        specialTile = new SpecialTile(SpecialTileType.GO);
        specialTile.setTileName("Start");
        specialTile.setPosition(0);
        specialTile.setOwnable(false);
        assertEquals("0) Start, GO", specialTile.toString());
    }
}