package ee.taltech.iti0200.monopoly.game.logic.game;

import ee.taltech.iti0200.monopoly.game.board.tile.*;
import ee.taltech.iti0200.monopoly.game.dice.Dice;
import ee.taltech.iti0200.monopoly.game.logic.initializer.PlayersInitializer;
import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PropertyServiceTest {
    private PropertyService propertyService;
    private PropertyTile propertyTile1, propertyTile2, propertyTile3, propertyTile4, propertyTile5, propertyTile6;
    private Player player1, player2;
    private Game game;
    private PropertyTileGroup propertyTileGroup;
    private Dice dice;
    private List<PropertyTile> propertyTiles;

    @BeforeEach
    void setUp() {
        player1 = new Player();
        player2 = new Player();
        List<Player> players = new ArrayList<>();
        players.add(PlayersInitializer.initializeAndReturnPlayer(player1));
        players.add(PlayersInitializer.initializeAndReturnPlayer(player2));
        game = new Game(null);
        propertyService = new PropertyService(game);
        propertyTileGroup = new PropertyTileGroup();
        propertyTile1 = new RegularPropertyTile(1d, 2d, 3d, 4d, 5d, 6d, 0, 50d);
        propertyTile2 = new RegularPropertyTile(1d, 2d, 3d, 4d, 5d, 6d, 0, 50d);
        propertyTile3 = new RegularPropertyTile(1d, 2d, 3d, 4d, 5d, 6d, 0, 50d);
        propertyTile4 = new RailroadPropertyTile(propertyTileGroup, 1d, 2d, 3d, 4d);
        propertyTile5 = new UtilityPropertyTile(propertyTileGroup, 1, 2);
        propertyTile6 = new UtilityPropertyTile(propertyTileGroup, 1, 2);
        dice = new Dice();
        dice.setDie1(1);
        dice.setDie2(2);
        dice.setDoubles(false);
        game.initializeGame(players);
        game.setCurrentTurnPlayer(player1);
        propertyTiles = new ArrayList<>();
        propertyTiles.add(propertyTile1);
        propertyTile1.setOwner(player1);
        propertyTile1.setPropertyColor(PropertyColor.BROWN);
        propertyTile2.setPropertyColor(PropertyColor.BROWN);
        propertyTile3.setPropertyColor(PropertyColor.BROWN);
        propertyTile4.setPropertyColor(PropertyColor.RAILROAD);
        propertyTile5.setPropertyColor(PropertyColor.UTILITY);
        propertyTile6.setPropertyColor(PropertyColor.UTILITY);
        player1.setOwnedPropertyTiles(propertyTiles);
    }

    @Test
    void purchaseTile() {
        propertyService.purchaseTile(player1, propertyTile4);
        assertEquals(2, player1.getOwnedPropertyTiles().size());
    }

    @Test
    void getRentAmountForPropertyRegularPropertyBaseRent() {
        assertEquals(1d, propertyService.getRentAmountForProperty(propertyTile1, dice));
    }

    @Test
    void getRentAmountForPropertyRegularPropertyWithBuildings1() {
        ((RegularPropertyTile) propertyTile1).setBuildingsCount(1);
        assertEquals(2d, propertyService.getRentAmountForProperty(propertyTile1, dice));
    }

    @Test
    void getRentAmountForPropertyRegularPropertyWithBuildings2() {
        ((RegularPropertyTile) propertyTile1).setBuildingsCount(2);
        assertEquals(3d, propertyService.getRentAmountForProperty(propertyTile1, dice));
    }

    @Test
    void getRentAmountForPropertyRegularPropertyWithBuildings3() {
        ((RegularPropertyTile) propertyTile1).setBuildingsCount(3);
        assertEquals(4d, propertyService.getRentAmountForProperty(propertyTile1, dice));
    }

    @Test
    void getRentAmountForPropertyRegularPropertyWithBuildings4() {
        ((RegularPropertyTile) propertyTile1).setBuildingsCount(4);
        assertEquals(5d, propertyService.getRentAmountForProperty(propertyTile1, dice));
    }

    @Test
    void getRentAmountForPropertyRegularPropertyWithBuildings5() {
        ((RegularPropertyTile) propertyTile1).setBuildingsCount(5);
        assertEquals(6d, propertyService.getRentAmountForProperty(propertyTile1, dice));
    }

    @Test
    void getRentAmountForPropertyRegularPropertyWithBuildingsException() {
        ((RegularPropertyTile) propertyTile1).setBuildingsCount(10);
        assertThrows(RuntimeException.class, () -> propertyService.getRentAmountForProperty(propertyTile1, dice));
    }

    @Test
    void getRentAmountForPropertyRegularPropertyAllThreeColorsOwned() {
        propertyTiles.add(propertyTile2);
        propertyTiles.add(propertyTile3);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertEquals(2d, propertyService.getRentAmountForProperty(propertyTile1, dice));
    }

    @Test
    void getRentAmountForPropertyUtilityCard() {
        propertyTiles.add(propertyTile5);
        propertyTile5.setOwner(player1);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertEquals(1.2d, propertyService.getRentAmountForProperty(propertyTile5, dice), 0.001);
    }

    @Test
    void getRentAmountForPropertyUtilityCardOwnsTwo() {
        propertyTiles.add(propertyTile5);
        propertyTiles.add(propertyTile6);
        propertyTile5.setOwner(player1);
        propertyTile6.setOwner(player1);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertEquals(3d, propertyService.getRentAmountForProperty(propertyTile5, dice), 0.001);
    }

    @Test
    void getRentAmountForPropertyRailRoadCardWith1Station() {
        propertyTiles.add(propertyTile4);
        propertyTile4.setOwner(player1);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertEquals(1d, propertyService.getRentAmountForProperty(propertyTile4, dice));
    }

    @Test
    void getRentAmountForPropertyRailRoadCardWith2Stations() {
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTile4.setOwner(player1);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertEquals(2d, propertyService.getRentAmountForProperty(propertyTile4, dice));
    }

    @Test
    void getRentAmountForPropertyRailRoadCardWith3Stations() {
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTile4.setOwner(player1);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertEquals(3d, propertyService.getRentAmountForProperty(propertyTile4, dice));
    }

    @Test
    void getRentAmountForPropertyRailRoadCardWith4Stations() {
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTile4.setOwner(player1);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertEquals(4d, propertyService.getRentAmountForProperty(propertyTile4, dice));
    }

    @Test
    void getRentAmountForPropertyRailRoadCardException() {
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTiles.add(propertyTile4);
        propertyTile4.setOwner(player1);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertThrows(RuntimeException.class, () -> propertyService.getRentAmountForProperty(propertyTile4, dice));
    }

    @Test
    void getMaintenanceCostForProperty() {
        propertyTile2.setOwner(player1);
        ((RegularPropertyTile) propertyTile2).setBuildingsCount(5);
        ((RegularPropertyTile) propertyTile1).setBuildingsCount(1);
        propertyTiles.add(propertyTile2);
        propertyTiles.add(propertyTile3);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertEquals(3d , propertyService.getMaintenanceCostForProperty(1d, 2d));
    }

    @Test
    void playerHasAllSameColorProperties() {
        propertyTiles.add(propertyTile2);
        propertyTiles.add(propertyTile3);
        player1.setOwnedPropertyTiles(propertyTiles);
        assertTrue(propertyService.playerHasAllSameColorProperties(player1, propertyTile1));
    }

    @Test
    void calculateHighestRentOnBoard() {
        Tile tile = propertyTile1;
        List<Tile> tiles = new ArrayList<>();
        tiles.add(tile);
        game.getBoard().setTiles(tiles);
        assertEquals(1d, propertyService.calculateHighestRentOnBoard());
    }
}