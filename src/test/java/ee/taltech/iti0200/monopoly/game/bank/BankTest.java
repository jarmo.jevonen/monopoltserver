package ee.taltech.iti0200.monopoly.game.bank;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BankTest {
    private Bank bank;

    @BeforeEach
    void setUp() {
        this.bank = new Bank(1, 2, 3, 4);
    }

    @Test
    void getTotalHousesCount() {
        assertEquals(1, bank.getTotalHousesCount());
    }

    @Test
    void getTotalHotelsCount() {
        assertEquals(2, bank.getTotalHotelsCount());
    }

    @Test
    void getAvailableHousesCount() {
        assertEquals(3, bank.getAvailableHousesCount());
    }

    @Test
    void getAvailableHotelsCount() {
        assertEquals(4, bank.getAvailableHotelsCount());
    }

    @Test
    void setTotalHousesCount() {
        bank.setTotalHousesCount(9);
        assertEquals(9, bank.getTotalHousesCount());
    }

    @Test
    void setTotalHotelsCount() {
        bank.setTotalHotelsCount(8);
        assertEquals(8, bank.getTotalHotelsCount());
    }

    @Test
    void setAvailableHousesCount() {
        bank.setAvailableHousesCount(7);
        assertEquals(7, bank.getAvailableHousesCount());
    }

    @Test
    void setAvailableHotelsCount() {
        bank.setAvailableHotelsCount(6);
        assertEquals(6, bank.getAvailableHotelsCount());
    }
}