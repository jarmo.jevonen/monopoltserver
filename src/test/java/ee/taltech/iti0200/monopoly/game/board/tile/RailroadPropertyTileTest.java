package ee.taltech.iti0200.monopoly.game.board.tile;

import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class RailroadPropertyTileTest {
    private RailroadPropertyTile railroadPropertyTile;
    private Player owner;
    private PropertyTileGroup propertyTileGroup;

    @BeforeEach
    void setUp() {
        railroadPropertyTile = new RailroadPropertyTile();
        owner = new Player();
        owner.setName("Foo Owner");
        propertyTileGroup = new PropertyTileGroup();
        propertyTileGroup.setPropertyTiles(new ArrayList<>());
        propertyTileGroup.setPropertyColor(PropertyColor.MAGENTA);
        propertyTileGroup.setHousePrice(50);

        railroadPropertyTile.setTileName("FooBar");
        railroadPropertyTile.setPosition(3);
        railroadPropertyTile.setOwnable(true);

        railroadPropertyTile.setOwner(owner);
        railroadPropertyTile.setPropertyColor(PropertyColor.GREEN);
        railroadPropertyTile.setPrice(10);
        railroadPropertyTile.setMortgageValue(5);
        railroadPropertyTile.setMortgaged(true);
        railroadPropertyTile.setPropertyTileGroup(propertyTileGroup);

        railroadPropertyTile.setRentWithOneStation(1);
        railroadPropertyTile.setRentWithTwoStations(2);
        railroadPropertyTile.setRentWithThreeStations(3);
        railroadPropertyTile.setRentWithFourStations(4);
    }

    @Test
    void testToString() {
        assertEquals("3) FooBar, GREEN", railroadPropertyTile.toString());
    }

    @Test
    void getPropertyTileGroup() {
        assertEquals(propertyTileGroup, railroadPropertyTile.getPropertyTileGroup());
    }

    @Test
    void getRentWithOneStation() {
        assertEquals(1, railroadPropertyTile.getRentWithOneStation());
    }

    @Test
    void getRentWithTwoStations() {
        assertEquals(2, railroadPropertyTile.getRentWithTwoStations());
    }

    @Test
    void getRentWithThreeStations() {
        assertEquals(3, railroadPropertyTile.getRentWithThreeStations());
    }

    @Test
    void getRentWithFourStations() {
        assertEquals(4, railroadPropertyTile.getRentWithFourStations());
    }

    @Test
    void setPropertyTileGroup() {
        railroadPropertyTile.setPropertyTileGroup(null);
        assertNull(railroadPropertyTile.getPropertyTileGroup());
    }

    @Test
    void setRentWithOneStation() {
        railroadPropertyTile.setRentWithOneStation(9);
        assertEquals(9, railroadPropertyTile.getRentWithOneStation());
    }

    @Test
    void setRentWithTwoStations() {
        railroadPropertyTile.setRentWithTwoStations(8);
        assertEquals(8, railroadPropertyTile.getRentWithTwoStations());
    }

    @Test
    void setRentWithThreeStations() {
        railroadPropertyTile.setRentWithThreeStations(7);
        assertEquals(7, railroadPropertyTile.getRentWithThreeStations());
    }

    @Test
    void setRentWithFourStations() {
        railroadPropertyTile.setRentWithFourStations(6);
        assertEquals(6, railroadPropertyTile.getRentWithFourStations());
    }
}