package ee.taltech.iti0200.monopoly.game.gamedataloader;

import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TileLoaderTest {

    @Test
    void loadTilesFromFiles() {
        List<Tile> tiles = TileLoader.loadTilesFromFiles();
        assertEquals(40, tiles.size());
    }

    @Test
    void getTileStringFromFiles() {
        String tilesString = TileLoader.getTileStringFromFiles();
        assertEquals("{\"@class\":\"ee.t", tilesString.substring(0, 15));
    }
}