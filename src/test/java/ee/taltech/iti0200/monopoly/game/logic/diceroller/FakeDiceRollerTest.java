package ee.taltech.iti0200.monopoly.game.logic.diceroller;

import ee.taltech.iti0200.monopoly.game.dice.Dice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class FakeDiceRollerTest {
    private IDiceRoller iDiceRoller;
    private Dice dice;
    private List<int[]> predefinedRolls;

    @BeforeEach
    void setUp() {
        predefinedRolls = new ArrayList<>(List.of(new int[] {1, 1}, new int[] {1, 2}));
        iDiceRoller = new FakeDiceRoller(predefinedRolls, 1);
        dice = new Dice();
    }

    @Test
    void rollAndReturnDice() {
        iDiceRoller.rollAndReturnDice(dice);
        assertEquals(2, dice.getDie1(), dice.getDie2());
        iDiceRoller.rollAndReturnDice(dice);
        assertFalse(dice.isDoubles());
    }

    @Test
    void getDiceRolls() {
        ((FakeDiceRoller) iDiceRoller).setDiceRolls(predefinedRolls);
        assertEquals(predefinedRolls ,((FakeDiceRoller) iDiceRoller).getDiceRolls());
    }

    @Test
    void getRandom() {
        ((FakeDiceRoller) iDiceRoller).setRandom(new Random());
        assertNotNull(((FakeDiceRoller) iDiceRoller).getRandom());
    }
}