package ee.taltech.iti0200.monopoly.game.card;

import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {
    private Card card;
    private Player playerFoo;

    @BeforeEach
    void setUp() {
        card = new Card();
        playerFoo = new Player();
        playerFoo.setName("Foo");

        card.setType(CardType.CHANCE);
        card.setAction(CardAction.GET_OUT_OF_JAIL);
        card.setText1("Foo1");
        card.setText2("Foo2");
        card.setText3("Foo3");
        card.setText4("Foo4");
        card.setCollectible(true);
        card.setMoveToPos(2);
        card.setMoveSteps(4);
        card.setOwner(playerFoo);
        card.setReceiveMoneyFromBank(10);
        card.setPayMoneyToBank(20);
        card.setReceiveMoneyFromPlayers(5);
        card.setMaintenancePerHouse(3);
        card.setMaintenancePerHotel(4);
        card.setCardIndex(7);
    }

    @Test
    void getType() {
        assertEquals(CardType.CHANCE, card.getType());
    }

    @Test
    void getAction() {
        assertEquals(CardAction.GET_OUT_OF_JAIL, card.getAction());
    }

    @Test
    void getText1() {
        assertEquals("Foo1", card.getText1());
    }

    @Test
    void getText2() {
        assertEquals("Foo2", card.getText2());
    }

    @Test
    void getText3() {
        assertEquals("Foo3", card.getText3());
    }

    @Test
    void getText4() {
        assertEquals("Foo4", card.getText4());
    }

    @Test
    void isCollectible() {
        assertTrue(card.isCollectible());
    }

    @Test
    void getMoveToPos() {
        assertEquals(2, card.getMoveToPos());
    }

    @Test
    void getMoveSteps() {
        assertEquals(4, card.getMoveSteps());
    }

    @Test
    void getOwner() {
        assertEquals(playerFoo, card.getOwner());
    }

    @Test
    void getReceiveMoneyFromBank() {
        assertEquals(10, card.getReceiveMoneyFromBank());
    }

    @Test
    void getPayMoneyToBank() {
        assertEquals(20, card.getPayMoneyToBank());
    }

    @Test
    void getReceiveMoneyFromPlayers() {
        assertEquals(5, card.getReceiveMoneyFromPlayers());
    }

    @Test
    void getMaintenancePerHotel() {
        assertEquals(4, card.getMaintenancePerHotel());
    }

    @Test
    void getMaintenancePerHouse() {
        assertEquals(3, card.getMaintenancePerHouse());
    }

    @Test
    void getCardIndex() {
        assertEquals(7, card.getCardIndex());
    }

    @Test
    void setType() {
        assertEquals(CardType.CHANCE, card.getType());
    }

    @Test
    void setAction() {
        assertEquals(CardAction.GET_OUT_OF_JAIL, card.getAction());
    }

    @Test
    void setText1() {
        card.setText1("Bar1");
        assertEquals("Bar1", card.getText1());
    }

    @Test
    void setText2() {
        card.setText2("Bar2");
        assertEquals("Bar2", card.getText2());
    }

    @Test
    void setText3() {
        card.setText3("Bar3");
        assertEquals("Bar3", card.getText3());
    }

    @Test
    void setText4() {
        card.setText4("Bar4");
        assertEquals("Bar4", card.getText4());
    }

    @Test
    void setIsCollectible() {
        card.setCollectible(false);
        assertFalse(card.isCollectible());
    }

    @Test
    void setMoveToPos() {
        card.setMoveToPos(6);
        assertEquals(6, card.getMoveToPos());
    }

    @Test
    void setMoveSteps() {
        card.setMoveSteps(8);
        assertEquals(8, card.getMoveSteps());
    }

    @Test
    void setOwner() {
        card.setOwner(null);
        assertNull(card.getOwner());
    }

    @Test
    void setReceiveMoneyFromBank() {
        card.setReceiveMoneyFromBank(80);
        assertEquals(80, card.getReceiveMoneyFromBank());
    }

    @Test
    void setPayMoneyToBank() {
        card.setPayMoneyToBank(60);
        assertEquals(60, card.getPayMoneyToBank());
    }

    @Test
    void setReceiveMoneyFromPlayers() {
        card.setReceiveMoneyFromPlayers(35);
        assertEquals(35, card.getReceiveMoneyFromPlayers());
    }

    @Test
    void setMaintenancePerHotel() {
        card.setMaintenancePerHotel(50);
        assertEquals(50, card.getMaintenancePerHotel());
    }

    @Test
    void setMaintenancePerHouse() {
        card.setMaintenancePerHouse(50);
        assertEquals(50, card.getMaintenancePerHouse());
    }

    @Test
    void setCardIndex() {
        card.setCardIndex(70);
        assertEquals(70, card.getCardIndex());
    }
}