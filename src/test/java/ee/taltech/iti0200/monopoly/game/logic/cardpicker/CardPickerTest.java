package ee.taltech.iti0200.monopoly.game.logic.cardpicker;

import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.card.Deck;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CardPickerTest {
    private ICardPicker cardPicker;
    private Deck deck;
    private Card card1, card2;

    @BeforeEach
    void setUp() {
        cardPicker = new CardPicker();
        card1 = new Card();
        card1.setType(CardType.CHANCE);
        card2 = new Card();
        card2.setType(CardType.COMMUNITY_CHEST);
        List<Card> chanceCardList = new ArrayList<>();
        List<Card> communityCardList = new ArrayList<>();
        chanceCardList.add(card1);
        communityCardList.add(card2);
        deck = new Deck();
        deck.setChanceCards(chanceCardList);
        deck.setCommunityChestCards(communityCardList);
    }

    @Test
    void pickACardChance() {
        assertEquals(card1, cardPicker.pickACard(deck, CardType.CHANCE));
    }

    @Test
    void pickACardCommunity() {
        assertEquals(card2, cardPicker.pickACard(deck, CardType.COMMUNITY_CHEST));
    }
}