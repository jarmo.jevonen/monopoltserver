package ee.taltech.iti0200.monopoly.game.valuecalc;

import ee.taltech.iti0200.monopoly.game.board.tile.*;
import ee.taltech.iti0200.monopoly.game.dice.Dice;
import ee.taltech.iti0200.monopoly.game.logic.game.Game;
import ee.taltech.iti0200.monopoly.game.logic.game.PropertyService;
import ee.taltech.iti0200.monopoly.game.logic.initializer.PlayersInitializer;
import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ValueCalculatorTest {
    private ValueCalculator valueCalculator;
    private Player player1;
    private Game game;
    private PropertyTile propertyTile1, propertyTile2, propertyTile3;
    private PropertyTileGroup propertyTileGroup;
    private Dice dice;
    private List<PropertyTile> propertyTiles;

    @BeforeEach
    void setUp() {
        player1 = new Player();
        List<Player> players = new ArrayList<>();
        players.add(PlayersInitializer.initializeAndReturnPlayer(player1));
        game = new Game(null);
        propertyTileGroup = new PropertyTileGroup();
        propertyTile1 = new RegularPropertyTile(1d, 2d, 3d, 4d, 5d, 6d, 0, 50d);
        propertyTile2 = new RegularPropertyTile(1d, 2d, 3d, 4d, 5d, 6d, 0, 50d);
        propertyTile3 = new UtilityPropertyTile(propertyTileGroup, 1, 2);
        game.initializeGame(players);
        game.setCurrentTurnPlayer(player1);
        valueCalculator = new ValueCalculator(game);
        propertyTiles = new ArrayList<>();
        propertyTiles.add(propertyTile1);
        propertyTiles.add(propertyTile2);
        propertyTiles.add(propertyTile3);
        propertyTile1.setOwner(player1);
        propertyTile1.setPropertyColor(PropertyColor.BROWN);
        propertyTile2.setPropertyColor(PropertyColor.BROWN);
        propertyTile3.setPropertyColor(PropertyColor.UTILITY);
        propertyTile1.setMortgageValue(1d);
        propertyTile2.setMortgageValue(1d);
        propertyTile3.setMortgageValue(1d);
        ((RegularPropertyTile) propertyTile1).setBuildingsCount(2);
        ((RegularPropertyTile) propertyTile1).setUpgradePrice(50d);
        player1.setOwnedPropertyTiles(propertyTiles);
    }

    @Test
    void calculatePlayerValueBySellingBuildingsAndTakingMortgages() {
        assertEquals(203d, valueCalculator.calculatePlayerValueBySellingBuildingsAndTakingMortgages(player1));
    }
}