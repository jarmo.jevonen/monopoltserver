package ee.taltech.iti0200.monopoly.game.logic.initializer;

import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayersInitializerTest {
    private PlayersInitializer playersInitializer;
    private Player player;

    @BeforeEach
    void setUp() {
        playersInitializer = new PlayersInitializer();
        player = new Player();
    }

    @Test
    void initializeAndReturnPlayer() {
        assertEquals(0, PlayersInitializer.initializeAndReturnPlayer(player).getPosition());
    }
}