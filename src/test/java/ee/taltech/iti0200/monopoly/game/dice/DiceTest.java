package ee.taltech.iti0200.monopoly.game.dice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DiceTest {
    private Dice dice;

    @BeforeEach
    void setUp() {
        dice = new Dice();
        dice.setDie1(2);
        dice.setDie2(3);
        dice.setDoubles(false);
    }

    @Test
    void getDie1() {
        assertEquals(2, dice.getDie1());
    }

    @Test
    void getDie2() {
        assertEquals(3, dice.getDie2());
    }

    @Test
    void isDoubles() {
        assertFalse(dice.isDoubles());
    }

    @Test
    void setDie1() {
        dice.setDie1(5);
        assertEquals(5, dice.getDie1());
    }

    @Test
    void setDie2() {
        dice.setDie2(5);
        assertEquals(5, dice.getDie2());
    }

    @Test
    void setDoubles() {
        dice.setDoubles(true);
        assertTrue(dice.isDoubles());
    }
}