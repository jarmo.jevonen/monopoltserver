package ee.taltech.iti0200.monopoly.game.logic.initializer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TilesInitializerTest {
    private TilesInitializer initializer;

    @BeforeEach
    void setUp() {
        initializer = new TilesInitializer();
    }

    @Test
    void initializeAndGetTiles() {
        TilesInitializer.initializeAndGetTiles();
    }
}