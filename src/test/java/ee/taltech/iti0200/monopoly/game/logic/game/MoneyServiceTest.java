package ee.taltech.iti0200.monopoly.game.logic.game;

import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoneyServiceTest {
    private Player player;
    private MoneyService moneyService;
    private Game game;

    @BeforeEach
    void setUp() {
        player = new Player();
        player.setBalance(10);
        moneyService = new MoneyService();
    }

    @Test
    void playerHasEnoughMoney() {
        assertTrue(moneyService.playerHasEnoughMoney(player, 10));
    }

    @Test
    void balanceDeductAmount() {
        moneyService.balanceDeductAmount(player, 10);
        assertEquals(0, player.getBalance());
    }

    @Test
    void balanceAddAmount() {
        moneyService.balanceAddAmount(player, 10);
        assertEquals(20, player.getBalance());
    }
}