package ee.taltech.iti0200.monopoly.game.logic.initializer;

import ee.taltech.iti0200.monopoly.game.board.Board;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoardInitializerTest {
    BoardInitializer initializer;

    @Test
    void initializeAndReturnBoard() {
        initializer = new BoardInitializer();
        assertTrue(BoardInitializer.initializeAndReturnBoard() instanceof Board);
    }
}