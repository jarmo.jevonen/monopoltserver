package ee.taltech.iti0200.monopoly.game.logic.game;

import ee.taltech.iti0200.monopoly.game.board.tile.PropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardAction;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.dice.Dice;
import ee.taltech.iti0200.monopoly.game.jail.Jail;
import ee.taltech.iti0200.monopoly.game.logic.cardpicker.FakeCardPicker;
import ee.taltech.iti0200.monopoly.game.logic.cardpicker.ICardPicker;
import ee.taltech.iti0200.monopoly.game.logic.diceroller.FakeDiceRoller;
import ee.taltech.iti0200.monopoly.game.logic.diceroller.IDiceRoller;
import ee.taltech.iti0200.monopoly.game.logic.gameenigne.GameNetworkingServerLogic;
import ee.taltech.iti0200.monopoly.game.logic.initializer.PlayersInitializer;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.serv.MpServer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
    private Game game;
    private static int port = 26300;
    private String lobbyId;
    private List<Player> players;
    private ICardPicker cardPicker;
    private IDiceRoller diceRoller;

    @BeforeEach
    void setUp() {
        MpServer mpServer = new MpServer(port++);
        lobbyId = mpServer.createNewLobby(0);
        game = mpServer.getLobbiesMap().get(lobbyId).getGame();

        Player player1 = new Player();
        player1.setName("Alice");
        player1.setConnectionId(1234);
        PlayersInitializer.initializeAndReturnPlayer(player1);
        player1.setPlayerId("alice");

        Player player2 = new Player();
        player2.setName("Bob");
        player2.setConnectionId(3456);
        PlayersInitializer.initializeAndReturnPlayer(player2);
        player2.setPlayerId("bob");

        Player player3 = new Player();
        player3.setName("John");
        player3.setConnectionId(5678);
        PlayersInitializer.initializeAndReturnPlayer(player3);
        player3.setPlayerId("john");
        player3.setAI(true);

        players = new ArrayList<>(List.of(player1, player2, player3));

        game.initializeGame(players);
        game.startGame();
    }

    @Test
    void endTurnCanEnd() {
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        game.endTurn(game.getCurrentTurnPlayer(), false);
    }

    @Test
    void endTurnAI() {
        game.setCurrentTurnPlayer(players.get(2));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        game.endTurn(players.get(2), false);
    }

    @Test
    void endTurnForceBankruptcy() {
        game.setCurrentTurnPlayer(players.get(1));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        game.getCurrentTurnPlayer().setBalance(-1000);
        game.endTurn(players.get(1), true);
    }

    @Test
    void endTurnDontForceBankruptcy() {
        game.setCurrentTurnPlayer(players.get(1));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        game.getCurrentTurnPlayer().setBalance(-1000);
        game.endTurn(players.get(1), false);
    }

    @Test
    void endTurnForceBankruptcyAI() {
        game.setCurrentTurnPlayer(players.get(2));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        game.getCurrentTurnPlayer().setBalance(-1000);
        game.endTurn(players.get(2), true);
    }

    @Test
    void endTurnDontForceBankruptcyAI() {
        game.setCurrentTurnPlayer(players.get(2));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        game.getCurrentTurnPlayer().setBalance(-1000);
        game.endTurn(players.get(2), false);
    }

    @Test
    void endTurnBankruptcySkipPlayer() {
        game.setCurrentTurnPlayer(players.get(1));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        game.getCurrentTurnPlayer().setBalance(1000);
        players.get(2).setHasGoneBankrupt(true);
        game.endTurn(players.get(2), false);
    }

    @Test
    void endTurnCheckForWin() {
        game.setCurrentTurnPlayer(players.get(0));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        players.get(1).setHasGoneBankrupt(true);
        players.get(2).setHasGoneBankrupt(true);
        game.endTurn(players.get(0), false);
    }

    @Test
    void endTurnCheckForWinNotOver() {
        game.setCurrentTurnPlayer(players.get(0));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        players.get(1).setHasGoneBankrupt(true);
        players.get(2).setHasGoneBankrupt(false);
        game.endTurn(players.get(0), false);
    }

    @Test
    void endTurnNextPlayerInJail() {
        game.setCurrentTurnPlayer(players.get(2));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        game.getJail().getPlayersInJailMap().put(players.get(0).getPlayerId(), 1);
        players.get(0).setInJail(true);
        game.endTurn(players.get(2), false);
    }

    @Test
    void endTurnNextPlayerAIInJail() {
        game.setCurrentTurnPlayer(players.get(1));
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        game.getJail().getPlayersInJailMap().put(players.get(2).getPlayerId(), 1);
        players.get(2).setInJail(true);
        game.endTurn(players.get(1), false);
    }

    @Test
    void rollDiceInJailLessThanThreeTurnsNotDoubles() {
        diceRoller = new FakeDiceRoller(new ArrayList<>(List.of(new int[] {1, 2}, new int[] {2, 3}, new int[] {3, 4})), 10);
        game.setDiceRoller(diceRoller);

        game.setCurrentTurnPlayer(players.get(0));
        players.get(0).setInJail(true);
        game.getJail().getPlayersInJailMap().put(players.get(0).getPlayerId(), 2);

        Dice dice = new Dice();
        dice.setDie1(1);
        dice.setDie2(2);
        dice.setDoubles(false);

        game.setCanCurrentTurnPlayerEndTheTurn(false);
        game.setCanCurrentTurnPlayerRollTheDice(true);
        game.rollDice(game.getCurrentTurnPlayer());
    }

    @Test
    void rollDiceInJailLessThanThreeTurnsNotDoublesAI() {
        diceRoller = new FakeDiceRoller(new ArrayList<>(List.of(new int[] {1, 2}, new int[] {2, 3}, new int[] {3, 4})), 10);
        game.setDiceRoller(diceRoller);

        game.setCurrentTurnPlayer(players.get(2));
        players.get(2).setInJail(true);
        game.getJail().getPlayersInJailMap().put(players.get(2).getPlayerId(), 2);

        Dice dice = new Dice();
        dice.setDie1(1);
        dice.setDie2(2);
        dice.setDoubles(false);

        game.setCanCurrentTurnPlayerEndTheTurn(false);
        game.setCanCurrentTurnPlayerRollTheDice(true);
        game.rollDice(game.getCurrentTurnPlayer());
    }

    @Test
    void rollDiceInJailDoubles() {
        diceRoller = new FakeDiceRoller(new ArrayList<>(List.of(new int[] {2, 2}, new int[] {2, 3}, new int[] {3, 4})), 10);
        game.setDiceRoller(diceRoller);

        game.setCurrentTurnPlayer(players.get(0));
        players.get(0).setInJail(true);
        game.getJail().getPlayersInJailMap().put(players.get(0).getPlayerId(), 2);

        Dice dice = new Dice();
        dice.setDie1(2);
        dice.setDie2(2);
        dice.setDoubles(true);

        game.setCanCurrentTurnPlayerEndTheTurn(false);
        game.setCanCurrentTurnPlayerRollTheDice(true);
        game.rollDice(game.getCurrentTurnPlayer());
    }

    @Test
    void rollDiceInJailSequentialDoubles() {
        diceRoller = new FakeDiceRoller(new ArrayList<>(List.of(new int[] {2, 2}, new int[] {2, 3}, new int[] {3, 4})), 10);
        game.setDiceRoller(diceRoller);

        game.setCurrentTurnPlayer(players.get(0));
        players.get(0).setSequentialDoublesCount(1);

        Dice dice = new Dice();
        dice.setDie1(2);
        dice.setDie2(2);
        dice.setDoubles(true);

        game.setCanCurrentTurnPlayerEndTheTurn(false);
        game.setCanCurrentTurnPlayerRollTheDice(true);
        game.rollDice(game.getCurrentTurnPlayer());
    }

    @Test
    void rollDiceInJailSequentialDoublesFull() {
        diceRoller = new FakeDiceRoller(new ArrayList<>(List.of(new int[] {2, 2}, new int[] {2, 3}, new int[] {3, 4})), 10);
        game.setDiceRoller(diceRoller);

        game.setCurrentTurnPlayer(players.get(0));
        players.get(0).setSequentialDoublesCount(2);

        Dice dice = new Dice();
        dice.setDie1(2);
        dice.setDie2(2);
        dice.setDoubles(true);

        game.setCanCurrentTurnPlayerEndTheTurn(false);
        game.setCanCurrentTurnPlayerRollTheDice(true);
        game.rollDice(game.getCurrentTurnPlayer());
    }

    @Test
    void tryToSetMortgageToPropertyTileTileNotOwned() {
        game.tryToSetMortgageToPropertyTile(players.get(0), (PropertyTile) game.getBoard().getTiles().get(8), true);
    }

    @Test
    void tryToSetMortgageToPropertyTileToBeMortgaged() {
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(8));
        Player player = players.get(0);
        propertyTile.setOwner(player);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));
        game.tryToSetMortgageToPropertyTile(player, propertyTile, true);
    }

    @Test
    void tryToSetMortgageToPropertyTileToBeUnMortgagedNotEnoughMoney() {
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(8));
        Player player = players.get(0);
        player.setBalance(0);
        propertyTile.setOwner(player);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));
        propertyTile.setMortgaged(true);
        game.tryToSetMortgageToPropertyTile(player, propertyTile, false);
    }

    @Test
    void tryToSetMortgageToPropertyTileToBeUnMortgagedEnoughMoney() {
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(8));
        Player player = players.get(0);
        propertyTile.setOwner(player);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));
        propertyTile.setMortgaged(true);
        game.tryToSetMortgageToPropertyTile(player, propertyTile, false);
    }

    @Test
    void receivePurchaseDecision() {
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(8));
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        player.setPosition(8);
        game.receivePurchaseDecision(player, propertyTile, true);
    }

    @Test
    void receivePurchaseDecisionNotCurrentTurnPlayer() {
        game.setCurrentTurnPlayer(players.get(1));
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(8));
        Player player = players.get(0);
        player.setPosition(8);
        game.receivePurchaseDecision(player, propertyTile, true);
    }

    @Test
    void receivePurchaseDecisionNotEnoughMoney() {
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(8));
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        player.setBalance(0);
        player.setPosition(8);
        game.receivePurchaseDecision(player, propertyTile, true);
    }

    @Test
    void receiveCardChoiceNotPlayersTurn() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(players.get(1));
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);
        game.receiveCardChoice(player, true);
    }

    @Test
    void receiveCardChoicePay() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);
        game.receiveCardChoice(player, true);
    }

    @Test
    void receiveCardChoiceSwitchToChance() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);
        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchToCommunityChest() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.CHANCE);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);
        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardGoToJail() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(14)));
        game.setCardPicker(cardPicker);

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardGoToPos() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(3)));
        game.setCardPicker(cardPicker);

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardGoBackTo() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.CHANCE);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(13)));
        game.setCardPicker(cardPicker);

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardGoSteps() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(13)));
        game.setCardPicker(cardPicker);

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardGetOutOfJail() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(11)));
        game.setCardPicker(cardPicker);

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchAI() {
        Player player = players.get(2);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.CHANCE);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(3)));
        game.setCardPicker(cardPicker);

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitch() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.CHANCE);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(3)));
        game.setCardPicker(cardPicker);

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardReceiveMoneyFromPlayers() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.CHANCE);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(9)));
        game.setCardPicker(cardPicker);

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardReceiveMoneyFromBank() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(4)));
        game.setCardPicker(cardPicker);

        game.receiveCardChoice(player, false);
    }


    @Test
    void receiveCardUseGetOutOfJailCard() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);

        players.get(0).setInJail(true);
        game.getJail().getPlayersInJailMap().put(players.get(0).getPlayerId(), 1);

        Card card = new Card();
        card.setAction(CardAction.GET_OUT_OF_JAIL);
        player.setOwnedCards(new ArrayList<>(List.of(card)));

        game.receiveCardUseGetOutOfJailCard(player, true);
    }

    @Test
    void receiveCardUseGetOutOfJailCardNoCard() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        game.receiveCardUseGetOutOfJailCard(player, true);
    }

    @Test
    void receiveMoneyPayToGetOutOfJail() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);

        players.get(0).setInJail(true);
        game.getJail().getPlayersInJailMap().put(players.get(0).getPlayerId(), 1);

        game.receiveMoneyPayToGetOutOfJail(player);
    }

    @Test
    void receivePropertyUpDowngradeDecisionNotRegularDowngradeMortgage() {
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(12));
        Player player = players.get(0);
        propertyTile.setOwner(player);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));
        game.receivePropertyUpDowngradeDecision(player, propertyTile, false);
    }

    @Test
    void receivePropertyUpDowngradeDecisionNotRegularDowngradeAlreadyMortgaged() {
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(12));
        Player player = players.get(0);
        propertyTile.setOwner(player);
        propertyTile.setMortgaged(true);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));
        game.receivePropertyUpDowngradeDecision(player, propertyTile, false);
    }

    @Test
    void receivePropertyUpDowngradeDecisionNotRegularUpgradeAlreadyMortgaged() {
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(12));
        Player player = players.get(0);
        propertyTile.setOwner(player);
        propertyTile.setMortgaged(true);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));
        game.receivePropertyUpDowngradeDecision(player, propertyTile, true);
    }

    @Test
    void receivePropertyUpDowngradeDecisionNotRegularUpgrade() {
        PropertyTile propertyTile = ((PropertyTile) game.getBoard().getTiles().get(12));
        Player player = players.get(0);
        propertyTile.setOwner(player);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));
        game.receivePropertyUpDowngradeDecision(player, propertyTile, true);
    }

    @Test
    void receivePropertyUpDowngradeDecisionRegularUpgradeAlreadyFull() {
        RegularPropertyTile regularPropertyTile6 = ((RegularPropertyTile) game.getBoard().getTiles().get(6));
        RegularPropertyTile regularPropertyTile8 = ((RegularPropertyTile) game.getBoard().getTiles().get(8));
        RegularPropertyTile regularPropertyTile9 = ((RegularPropertyTile) game.getBoard().getTiles().get(9));
        Player player = players.get(0);
        regularPropertyTile6.setOwner(player);
        regularPropertyTile8.setOwner(player);
        regularPropertyTile9.setOwner(player);
        regularPropertyTile6.setBuildingsCount(5);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(regularPropertyTile6, regularPropertyTile8, regularPropertyTile9)));
        game.receivePropertyUpDowngradeDecision(player, regularPropertyTile6, true);
    }

    @Test
    void receivePropertyUpDowngradeDecisionRegularUpgradeDontHaveAllColors() {
        RegularPropertyTile regularPropertyTile6 = ((RegularPropertyTile) game.getBoard().getTiles().get(6));
        RegularPropertyTile regularPropertyTile8 = ((RegularPropertyTile) game.getBoard().getTiles().get(8));
        Player player = players.get(0);
        regularPropertyTile6.setOwner(player);
        regularPropertyTile8.setOwner(player);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(regularPropertyTile6, regularPropertyTile8)));
        game.receivePropertyUpDowngradeDecision(player, regularPropertyTile6, true);
    }

    @Test
    void receivePropertyUpDowngradeDecisionRegularUpgrade() {
        RegularPropertyTile regularPropertyTile6 = ((RegularPropertyTile) game.getBoard().getTiles().get(6));
        RegularPropertyTile regularPropertyTile8 = ((RegularPropertyTile) game.getBoard().getTiles().get(8));
        RegularPropertyTile regularPropertyTile9 = ((RegularPropertyTile) game.getBoard().getTiles().get(9));
        Player player = players.get(0);
        regularPropertyTile6.setOwner(player);
        regularPropertyTile8.setOwner(player);
        regularPropertyTile9.setOwner(player);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(regularPropertyTile6, regularPropertyTile8, regularPropertyTile9)));
        game.receivePropertyUpDowngradeDecision(player, regularPropertyTile6, true);
    }

    @Test
    void receivePropertyUpDowngradeDecisionRegularUpgradeMortgaged() {
        RegularPropertyTile regularPropertyTile6 = ((RegularPropertyTile) game.getBoard().getTiles().get(6));
        RegularPropertyTile regularPropertyTile8 = ((RegularPropertyTile) game.getBoard().getTiles().get(8));
        RegularPropertyTile regularPropertyTile9 = ((RegularPropertyTile) game.getBoard().getTiles().get(9));
        Player player = players.get(0);
        regularPropertyTile6.setOwner(player);
        regularPropertyTile8.setOwner(player);
        regularPropertyTile9.setOwner(player);
        regularPropertyTile6.setMortgaged(true);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(regularPropertyTile6, regularPropertyTile8, regularPropertyTile9)));
        game.receivePropertyUpDowngradeDecision(player, regularPropertyTile6, true);
    }

    @Test
    void receivePropertyUpDowngradeDecisionRegularDowngradeMortgaged() {
        RegularPropertyTile regularPropertyTile6 = ((RegularPropertyTile) game.getBoard().getTiles().get(6));
        RegularPropertyTile regularPropertyTile8 = ((RegularPropertyTile) game.getBoard().getTiles().get(8));
        RegularPropertyTile regularPropertyTile9 = ((RegularPropertyTile) game.getBoard().getTiles().get(9));
        Player player = players.get(0);
        regularPropertyTile6.setOwner(player);
        regularPropertyTile8.setOwner(player);
        regularPropertyTile9.setOwner(player);
        regularPropertyTile6.setMortgaged(true);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(regularPropertyTile6, regularPropertyTile8, regularPropertyTile9)));
        game.receivePropertyUpDowngradeDecision(player, regularPropertyTile6, false);
    }

    @Test
    void receivePropertyUpDowngradeDecisionRegularDowngrade() {
        RegularPropertyTile regularPropertyTile6 = ((RegularPropertyTile) game.getBoard().getTiles().get(6));
        RegularPropertyTile regularPropertyTile8 = ((RegularPropertyTile) game.getBoard().getTiles().get(8));
        RegularPropertyTile regularPropertyTile9 = ((RegularPropertyTile) game.getBoard().getTiles().get(9));
        Player player = players.get(0);
        regularPropertyTile6.setOwner(player);
        regularPropertyTile8.setOwner(player);
        regularPropertyTile9.setOwner(player);
        regularPropertyTile6.setBuildingsCount(3);
        player.setOwnedPropertyTiles(new ArrayList<>(List.of(regularPropertyTile6, regularPropertyTile8, regularPropertyTile9)));
        game.receivePropertyUpDowngradeDecision(player, regularPropertyTile6, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardGoToPosPayRent() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(10)));
        game.setCardPicker(cardPicker);

        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(11);
        propertyTile.setOwner(players.get(1));
        players.get(1).setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardGoToPosPayRentMortgaged() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(10)));
        game.setCardPicker(cardPicker);

        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(11);
        propertyTile.setOwner(players.get(1));
        propertyTile.setMortgaged(true);
        players.get(1).setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));

        game.receiveCardChoice(player, false);
    }

    @Test
    void receiveCardChoiceSwitchTakeCardGoToPosAlreadyOwnTile() {
        Player player = players.get(0);
        game.setCurrentTurnPlayer(player);
        Card takenCard = new Card();
        takenCard.setType(CardType.COMMUNITY_CHEST);
        takenCard.setAction(CardAction.PAY_OR_SWITCH);
        game.setTakenCard(takenCard);

        cardPicker = new FakeCardPicker(new ArrayList<>(List.of(10)));
        game.setCardPicker(cardPicker);

        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(11);
        propertyTile.setOwner(players.get(0));
        players.get(0).setOwnedPropertyTiles(new ArrayList<>(List.of(propertyTile)));

        game.receiveCardChoice(player, false);
    }

    @Test
    void playerDisconnected() {
        Player player = players.get(2);
        game.setCurrentTurnPlayer(player);
        game.playerDisconnected(player);
    }

    @Test
    void getGameNetworkingServerLogic() {
        game.setGameNetworkingServerLogic(null);
        assertNull(game.getGameNetworkingServerLogic());
    }

    @Test
    void getTurnCount() {
        game.setTurnCount(0);
        assertEquals(0, game.getTurnCount());
    }

    @Test
    void getBoard() {
        game.setBoard(null);
        assertNull(game.getBoard());
    }

    @Test
    void getPlayers() {
        game.setPlayers(null);
        assertNull(game.getPlayers());
    }

    @Test
    void getBank() {game.setBank(null);
        assertNull(game.getBank());
    }

    @Test
    void getJail() {
        game.setJail(null);
        assertNull(game.getJail());
    }

    @Test
    void getDice() {
        game.setDice(null);
        assertNull(game.getDice());
    }

    @Test
    void getCardPicker() {
        game.setCardPicker(null);
        assertNull(game.getCardPicker());
    }

    @Test
    void getDiceRoller() {
        game.setDiceRoller(null);
        assertNull(game.getDiceRoller());
    }

    @Test
    void getOpportunityDeck() {
        game.setOpportunityDeck(null);
        assertNull(game.getOpportunityDeck());
    }

    @Test
    void getValueCalculator() {
        game.setValueCalculator(null);
        assertNull(game.getValueCalculator());
    }

    @Test
    void isGameInProgress() {
        game.setGameInProgress(false);
        assertFalse(game.isGameInProgress());
    }

    @Test
    void getCurrentTurnPlayer() {
        game.setCurrentTurnPlayer(null);
        assertNull(game.getCurrentTurnPlayer());
    }

    @Test
    void isCanCurrentTurnPlayerRollTheDice() {
        game.setCanCurrentTurnPlayerRollTheDice(true);
        assertTrue(game.isCanCurrentTurnPlayerRollTheDice());
    }

    @Test
    void isCanCurrentTurnPlayerEndTheTurn() {
        game.setCanCurrentTurnPlayerEndTheTurn(true);
        assertTrue(game.isCanCurrentTurnPlayerEndTheTurn());
    }

    @Test
    void getTakenCard() {
        game.setTakenCard(null);
        assertNull(game.getTakenCard());
    }

    @Test
    void getMoneyService() {
        game.setMoneyService(null);
        assertNull(game.getMoneyService());
    }

    @Test
    void getPropertyService() {
        game.setPropertyService(null);
        assertNull(game.getPropertyService());
    }

    @Test
    void getCpuPlayerLogic() {
        game.setCpuPlayerLogic(null);
        assertNull(game.getCpuPlayerLogic());
    }

    @Test
    void getPropertyTileBaseValueMap() {
        game.setPropertyTileBaseValueMap(null);
        assertNull(game.getPropertyTileBaseValueMap());
    }
}