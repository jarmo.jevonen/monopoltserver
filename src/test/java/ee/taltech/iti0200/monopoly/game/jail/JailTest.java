package ee.taltech.iti0200.monopoly.game.jail;

import ee.taltech.iti0200.monopoly.game.player.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class JailTest {
    private Jail jail;

    @BeforeEach
    void setUp() {
        jail = new Jail();
        jail.setPlayersInJailMap(Map.of("FooId", 3));
    }

    @Test
    void getPlayersInJailMap() {
        assertEquals(Map.of("FooId", 3), jail.getPlayersInJailMap());
    }

    @Test
    void setPlayersInJailMap() {
        jail.setPlayersInJailMap(Map.of("BarId", 4));
        assertEquals(Map.of("BarId", 4), jail.getPlayersInJailMap());
    }
}