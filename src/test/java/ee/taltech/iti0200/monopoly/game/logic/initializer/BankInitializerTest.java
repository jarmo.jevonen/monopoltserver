package ee.taltech.iti0200.monopoly.game.logic.initializer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BankInitializerTest {
    private BankInitializer bankInitializer;

    @BeforeEach
    void setUp() {
        bankInitializer = new BankInitializer();
    }

    @Test
    void initializeAndReturnBank() {
        assertEquals(12, BankInitializer.initializeAndReturnBank().getAvailableHotelsCount());
    }
}