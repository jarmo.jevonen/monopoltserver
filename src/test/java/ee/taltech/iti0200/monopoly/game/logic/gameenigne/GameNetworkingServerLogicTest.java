package ee.taltech.iti0200.monopoly.game.logic.gameenigne;

import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.UtilityPropertyTile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardAction;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.dice.Dice;
import ee.taltech.iti0200.monopoly.game.jail.Jail;
import ee.taltech.iti0200.monopoly.game.logic.game.CpuPlayerLogic;
import ee.taltech.iti0200.monopoly.game.logic.game.Game;
import ee.taltech.iti0200.monopoly.game.logic.initializer.BoardInitializer;
import ee.taltech.iti0200.monopoly.game.logic.initializer.PlayersInitializer;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.dto.DTOGameDTOHandler;
import ee.taltech.iti0200.monopoly.server.dto.DTOPlayerReadyObject;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver.DTOatsCardChoice;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver.DTOatsCardUseGetOutOfJailCard;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.die.toserver.DTOatsDieRollTheDice;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.toserver.DTOatsGameStatePlayerBankruptcyAcknowledgement;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.toserver.DTOatsGameStatePlayerEndTurn;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.toserver.DTOatsMoneyPayToGetOutOfJail;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver.DTOatsPropertyPurchaseDecision;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver.DTOatsPropertyUpDowngradeDecision;
import ee.taltech.iti0200.monopoly.server.serv.MpServer;
import org.codehaus.jackson.JsonParseException;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class GameNetworkingServerLogicTest {
    private static int port = 24000;
    private GameNetworkingServerLogic gameNetworkingServerLogic;
    private Game game;
    private Player player1;
    private MpServer mpServer;
    private String lobbyId;

    @BeforeEach
    void setUp() {
        port++;
        mpServer = new MpServer(port);
        lobbyId = mpServer.createNewLobby(0);
        gameNetworkingServerLogic = new GameNetworkingServerLogic(mpServer, lobbyId);
        game = new Game(gameNetworkingServerLogic);

        player1 = new Player();
        player1.setName("Alice");
        player1.setConnectionId(1234);
        player1.setPlayerId("dcddc87f-f7b7-44ba-87b2-a4075809232f");
        PlayersInitializer.initializeAndReturnPlayer(player1);
        player1.setPlayerId("abcde");

        List<Player> players = new ArrayList<>(List.of(
                player1
        ));
        game.initializeGame(players);
        gameNetworkingServerLogic.setPlayers(players);
        game.initializeGame(players);

    }

    @Test
    void receivePlayerReadyStatus() {
        DTOPlayerReadyObject dtoPlayerReadyObject = new DTOPlayerReadyObject();
        dtoPlayerReadyObject.setPlayerId("abcde");
        dtoPlayerReadyObject.setReadyStatus(false);
        dtoPlayerReadyObject.setPlayerName("Alice");
        gameNetworkingServerLogic.setDtoPlayerReadyObjects(List.of(dtoPlayerReadyObject));

        String dtoString = "{\"playerId\":\"abcde\",\"readyStatus\":false,\"playerName\":\"Alice\"}";
        gameNetworkingServerLogic.receivePlayerReadyStatus(1234, dtoString);
    }

    @Test
    void receivePlayerReadyStatusAllReady() {
        DTOPlayerReadyObject dtoPlayerReadyObject = new DTOPlayerReadyObject();
        dtoPlayerReadyObject.setPlayerId("abcde");
        dtoPlayerReadyObject.setReadyStatus(true);
        dtoPlayerReadyObject.setPlayerName("Alice");
        gameNetworkingServerLogic.setDtoPlayerReadyObjects(List.of(dtoPlayerReadyObject));

        String dtoString = "{\"playerId\":\"abcde\",\"readyStatus\":true,\"playerName\":\"Alice\"}";
        gameNetworkingServerLogic.receivePlayerReadyStatus(1234, dtoString);
        assertTrue(gameNetworkingServerLogic.getGame().isGameInProgress());
    }

    @Test
    void broadcastPlayerReadyStatuses() {
        DTOPlayerReadyObject dtoPlayerReadyObject = new DTOPlayerReadyObject();
        dtoPlayerReadyObject.setPlayerId("abcde");
        dtoPlayerReadyObject.setReadyStatus(true);
        dtoPlayerReadyObject.setPlayerName("Alice");
        gameNetworkingServerLogic.setDtoPlayerReadyObjects(List.of(dtoPlayerReadyObject));

        String dtoString = "{\"playerId\":\"abcde\",\"readyStatus\":true,\"playerName\":\"Alice\"}";
        gameNetworkingServerLogic.receivePlayerReadyStatus(1234, dtoString);
        gameNetworkingServerLogic.broadcastPlayerReadyStatuses();
        assertTrue(gameNetworkingServerLogic.getGame().isGameInProgress());
    }

    @Test
    void broadcastGameStart() {
        gameNetworkingServerLogic.broadcastGameStart();
    }

    @Test
    void sendAPlayerIdToTheConnectedClient() {
        gameNetworkingServerLogic.sendAPlayerIdToTheConnectedClient(1234, "abcde");
    }

    @Test
    void playerDisconnected() {
        DTOPlayerReadyObject dtoPlayerReadyObject = new DTOPlayerReadyObject();
        dtoPlayerReadyObject.setPlayerId("abcde");
        gameNetworkingServerLogic.getDtoPlayerReadyObjects().add(dtoPlayerReadyObject);
        gameNetworkingServerLogic.getGame().setCurrentTurnPlayer(player1);
        gameNetworkingServerLogic.getGame().setCpuPlayerLogic(new CpuPlayerLogic(gameNetworkingServerLogic.getGame()));
        gameNetworkingServerLogic.playerDisconnected(1234);
    }

    @Test
    void getGameGameInProgress() {
        DTOPlayerReadyObject dtoPlayerReadyObject = new DTOPlayerReadyObject();
        dtoPlayerReadyObject.setPlayerId("abcde");
        dtoPlayerReadyObject.setReadyStatus(true);
        dtoPlayerReadyObject.setPlayerName("Alice");
        gameNetworkingServerLogic.setDtoPlayerReadyObjects(List.of(dtoPlayerReadyObject));

        String dtoString = "{\"playerId\":\"abcde\",\"readyStatus\":true,\"playerName\":\"Alice\"}";
        gameNetworkingServerLogic.receivePlayerReadyStatus(1234, dtoString);
        gameNetworkingServerLogic.broadcastPlayerReadyStatuses();
        assertTrue(gameNetworkingServerLogic.getGameGameInProgress());
    }

    @Test
    void endGameGameInProgress() {
        gameNetworkingServerLogic.endGameGameInProgress();
        assertFalse(gameNetworkingServerLogic.getGame().isGameInProgress());
    }

    @Test
    void sendCardSetPlayerOwnedGetOutOfJailCardsCount() {
        gameNetworkingServerLogic.sendCardSetPlayerOwnedGetOutOfJailCardsCount(player1);
    }

    @Test
    void sendCardSwitchACardOrPay() {
        gameNetworkingServerLogic.sendCardSwitchACardOrPay(player1, CardType.CHANCE, CardType.COMMUNITY_CHEST, 10);
    }

    @Test
    void broadcastCardTakeACard() {
        gameNetworkingServerLogic.broadcastCardTakeACard(player1, new Card());
    }

    @Test
    void broadcastDieDiceHasBeenRolled() {
        gameNetworkingServerLogic.broadcastDieDiceHasBeenRolled(new Dice());
    }

    @Test
    void sendGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn() {
        gameNetworkingServerLogic.sendGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(player1, true, true, true);
    }

    @Test
    void broadcastGameStatePlayerWentBankrupt() {
        gameNetworkingServerLogic.broadcastGameStatePlayerWentBankrupt(player1);
    }

    @Test
    void broadcastGameStateSetPlayerTurn() {
        gameNetworkingServerLogic.broadcastGameStateSetPlayerTurn(player1);
    }

    @Test
    void broadcastGameStateWinner() {
        gameNetworkingServerLogic.broadcastGameStateWinner(player1);
    }

    @Test
    void broadcastMoneyGetMoneyFromBank() {
        gameNetworkingServerLogic.broadcastMoneyGetMoneyFromBank(player1, 100);
    }

    @Test
    void broadcastMoneyPayMoneyToBank() {
        gameNetworkingServerLogic.broadcastMoneyPayMoneyToBank(player1, 100);
    }

    @Test
    void broadcastMoneyTransferMoneyBetweenPlayers() {
        Player player2 = new Player();
        gameNetworkingServerLogic.broadcastMoneyTransferMoneyBetweenPlayers(player1, player2, 10);
    }

    @Test
    void broadcastMovingMovePlayerToTile() {
        gameNetworkingServerLogic.broadcastMovingMovePlayerToTile(player1, 10, false, false);
    }

    @Test
    void sendPropertyOfferToBuy() {
        UtilityPropertyTile utilityPropertyTile = new UtilityPropertyTile();
        utilityPropertyTile.setPosition(12);
        gameNetworkingServerLogic.sendPropertyOfferToBuy(player1, utilityPropertyTile);
    }

    @Test
    void broadcastPropertyRemovePropertyOwnership() {
        UtilityPropertyTile utilityPropertyTile = new UtilityPropertyTile();
        utilityPropertyTile.setPosition(12);
        gameNetworkingServerLogic.broadcastPropertyRemovePropertyOwnership(player1, List.of(utilityPropertyTile));
    }

    @Test
    void broadcastPropertySetPropertyMortgagedStatus() {
        UtilityPropertyTile utilityPropertyTile = new UtilityPropertyTile();
        utilityPropertyTile.setPosition(12);
        gameNetworkingServerLogic.broadcastPropertySetPropertyMortgagedStatus(utilityPropertyTile);
    }

    @Test
    void broadcastPropertySetPropertyOwnership() {
        UtilityPropertyTile utilityPropertyTile = new UtilityPropertyTile();
        utilityPropertyTile.setPosition(12);
        gameNetworkingServerLogic.broadcastPropertySetPropertyOwnership(player1, utilityPropertyTile);
    }

    @Test
    void broadcastPropertyUpDowngradeProperty() {
        RegularPropertyTile regularPropertyTile = new RegularPropertyTile();
        regularPropertyTile.setPosition(12);
        regularPropertyTile.setBuildingsCount(5);
        gameNetworkingServerLogic.broadcastPropertyUpDowngradeProperty(regularPropertyTile);
    }

    @Test
    void receiveCardChoice() {
        DTOatsCardChoice dto = new DTOatsCardChoice();
        dto.setPlayerId("abcde");
        dto.setPay(true);
        gameNetworkingServerLogic.getGame().setCurrentTurnPlayer(new Player());
        gameNetworkingServerLogic.receiveCardChoice(dto, 1234);
    }

    @Test
    void receiveCardUseGetOutOfJailCard() {
        DTOatsCardUseGetOutOfJailCard dto = new DTOatsCardUseGetOutOfJailCard();
        dto.setUseCard(true);
        dto.setPlayerId("abcde");
        Card card = new Card();
        card.setAction(CardAction.GET_OUT_OF_JAIL);
        player1.setOwnedCards(new ArrayList<>(List.of(card)));
        Map<String, Integer> jailMap = new HashMap<>(Map.of("abcde", 1));
        Jail jail = new Jail();
        jail.setPlayersInJailMap(jailMap);
        gameNetworkingServerLogic.getGame().setJail(jail);
        gameNetworkingServerLogic.getGame().setCurrentTurnPlayer(player1);
        gameNetworkingServerLogic.receiveCardUseGetOutOfJailCard(dto, 1234);
    }

    @Test
    void receiveDieRollTheDice() {
        DTOatsDieRollTheDice dto = new DTOatsDieRollTheDice();
        dto.setPlayerId("abcde");
        gameNetworkingServerLogic.receiveDieRollTheDice(dto, 1234);
    }

    @Test
    void receiveGameStatePlayerEndTurn() {
        DTOatsGameStatePlayerEndTurn dto = new DTOatsGameStatePlayerEndTurn();
        dto.setPlayerId("abcde");
        gameNetworkingServerLogic.receiveGameStatePlayerEndTurn(dto, 1234);
    }

    @Test
    void receiveGameStatePlayerBankruptcyAcknowledgement() {
        DTOatsGameStatePlayerBankruptcyAcknowledgement dto = new DTOatsGameStatePlayerBankruptcyAcknowledgement();
        dto.setPlayerId("abcde");
        assertThrows(NullPointerException.class, () -> gameNetworkingServerLogic.receiveGameStatePlayerBankruptcyAcknowledgement(dto, 1234));
    }

    @Test
    void receiveMoneyPayToGetOutOfJail() {
        DTOatsMoneyPayToGetOutOfJail dto = new DTOatsMoneyPayToGetOutOfJail();
        dto.setPlayerId("abcde");
        Map<String, Integer> jailMap = new HashMap<>(Map.of("abcde", 1));
        Jail jail = new Jail();
        jail.setPlayersInJailMap(jailMap);
        gameNetworkingServerLogic.getGame().setJail(jail);
        player1.setInJail(true);
        gameNetworkingServerLogic.receiveMoneyPayToGetOutOfJail(dto, 1234);
    }

    @Test
    void receivePropertyPurchaseDecision() {
        DTOatsPropertyPurchaseDecision dto = new DTOatsPropertyPurchaseDecision();
        dto.setPlayerId("abcde");
        dto.setConfirmPurchase(false);
        gameNetworkingServerLogic.getGame().setBoard(BoardInitializer.initializeAndReturnBoard());
        player1.setPosition(8);
        gameNetworkingServerLogic.getGame().setCurrentTurnPlayer(player1);
        gameNetworkingServerLogic.receivePropertyPurchaseDecision(dto, 1234);
    }

    @Test
    void receivePropertyUpDowngradeDecision() {
        DTOatsPropertyUpDowngradeDecision dto = new DTOatsPropertyUpDowngradeDecision();
        dto.setPlayerId("abcde");
        dto.setPropertyIndex(8);
        dto.setUpgrade(false);
        gameNetworkingServerLogic.getGame().setBoard(BoardInitializer.initializeAndReturnBoard());
        gameNetworkingServerLogic.receivePropertyUpDowngradeDecision(dto, 1234);
    }

    @Test
    void getGame() {
        assertNull(gameNetworkingServerLogic.getGame().getJail());
    }

    @Test
    void getDtoPlayerReadyObjects() {
        DTOPlayerReadyObject dtoPlayerReadyObject = new DTOPlayerReadyObject();
        dtoPlayerReadyObject.setPlayerId("abcde");
        dtoPlayerReadyObject.setReadyStatus(true);
        dtoPlayerReadyObject.setPlayerName("Alice");
        gameNetworkingServerLogic.setDtoPlayerReadyObjects(List.of(dtoPlayerReadyObject));
        assertEquals(List.of(dtoPlayerReadyObject), gameNetworkingServerLogic.getDtoPlayerReadyObjects());
    }

    @Test
    void getPlayers() {
        assertEquals(List.of(player1), gameNetworkingServerLogic.getPlayers());
    }

    @Test
    void getMpServer() {
        mpServer = gameNetworkingServerLogic.getMpServer();
        assertEquals(mpServer, gameNetworkingServerLogic.getMpServer());
    }

    @Test
    void getDtoGameDTOHandler() {
        DTOGameDTOHandler dtoGameDTOHandler = gameNetworkingServerLogic.getDtoGameDTOHandler();
        assertEquals(dtoGameDTOHandler, gameNetworkingServerLogic.getDtoGameDTOHandler());
    }
}