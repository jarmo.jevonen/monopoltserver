package ee.taltech.iti0200.monopoly.server.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileUtilTest {
    private FileUtil fileUtil;

    @BeforeEach
    void setUp() {
        fileUtil = new FileUtil();
    }

    @Test
    void readFileFromClasspath() {
        FileUtil.readFileFromClasspath("server.properties");
    }

    @Test
    void readFileFromClasspathNull() {
        assertThrows(IllegalStateException.class, () -> FileUtil.readFileFromClasspath("lel"));
    }
}