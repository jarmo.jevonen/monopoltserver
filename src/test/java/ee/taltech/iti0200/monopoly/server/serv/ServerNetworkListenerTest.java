package ee.taltech.iti0200.monopoly.server.serv;

import ee.taltech.iti0200.monopoly.game.logic.game.Game;
import ee.taltech.iti0200.monopoly.game.logic.gameenigne.GameNetworkingServerLogic;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.packets.Packets;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ServerNetworkListenerTest {
    private ServerNetworkListener serverNetworkListener;
    private MpServer mpServer;
    private String lobbyId;
    private GameNetworkingServerLogic gameNetworkingServerLogic;
    private Game game;
    private Player player1;

    private static int port = 26100;

    @BeforeEach
    void setUp() {
        port++;
        mpServer = new MpServer(port);
        lobbyId = mpServer.createNewLobby(0);
        gameNetworkingServerLogic = mpServer.getLobbiesMap().get(lobbyId);
        game = gameNetworkingServerLogic.getGame();
        serverNetworkListener = mpServer.getSnl();
    }

    @Test
    void receivedSimpleMessage() {
        Packets.PacketSimpleMessage packet = new Packets.PacketSimpleMessage();
        packet.setChatMessage("Foo");
        serverNetworkListener.received(null, packet);
    }

    @Test
    void receivedRegisterPlayerFromClient() {
        Packets.PacketRegisterPlayerFromClient packet = new Packets.PacketRegisterPlayerFromClient();
        packet.setPlayerName("Foo");
        assertThrows(NullPointerException.class, () -> serverNetworkListener.received(null, packet));
    }

    @Test
    void receivedRegisterAIPlayerFromClient() {
        Packets.PacketRegisterAIPlayerFromClient packet = new Packets.PacketRegisterAIPlayerFromClient();
        serverNetworkListener.received(null, packet);
    }

    @Test
    void receivedRegisterAIPlayerFromClientFull() {
        mpServer.getLobbiesMap().get(lobbyId).setPlayers(new ArrayList<>(List.of(
                new Player(),
                new Player(),
                new Player(),
                new Player(),
                new Player(),
                new Player()
        )));
        Packets.PacketRegisterAIPlayerFromClient packet = new Packets.PacketRegisterAIPlayerFromClient();
        serverNetworkListener.received(null, packet);
    }

    @Test
    void receivedPlayerReadyObject() {
        player1 = new Player();
        player1.setName("Alice");
        player1.setPlayerId("abcde");
        player1.setConnectionId(1234);
        mpServer.getLobbiesMap().get(lobbyId).setPlayers(new ArrayList<>(List.of(
                player1
        )));
        Packets.PacketServerReceivePlayerReadyObject packet = new Packets.PacketServerReceivePlayerReadyObject();
        packet.setPlayerReadyObjectDTOString("{\"playerId\":\"abcde\",\"readyStatus\":false,\"playerName\":\"Alice\"}");
        assertThrows(NullPointerException.class, () -> serverNetworkListener.received(null, packet));
    }

    @Test
    void receivedDTOGameplayAction() {
        player1 = new Player();
        player1.setName("Alice");
        player1.setPlayerId("abcde");
        player1.setConnectionId(1234);
        mpServer.getLobbiesMap().get(lobbyId).setPlayers(new ArrayList<>(List.of(
                player1
        )));
        Packets.PacketServerReceiveDTOGameplayAction packet = new Packets.PacketServerReceiveDTOGameplayAction();
        packet.setTypeOfDTOGameplayAction("DTOatsDieRollTheDice");
        packet.setDTOGameplayActionJsonString("{\"playerId\":\"abcde\"}");
        assertThrows(NullPointerException.class, () -> serverNetworkListener.received(null, packet));
    }

    @Test
    void getMpServer() {
        assertEquals(mpServer, serverNetworkListener.getMpServer());
    }

    @Test
    void connected() {
        assertThrows(NullPointerException.class, () -> serverNetworkListener.connected(null));
    }

    @Test
    void diconnected() {
        assertThrows(NullPointerException.class, () -> serverNetworkListener.disconnected(null));
    }
}