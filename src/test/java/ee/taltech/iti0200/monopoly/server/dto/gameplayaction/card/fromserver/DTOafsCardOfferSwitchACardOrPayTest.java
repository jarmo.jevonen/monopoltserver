package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.fromserver;

import ee.taltech.iti0200.monopoly.game.card.CardType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsCardOfferSwitchACardOrPayTest {
    private DTOafsCardOfferSwitchACardOrPay dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsCardOfferSwitchACardOrPay();
        dto.setPlayerId("FooId");
        dto.setAmountToPay(10);
        dto.setCurrentCardType(CardType.CHANCE);
        dto.setAnotherCardType(CardType.COMMUNITY_CHEST);
    }

    @Test
    void getPlayerId() {assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void getCurrentCardType() {
        assertEquals(CardType.CHANCE, dto.getCurrentCardType());
    }

    @Test
    void getAnotherCardType() {
        assertEquals(CardType.COMMUNITY_CHEST, dto.getAnotherCardType());
    }

    @Test
    void getAmountToPay() {
        assertEquals(10, dto.getAmountToPay());
    }

    @Test
    void setPlayerId() {
        dto.setPlayerId("BarId");
        assertEquals("BarId", dto.getPlayerId());
    }

    @Test
    void setCurrentCardType() {
        dto.setCurrentCardType(CardType.COMMUNITY_CHEST);
        assertEquals(CardType.COMMUNITY_CHEST, dto.getCurrentCardType());
    }

    @Test
    void setAnotherCardType() {
        dto.setCurrentCardType(CardType.CHANCE);
        assertEquals(CardType.CHANCE, dto.getCurrentCardType());
    }

    @Test
    void setAmountToPay() {
        dto.setAmountToPay(20);
        assertEquals(20, dto.getAmountToPay());
    }
}