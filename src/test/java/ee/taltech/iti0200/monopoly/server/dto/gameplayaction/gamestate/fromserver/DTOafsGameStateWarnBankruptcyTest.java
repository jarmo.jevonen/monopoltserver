package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsGameStateWarnBankruptcyTest {
    private DTOafsGameStateWarnBankruptcy dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsGameStateWarnBankruptcy();
        dto.setPlayerId("Foo");
    }

    @Test
    void getPlayerId() {
        assertEquals("Foo", dto.getPlayerId());
    }

}