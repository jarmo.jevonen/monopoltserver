package ee.taltech.iti0200.monopoly.server.packets;

import com.esotericsoftware.kryo.Kryo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PacketsTest {
    private Packets.PacketServerSendGameStart packetServerSendGameStart;
    private Packets.PacketSimpleMessage packetSimpleMessage;
    private Packets.PacketRegisterPlayerFromClient packetRegisterPlayerFromClient;
    private Packets.PacketRegisterAIPlayerFromClient packetRegisterAIPlayerFromClient;
    private Packets.PacketRegisterPlayerFromServer packetRegisterPlayerFromServer;
    private Packets.PacketServerSendDTOGameboardData packetServerSendDTOGameboardData;
    private Packets.PacketServerSendDTOGameplayAction packetServerSendDTOGameplayAction;
    private Packets.PacketServerSendPlayerReadyObjectsJson packetServerSendPlayerReadyObjectsJson;
    private Packets.PacketServerReceiveDTOGameplayAction packetServerReceiveDTOGameplayAction;
    private Packets.PacketServerReceivePlayerReadyObject packetServerReceivePlayerReadyObject;

    @Test
    void registerPackets() {
        Packets packets = new Packets();
        Kryo kryo = new Kryo();
        Packets.RegisterPackets(kryo);
    }

    @Test
    void packetSimpleMessage() {
        packetSimpleMessage = new Packets.PacketSimpleMessage();
        packetSimpleMessage.setChatMessage("foo");
        assertEquals("foo", packetSimpleMessage.getChatMessage());
    }

    @Test
    void packetRegisterPlayerFromClientPlayerName() {
        packetRegisterPlayerFromClient = new Packets.PacketRegisterPlayerFromClient();
        packetRegisterPlayerFromClient.setPlayerName("foo");
        packetRegisterPlayerFromClient.setLobbyId("bar");
        assertEquals("foo", packetRegisterPlayerFromClient.getPlayerName());
    }

    @Test
    void packetRegisterPlayerFromClientLobbyId() {
        packetRegisterPlayerFromClient = new Packets.PacketRegisterPlayerFromClient();
        packetRegisterPlayerFromClient.setPlayerName("foo");
        packetRegisterPlayerFromClient.setLobbyId("bar");
        assertEquals("bar", packetRegisterPlayerFromClient.getLobbyId());
    }

    @Test
    void packetRegisterAIPlayerFromClient() {
        packetRegisterAIPlayerFromClient = new Packets.PacketRegisterAIPlayerFromClient();
    }

    @Test
    void packetRegisterPlayerFromServer() {
        packetRegisterPlayerFromServer = new Packets.PacketRegisterPlayerFromServer();
        packetRegisterPlayerFromServer.setPlayerId("foo");
        assertEquals("foo", packetRegisterPlayerFromServer.getPlayerId());
    }

    @Test
    void packetServerSendDTOGameboardData() {
        packetServerSendDTOGameboardData = new Packets.PacketServerSendDTOGameboardData();
        packetServerSendDTOGameboardData.setDTOGameboardDataJsonString("foo");
        assertEquals("foo", packetServerSendDTOGameboardData.getDTOGameboardDataJsonString());
    }

    @Test
    void packetServerSendDTOGameboardDataType() {
        packetServerSendDTOGameboardData = new Packets.PacketServerSendDTOGameboardData();
        packetServerSendDTOGameboardData.setTypeOfDTOGameboardData("foo");
        assertEquals("foo", packetServerSendDTOGameboardData.getTypeOfDTOGameboardData());
    }

    @Test
    void packetServerSendDTOGameplayAction() {
        packetServerSendDTOGameplayAction = new Packets.PacketServerSendDTOGameplayAction();
        packetServerSendDTOGameplayAction.setDTOGameplayActionJsonString("foo");
        assertEquals("foo", packetServerSendDTOGameplayAction.getDTOGameplayActionJsonString());
    }

    @Test
    void packetServerSendDTOGameplayActionType() {
        packetServerSendDTOGameplayAction = new Packets.PacketServerSendDTOGameplayAction();
        packetServerSendDTOGameplayAction.setTypeOfDTOGameplayAction("foo");
        assertEquals("foo", packetServerSendDTOGameplayAction.getTypeOfDTOGameplayAction());
    }

    @Test
    void packetServerSendPlayerReadyObjectsJson() {
        packetServerSendPlayerReadyObjectsJson = new Packets.PacketServerSendPlayerReadyObjectsJson();
        packetServerSendPlayerReadyObjectsJson.setPlayerReadyObjectsJsonString("foo");
        assertEquals("foo", packetServerSendPlayerReadyObjectsJson.getPlayerReadyObjectsJsonString());
    }

    @Test
    void packetServerReceiveDTOGameplayAction() {
        packetServerReceiveDTOGameplayAction = new Packets.PacketServerReceiveDTOGameplayAction();
        packetServerReceiveDTOGameplayAction.setDTOGameplayActionJsonString("foo");
        assertEquals("foo", packetServerReceiveDTOGameplayAction.getDTOGameplayActionJsonString());
    }

    @Test
    void packetServerReceiveDTOGameplayActionType() {
        packetServerReceiveDTOGameplayAction = new Packets.PacketServerReceiveDTOGameplayAction();
        packetServerReceiveDTOGameplayAction.setTypeOfDTOGameplayAction("bar");
        assertEquals("bar", packetServerReceiveDTOGameplayAction.getTypeOfDTOGameplayAction());
    }

    @Test
    void packetServerReceivePlayerReadyObject() {
        packetServerReceivePlayerReadyObject = new Packets.PacketServerReceivePlayerReadyObject();
        packetServerReceivePlayerReadyObject.setPlayerReadyObjectDTOString("foo");
        assertEquals("foo", packetServerReceivePlayerReadyObject.getPlayerReadyObjectDTOString());
    }

    @Test
    void packetServerSendGameStart() {
        packetServerSendGameStart = new Packets.PacketServerSendGameStart();
        packetServerSendGameStart.setGameStatus("foo");
        assertEquals("foo", packetServerSendGameStart.getGameStatus());
    }







    @Test
    void packetSimpleMessageLobbyId() {
        packetSimpleMessage = new Packets.PacketSimpleMessage();
        packetSimpleMessage.setChatMessage("foo");
        packetSimpleMessage.setLobbyId("bar");
        assertEquals("bar", packetSimpleMessage.getLobbyId());
    }

    @Test
    void packetRegisterPlayerFromClientPlayerNameLobbyId() {
        packetRegisterPlayerFromClient = new Packets.PacketRegisterPlayerFromClient();
        packetRegisterPlayerFromClient.setPlayerName("foo");
        packetRegisterPlayerFromClient.setLobbyId("bar");
        assertEquals("bar", packetRegisterPlayerFromClient.getLobbyId());
    }

    @Test
    void packetRegisterAIPlayerFromClientLobbyId() {
        packetRegisterAIPlayerFromClient = new Packets.PacketRegisterAIPlayerFromClient();
        packetRegisterAIPlayerFromClient.setLobbyId("foo");
        assertEquals("foo", packetRegisterAIPlayerFromClient.getLobbyId());
    }

    @Test
    void packetRegisterPlayerFromServerLobbyId() {
        packetRegisterPlayerFromServer = new Packets.PacketRegisterPlayerFromServer();
        packetRegisterPlayerFromServer.setLobbyId("foo");
        assertEquals("foo", packetRegisterPlayerFromServer.getLobbyId());
    }

    @Test
    void packetServerSendDTOGameboardDataLobbyId() {
        packetServerSendDTOGameboardData = new Packets.PacketServerSendDTOGameboardData();
        packetServerSendDTOGameboardData.setLobbyId("foo");
        assertEquals("foo", packetServerSendDTOGameboardData.getLobbyId());
    }

    @Test
    void packetServerSendDTOGameboardDataTypeLobbyId() {
        packetServerSendDTOGameboardData = new Packets.PacketServerSendDTOGameboardData();
        packetServerSendDTOGameboardData.setLobbyId("foo");
        assertEquals("foo", packetServerSendDTOGameboardData.getLobbyId());
    }

    @Test
    void packetServerSendDTOGameplayActionLobbyId() {
        packetServerSendDTOGameplayAction = new Packets.PacketServerSendDTOGameplayAction();
        packetServerSendDTOGameplayAction.setLobbyId("foo");
        assertEquals("foo", packetServerSendDTOGameplayAction.getLobbyId());
    }

    @Test
    void packetServerSendDTOGameplayActionTypeLobbyId() {
        packetServerSendDTOGameplayAction = new Packets.PacketServerSendDTOGameplayAction();
        packetServerSendDTOGameplayAction.setLobbyId("foo");
        assertEquals("foo", packetServerSendDTOGameplayAction.getLobbyId());
    }

    @Test
    void packetServerSendPlayerReadyObjectsJsonLobbyId() {
        packetServerSendPlayerReadyObjectsJson = new Packets.PacketServerSendPlayerReadyObjectsJson();
        packetServerSendPlayerReadyObjectsJson.setLobbyId("foo");
        assertEquals("foo", packetServerSendPlayerReadyObjectsJson.getLobbyId());
    }

    @Test
    void packetServerReceiveDTOGameplayActionLobbyId() {
        packetServerReceiveDTOGameplayAction = new Packets.PacketServerReceiveDTOGameplayAction();
        packetServerReceiveDTOGameplayAction.setLobbyId("foo");
        assertEquals("foo", packetServerReceiveDTOGameplayAction.getLobbyId());
    }

    @Test
    void packetServerReceiveDTOGameplayActionTypeLobbyId() {
        packetServerReceiveDTOGameplayAction = new Packets.PacketServerReceiveDTOGameplayAction();
        packetServerReceiveDTOGameplayAction.setLobbyId("bar");
        assertEquals("bar", packetServerReceiveDTOGameplayAction.getLobbyId());
    }

    @Test
    void packetServerReceivePlayerReadyObjectLobbyId() {
        packetServerReceivePlayerReadyObject = new Packets.PacketServerReceivePlayerReadyObject();
        packetServerReceivePlayerReadyObject.setLobbyId("foo");
        assertEquals("foo", packetServerReceivePlayerReadyObject.getLobbyId());
    }

    @Test
    void packetServerSendGameStartLobbyId() {
        packetServerSendGameStart = new Packets.PacketServerSendGameStart();
        packetServerSendGameStart.setLobbyId("foo");
        assertEquals("foo", packetServerSendGameStart.getLobbyId());
    }
}
