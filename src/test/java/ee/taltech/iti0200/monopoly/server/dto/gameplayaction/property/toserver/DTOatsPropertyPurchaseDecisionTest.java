package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOatsPropertyPurchaseDecisionTest {
    private DTOatsPropertyPurchaseDecision dto;

    @BeforeEach
    void setUp() {
        dto = new DTOatsPropertyPurchaseDecision();
        dto.setPlayerId("FooId");
        dto.setConfirmPurchase(true);
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void isConfirmPurchase() {
        assertTrue(dto.isConfirmPurchase());
    }
}