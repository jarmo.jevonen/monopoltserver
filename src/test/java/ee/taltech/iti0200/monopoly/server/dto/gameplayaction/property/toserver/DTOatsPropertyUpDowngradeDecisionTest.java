package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOatsPropertyUpDowngradeDecisionTest {
    private DTOatsPropertyUpDowngradeDecision dto;

    @BeforeEach
    void setUp() {
        dto = new DTOatsPropertyUpDowngradeDecision();
        dto.setPlayerId("FooId");
        dto.setPropertyIndex(6);
        dto.setUpgrade(true);
    }

    @Test
    void getPropertyIndex() {
        assertEquals(6, dto.getPropertyIndex());
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void isUpgrade() {
        assertTrue(dto.isUpgrade());
    }
}