package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsMoneyPayMoneyToBankTest {
    private DTOafsMoneyPayMoneyToBank dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsMoneyPayMoneyToBank();
        dto.setPlayerId("FooId");
        dto.setPlayerNewBalance(100);
        dto.setPaidAmount(30);
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void getPaidAmount() {
        assertEquals(30, dto.getPaidAmount());
    }

    @Test
    void getPlayerNewBalance() {
        assertEquals(100, dto.getPlayerNewBalance());
    }
}