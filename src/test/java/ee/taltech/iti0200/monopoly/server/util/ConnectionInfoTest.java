package ee.taltech.iti0200.monopoly.server.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConnectionInfoTest {
    private ConnectionInfo connectionInfo;

    @BeforeEach
    void setUp() {
        connectionInfo = new ConnectionInfo("Foo", "Bar", "FooBar");
    }

    @Test
    void getAuthtoken() {
        assertEquals("Foo", connectionInfo.getAuthtoken());
    }

    @Test
    void getIp() {
        assertEquals("Bar", connectionInfo.getIp());
    }

    @Test
    void getPort() {
        assertEquals("FooBar", connectionInfo.getPort());
    }
}