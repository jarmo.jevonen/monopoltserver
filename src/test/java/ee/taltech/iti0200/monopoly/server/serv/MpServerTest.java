package ee.taltech.iti0200.monopoly.server.serv;

import com.esotericsoftware.kryonet.Server;
import ee.taltech.iti0200.monopoly.game.logic.gameenigne.GameNetworkingServerLogic;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.dto.DTOPlayerReadyObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MpServerTest {
    private MpServer mpServer;
    private static int port = 27000;
    private String lobbyId;

    @BeforeEach
    void setUp() {
        port++;
        mpServer = new MpServer(port);
        lobbyId = mpServer.createNewLobby(0);
    }

    @Test
    void playerConnected() {
        mpServer.playerConnected(3, lobbyId, "Foo", false);
    }

    @Test
    void playerConnectedAI() {
        mpServer.playerConnected(3, lobbyId, "Foo", true);
    }

    @Test
    void playerConnectedEmptyName() {
        mpServer.playerConnected(3, lobbyId, "", true);
    }

    @Test
    void playerConnectedDuplicateName() {
        mpServer.playerConnected(3, lobbyId, "Foo", true);
        mpServer.playerConnected(3, lobbyId, "Foo", true);
    }

    @Test
    void samePort() {
        assertThrows(RuntimeException.class, () -> mpServer = new MpServer(port));
    }

    @Test
    void getServerLogger() {
        assertEquals(MpServer.getServerLogger(), MpServer.getServerLogger());
    }

    @Test
    void playerDisconnected() {
        String lobbyId = mpServer.createNewLobby(0);
        Player player = new Player();
        player.setConnectionId(1);
        player.setPlayerId("Foo");
        DTOPlayerReadyObject dto = new DTOPlayerReadyObject();
        dto.setPlayerId("Foo");
        mpServer.getLobbiesMap().get(lobbyId).getDtoPlayerReadyObjects().add(dto);
        mpServer.getLobbiesMap().get(lobbyId).setPlayers(new ArrayList<>(List.of(player)));
        mpServer.playerDisconnected(1);
    }

    @Test
    void getServer() {
        Server server = mpServer.getServer();
        assertEquals(server, mpServer.getServer());
    }

    @Test
    void getSnl() {
        ServerNetworkListener snl = mpServer.getSnl();
        assertEquals(snl, mpServer.getSnl());
    }

    @Test
    void getGameNetworkingServerLogic() {
        GameNetworkingServerLogic gameNetworkingServerLogic = mpServer.getLobbiesMap().get(lobbyId);
        assertEquals(gameNetworkingServerLogic, mpServer.getLobbiesMap().get(lobbyId));
    }

    @Test
    void getPortFromArgs1() {
        String[] args = new String[] {"25565"};
        assertEquals(25565, MpServer.getPortFromArgs(args));
    }

    @Test
    void getPortFromArgs2() {
        String[] args = new String[] {};
        assertEquals(25565, MpServer.getPortFromArgs(args));
    }

    @Test
    void getPortFromArgs3() {
        String[] args = new String[] {"49-"};
        assertThrows(IllegalArgumentException.class, () -> MpServer.getPortFromArgs(args));
    }

    @Test
    void getPortFromArgs4() {
        String[] args = new String[] {"abc"};
        assertThrows(IllegalArgumentException.class, () -> MpServer.getPortFromArgs(args));
    }

    @Test
    void getPortFromArgs5() {
        String[] args = new String[] {"255658"};
        assertThrows(IllegalArgumentException.class, () -> MpServer.getPortFromArgs(args));
    }

    @Test
    void getPortFromArgs6() {
        String[] args = new String[] {"-578"};
        assertThrows(IllegalArgumentException.class, () -> MpServer.getPortFromArgs(args));
    }

    @Test
    void getPortFromArgs7() {
        String[] args = new String[] {"80000"};
        assertThrows(IllegalArgumentException.class, () -> MpServer.getPortFromArgs(args));
    }

    @Test
    void getPortFromArgs9() {
        String[] args = new String[] {"40000", "36888"};
        assertThrows(IllegalArgumentException.class, () -> MpServer.getPortFromArgs(args));
    }

    @Test
    void allConnectionsClosed() {
        mpServer.allConnectionsClosedInLobby(lobbyId);
    }

    @Test
    void main() {
        MpServer.main(new String[] {"26200"});
    }


}