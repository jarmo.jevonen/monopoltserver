package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsPropertyUpDowngradePropertyTest {
    private DTOafsPropertyUpDowngradeProperty dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsPropertyUpDowngradeProperty();
        dto.setPropertyIndex(6);
        dto.setUpDowngradeToLevel(4);
    }

    @Test
    void getPropertyIndex() {
        assertEquals(6, dto.getPropertyIndex());
    }

    @Test
    void getUpDowngradeToLevel() {
        assertEquals(4, dto.getUpDowngradeToLevel());
    }
}