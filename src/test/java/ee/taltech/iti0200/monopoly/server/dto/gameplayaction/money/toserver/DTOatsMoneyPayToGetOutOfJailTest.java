package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.toserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOatsMoneyPayToGetOutOfJailTest {
    private DTOatsMoneyPayToGetOutOfJail dto;

    @BeforeEach
    void setUp() {
        dto = new DTOatsMoneyPayToGetOutOfJail();
        dto.setPlayerId("FooId");
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }
}