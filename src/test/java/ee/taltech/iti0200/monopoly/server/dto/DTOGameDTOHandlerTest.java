package ee.taltech.iti0200.monopoly.server.dto;

import ee.taltech.iti0200.monopoly.game.logic.game.Game;
import ee.taltech.iti0200.monopoly.game.logic.gameenigne.GameNetworkingServerLogic;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.fromserver.DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver.DTOatsCardChoice;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver.DTOatsCardUseGetOutOfJailCard;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.die.toserver.DTOatsDieRollTheDice;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.toserver.DTOatsGameStatePlayerEndTurn;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.toserver.DTOatsMoneyPayToGetOutOfJail;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver.DTOatsPropertyPurchaseDecision;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver.DTOatsPropertyUpDowngradeDecision;
import ee.taltech.iti0200.monopoly.server.serv.MpServer;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DTOGameDTOHandlerTest {
    private static int port = 25865;
    private MpServer mpServer;
    private String lobbyId;

    private DTOGameDTOHandler dtoGameDTOHandler;
    private GameNetworkingServerLogic gameNetworkingServerLogic;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        port++;
        mpServer = new MpServer(port);
        lobbyId = mpServer.createNewLobby(0);
        gameNetworkingServerLogic = new GameNetworkingServerLogic(mpServer, lobbyId);
        dtoGameDTOHandler = new DTOGameDTOHandler(gameNetworkingServerLogic);
        gameNetworkingServerLogic.setDtoGameDTOHandler(dtoGameDTOHandler);
        gameNetworkingServerLogic.setGame(new Game(gameNetworkingServerLogic));
        objectMapper = new ObjectMapper();
    }

    @Test
    void sendDTOGameplayPacketBroadcast() {
        dtoGameDTOHandler.sendDTOGameplayPacket(DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount.class.getSimpleName(), "FooDTOJson", 0, true);
    }

    @Test
    void sendDTOGameplayPacketSingleClient() {
        dtoGameDTOHandler.sendDTOGameplayPacket(DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount.class.getSimpleName(), "FooDTOJson", 4, false);
    }

    @Test
    void sendDTOGameDataPacket() {
        dtoGameDTOHandler.sendDTOGameDataPacket("Foo", "Bar");
    }

    @Test
    void sendDTOPlayerReadyObject() {
        dtoGameDTOHandler.sendDTOPlayerReadyObject(new ArrayList<>(List.of(new DTOPlayerReadyObject())));
    }

    @Test
    void receiveDTOGameplayJsonStringsCardChoice() {
        assertThrows(NullPointerException.class, () ->
                dtoGameDTOHandler.receiveDTOGameplayJsonStrings(DTOatsCardChoice.class.getSimpleName(), "{\"playerId\":null,\"pay\":false}", 3)
        );
    }

    @Test
    void receiveDTOGameplayJsonStringsCardUseGetOutOfJailCard() {
        assertThrows(NullPointerException.class, () ->
                dtoGameDTOHandler.receiveDTOGameplayJsonStrings(DTOatsCardUseGetOutOfJailCard.class.getSimpleName(), "{\"playerId\":null,\"useCard\":false}", 3)
        );
    }

    @Test
    void receiveDTOGameplayJsonStringsDieRollTheDice() {
        assertThrows(NullPointerException.class, () ->
                dtoGameDTOHandler.receiveDTOGameplayJsonStrings(DTOatsDieRollTheDice.class.getSimpleName(), "{\"playerId\":null}", 3)
        );
    }

    @Test
    void receiveDTOGameplayJsonStringsGameStateEndTurn() {
        try {
            dtoGameDTOHandler.receiveDTOGameplayJsonStrings(DTOatsGameStatePlayerEndTurn.class.getSimpleName(), "{\"playerId\":null}", 3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void receiveDTOGameplayJsonStringsMoneyPayToGetOutOfJail() {
        assertThrows(NullPointerException.class, () ->
                dtoGameDTOHandler.receiveDTOGameplayJsonStrings(DTOatsMoneyPayToGetOutOfJail.class.getSimpleName(), "{\"playerId\":null}", 3)
        );
    }

    @Test
    void receiveDTOGameplayJsonStringsPropertyPurchaseDecision() {
        assertThrows(NullPointerException.class, () ->
                dtoGameDTOHandler.receiveDTOGameplayJsonStrings(DTOatsPropertyPurchaseDecision.class.getSimpleName(), "{\"playerId\":null}", 3)
        );
    }

    @Test
    void receiveDTOGameplayJsonStringsPropertyUpDowngradeDecision() {
        assertThrows(NullPointerException.class, () ->
                dtoGameDTOHandler.receiveDTOGameplayJsonStrings(DTOatsPropertyUpDowngradeDecision.class.getSimpleName(), "{\"playerId\":null}", 3)
        );
    }
}