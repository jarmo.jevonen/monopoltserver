package ee.taltech.iti0200.monopoly.server.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOPlayerReadyObjectTest {
    private DTOPlayerReadyObject dtoPlayerReadyObject;

    @BeforeEach
    void setUp() {
        dtoPlayerReadyObject = new DTOPlayerReadyObject("Foo", "Bar", true);
    }

    @Test
    void getPlayerName() {
        assertEquals("Foo", dtoPlayerReadyObject.getPlayerName());
    }

    @Test
    void getPlayerId() {
        assertEquals("Bar", dtoPlayerReadyObject.getPlayerId());
    }

    @Test
    void isReadyStatus() {
        assertTrue(dtoPlayerReadyObject.isReadyStatus());
    }

    @Test
    void setPlayerName() {
        dtoPlayerReadyObject.setPlayerName("Bar");
        assertEquals("Bar", dtoPlayerReadyObject.getPlayerName());
    }

    @Test
    void setPlayerId() {
        dtoPlayerReadyObject.setPlayerId("Foo");
        assertEquals("Foo", dtoPlayerReadyObject.getPlayerId());
    }

    @Test
    void setReadyStatus() {
        dtoPlayerReadyObject.setReadyStatus(false);
        assertFalse(dtoPlayerReadyObject.isReadyStatus());
    }
}