package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsPropertyOfferToBuyTest {
    DTOafsPropertyOfferToBuy dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsPropertyOfferToBuy();
        dto.setPlayerId("FooId");
        dto.setPropertyIndex(10);
    }

    @Test
    void getPropertyIndex() {
        assertEquals(10, dto.getPropertyIndex());
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }
}