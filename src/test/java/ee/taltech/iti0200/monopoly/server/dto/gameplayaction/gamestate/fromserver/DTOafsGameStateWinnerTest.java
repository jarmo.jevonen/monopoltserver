package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsGameStateWinnerTest {
    private DTOafsGameStateWinner dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsGameStateWinner();
        dto.setPlayerId("Foo");
    }

    @Test
    void getPlayerId() {
        assertEquals("Foo", dto.getPlayerId());
    }
}