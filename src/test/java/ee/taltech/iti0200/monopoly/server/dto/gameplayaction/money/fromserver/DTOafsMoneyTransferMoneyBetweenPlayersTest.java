package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsMoneyTransferMoneyBetweenPlayersTest {
    private DTOafsMoneyTransferMoneyBetweenPlayers dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsMoneyTransferMoneyBetweenPlayers();
        dto.setPayerPlayerId("PayerId");
        dto.setPayerPlayerNewBalance(90);
        dto.setPayeePlayerId("PayeeId");
        dto.setPayeePlayerNewBalance(110);
        dto.setTransferredAmount(10);
    }

    @Test
    void getPayerPlayerId() {
        assertEquals("PayerId", dto.getPayerPlayerId());
    }

    @Test
    void getPayeePlayerId() {
        assertEquals("PayeeId", dto.getPayeePlayerId());
    }

    @Test
    void getTransferredAmount() {
        assertEquals(10, dto.getTransferredAmount());
    }

    @Test
    void getPayerPlayerNewBalance() {
        assertEquals(90, dto.getPayerPlayerNewBalance());
    }

    @Test
    void getPayeePlayerNewBalance() {
        assertEquals(110, dto.getPayeePlayerNewBalance());
    }
}