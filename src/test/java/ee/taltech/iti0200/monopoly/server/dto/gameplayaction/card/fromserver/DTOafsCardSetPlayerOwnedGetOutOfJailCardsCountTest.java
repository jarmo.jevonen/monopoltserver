package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsCardSetPlayerOwnedGetOutOfJailCardsCountTest {
    private DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount();
        dto.setGetOutOfJailCardsCount(5);
        dto.setPlayerId("FooId");
    }

    @Test
    void getGetOutOfJailCardsCount() {
        assertEquals(5, dto.getGetOutOfJailCardsCount());
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }
}