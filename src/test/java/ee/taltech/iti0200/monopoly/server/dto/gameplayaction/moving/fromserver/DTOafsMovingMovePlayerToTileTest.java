package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.moving.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsMovingMovePlayerToTileTest {
    private DTOafsMovingMovePlayerToTile dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsMovingMovePlayerToTile();
        dto.setPlayerId("FooId");
        dto.setTileIndex(10);
        dto.setToJail(true);
        dto.setMoveBackwards(true);
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void getTileIndex() {
        assertEquals(10, dto.getTileIndex());
    }

    @Test
    void isToJail() {
        assertTrue(dto.isToJail());
    }

    @Test
    void isMoveBackwards() {
        assertTrue(dto.isMoveBackwards());
    }
}