package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsGameStateSetPlayerTurnTest {
    private DTOafsGameStateSetPlayerTurn dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsGameStateSetPlayerTurn();
        dto.setPlayerId("FooId");
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }
}