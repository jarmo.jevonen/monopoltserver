package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsPropertySetPropertyMortgagedStatusTest {
    private DTOafsPropertySetPropertyMortgagedStatus dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsPropertySetPropertyMortgagedStatus();
        dto.setPropertyIndex(6);
        dto.setMortgagedStatus(true);
    }

    @Test
    void getPropertyIndex() {
        assertEquals(6, dto.getPropertyIndex());
    }

    @Test
    void isMortgagedStatus() {
        assertTrue(dto.isMortgagedStatus());
    }
}