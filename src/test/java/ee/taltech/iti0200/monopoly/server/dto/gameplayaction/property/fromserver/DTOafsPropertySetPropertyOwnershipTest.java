package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.fromserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOafsPropertySetPropertyOwnershipTest {
    private DTOafsPropertySetPropertyOwnership dto;

    @BeforeEach
    void setUp() {
        dto = new DTOafsPropertySetPropertyOwnership();
        dto.setPlayerId("FooId");
        dto.setPropertyIndex(6);
    }

    @Test
    void getPropertyIndex() {
        assertEquals(6, dto.getPropertyIndex());
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }
}