package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.die.toserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOatsDieRollTheDiceTest {
    private DTOatsDieRollTheDice dto;

    @BeforeEach
    void setUp() {
        dto = new DTOatsDieRollTheDice();
        dto.setPlayerId("FooId");
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }
}