package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOatsCardChoiceTest {
    private DTOatsCardChoice dto;

    @BeforeEach
    void setUp() {
        dto = new DTOatsCardChoice();
        dto.setPay(true);
        dto.setPlayerId("FooId");
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }

    @Test
    void isPay() {
        assertTrue(dto.isPay());
    }
}