package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.toserver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DTOatsGameStatePlayerEndTurnTest {
    private DTOatsGameStatePlayerEndTurn dto;

    @BeforeEach
    void setUp() {
        dto = new DTOatsGameStatePlayerEndTurn();
        dto.setPlayerId("FooId");
    }

    @Test
    void getPlayerId() {
        assertEquals("FooId", dto.getPlayerId());
    }
}