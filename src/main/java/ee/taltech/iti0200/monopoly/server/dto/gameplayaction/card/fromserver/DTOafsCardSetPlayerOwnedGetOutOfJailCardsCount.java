package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.fromserver;

import ee.taltech.iti0200.monopoly.game.card.Card;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount {
    private int getOutOfJailCardsCount;
    private String playerId;
}
