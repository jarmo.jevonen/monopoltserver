package ee.taltech.iti0200.monopoly.server.serv;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.packets.Packets;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class ServerNetworkListener extends Listener {

    @Getter
    @Setter
    private MpServer mpServer;

    private static final String snlLoggerPrefix = "SnlLog-> ";
    private int aiNameIndex = 0;

    private final List<String> cpuPlayerNames = new ArrayList<>(List.of("Bob (AI)", "Alice (AI)", "Jim (AI)", "Lucy (AI)", "Carl (AI)"));

    public void connected(Connection connection) {
        MpServer.serverLogger.log(Level.INFO, snlLoggerPrefix + String.format("%sA client has connected.",snlLoggerPrefix));
        connection.setKeepAliveTCP(20000);
        connection.setTimeout(25000);
    }

    public void disconnected(Connection connection) {
        MpServer.serverLogger.log(Level.INFO, String.format("%sConnection with ID: %d disconnected at: %s", snlLoggerPrefix, connection.getID(), System.currentTimeMillis()));

        if (mpServer.getServer().getConnections().length == 0) {
            MpServer.serverLogger.log(Level.INFO, snlLoggerPrefix + String.format("%sAll connections have been closed;", snlLoggerPrefix));
            mpServer.allConnectionsClosed();
        } else {
            mpServer.playerDisconnected(connection.getID());
        }
    }

    public void received(Connection connection, Object object) {
        // Dummy packet for receiving simple messages.
        if (object instanceof Packets.PacketSimpleMessage) {
            Packets.PacketSimpleMessage packetSimpleMessage = (Packets.PacketSimpleMessage) object;
            System.out.println(packetSimpleMessage.getChatMessage());
        }

        // Client sent a packet to register itself.
        else if (object instanceof Packets.PacketServerReceiveCreateLobby) {
            Packets.PacketServerReceiveCreateLobby packetServerReceiveCreateLobby = (Packets.PacketServerReceiveCreateLobby) object;
            mpServer.createNewLobby(connection.getID());
        }

        // Client sent a packet to register itself.
        else if (object instanceof Packets.PacketRegisterPlayerFromClient) {
            Packets.PacketRegisterPlayerFromClient packetRegisterPlayerFromClient = (Packets.PacketRegisterPlayerFromClient) object;
            String lobbyId = packetRegisterPlayerFromClient.getLobbyId();
            String playerName = packetRegisterPlayerFromClient.getPlayerName();
            mpServer.playerConnected(connection.getID(), lobbyId, playerName, false);
        }
        // Client sent a packet to register an AI player.
        else if (object instanceof Packets.PacketRegisterAIPlayerFromClient) {
            Packets.PacketRegisterAIPlayerFromClient packetRegisterAIPlayerFromClient = (Packets.PacketRegisterAIPlayerFromClient) object;
            String lobbyId = packetRegisterAIPlayerFromClient.getLobbyId();
            if (!mpServer.getLobbiesMap().containsKey(lobbyId) || mpServer.getLobbiesMap().get(lobbyId).getPlayers().size() == 6) {
                return;
            }
            mpServer.playerConnected(100, lobbyId, cpuPlayerNames.get(aiNameIndex), true);
            aiNameIndex = (aiNameIndex + 1) % 5;
        }

        // Client has changed its ready-status.
        else if (object instanceof Packets.PacketServerReceivePlayerReadyObject) {
            Packets.PacketServerReceivePlayerReadyObject packetServerReceivePlayerReadyObject = (Packets.PacketServerReceivePlayerReadyObject) object;
            String lobbyId = packetServerReceivePlayerReadyObject.getLobbyId();
            mpServer.getLobbiesMap().get(lobbyId).receivePlayerReadyStatus(connection.getID(), packetServerReceivePlayerReadyObject.getPlayerReadyObjectDTOString());
        }
        // Packet from the server containing DTOGameplayAction
        else if (object instanceof Packets.PacketServerReceiveDTOGameplayAction) {
            Packets.PacketServerReceiveDTOGameplayAction packet = (Packets.PacketServerReceiveDTOGameplayAction) object;
            String lobbyId = packet.getLobbyId();
            try {
                mpServer.getLobbiesMap().get(lobbyId).getDtoGameDTOHandler().receiveDTOGameplayJsonStrings(packet.getTypeOfDTOGameplayAction(), packet.getDTOGameplayActionJsonString(), connection.getID());
            } catch (IOException e) {
                MpServer.serverLogger.log(Level.WARNING,  snlLoggerPrefix + "Unable to parse DTOgameplay Action: " + e);
            }
        }

    }
}
