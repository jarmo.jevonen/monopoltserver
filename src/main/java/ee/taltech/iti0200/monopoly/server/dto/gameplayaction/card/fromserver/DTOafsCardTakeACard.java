package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.fromserver;

import ee.taltech.iti0200.monopoly.game.card.CardType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsCardTakeACard {
    private String playerId;
    private CardType cardType;
    private int cardIndex;
}
