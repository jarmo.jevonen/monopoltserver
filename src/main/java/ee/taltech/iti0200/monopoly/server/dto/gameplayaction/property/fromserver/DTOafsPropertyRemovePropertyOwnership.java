package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class DTOafsPropertyRemovePropertyOwnership {
    private List<Integer> propertyIndices;
    private String playerId;
}
