package ee.taltech.iti0200.monopoly.server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DTOPlayerReadyObject {
    private String playerName;
    private String playerId;
    private boolean readyStatus;
}
