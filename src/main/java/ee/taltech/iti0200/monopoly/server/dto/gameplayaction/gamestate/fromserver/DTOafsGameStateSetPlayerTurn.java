package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.fromserver;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsGameStateSetPlayerTurn {
    private String PlayerId;
}
