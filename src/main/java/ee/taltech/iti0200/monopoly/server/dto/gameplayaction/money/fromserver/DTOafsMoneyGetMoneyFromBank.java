package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsMoneyGetMoneyFromBank {
    private String playerId;
    private double receivedAmount;
    private double playerNewBalance;
}
