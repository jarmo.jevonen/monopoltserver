package ee.taltech.iti0200.monopoly.server.util;

import java.util.Properties;

public class ConnectionUtil {

    public static ConnectionInfo loadConnectionInfo() {
        Properties properties = PropertyLoader.loadApplicationProperties();

        return new ConnectionInfo(
                properties.getProperty("authtoken"),
                properties.getProperty("ip"),
                properties.getProperty("port"));
    }

}
