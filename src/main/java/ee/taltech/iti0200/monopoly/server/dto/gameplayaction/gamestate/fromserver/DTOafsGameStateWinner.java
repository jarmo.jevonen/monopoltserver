package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.fromserver;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DTOafsGameStateWinner {
    private String PlayerId;
}
