package ee.taltech.iti0200.monopoly.server.serv;

import com.esotericsoftware.kryonet.Server;
import ee.taltech.iti0200.monopoly.game.logic.gameenigne.GameNetworkingServerLogic;
import ee.taltech.iti0200.monopoly.game.logic.initializer.PlayersInitializer;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.dto.DTOPlayerReadyObject;
import ee.taltech.iti0200.monopoly.server.packets.Packets;
import ee.taltech.iti0200.monopoly.server.util.ConnectionInfo;
import ee.taltech.iti0200.monopoly.server.util.ConnectionUtil;
import lombok.Getter;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MpServer {
    private static final ConnectionInfo connectionInfo = ConnectionUtil.loadConnectionInfo();
    private static final String serverIp = connectionInfo.getIp();
    private static int serverPort;
    //private static final int serverPort = Integer.parseInt(connectionInfo.getPort());
    @Getter
    protected static final Logger serverLogger = Logger.getLogger(MpServer.class.getName());
    private static final String serverLoggerPrefix = "ServerLog-> ";

    public ObjectMapper objectMapper = new ObjectMapper();

    @Getter
    private Server server;
    @Getter
    private ServerNetworkListener snl;
    //@Getter
    //private GameNetworkingServerLogic gameNetworkingServerLogic;

    @Getter
    private Map<String, GameNetworkingServerLogic> lobbiesMap;

    public MpServer(int portNumber) {
        setUp(portNumber);
        serverLogger.setLevel(Level.WARNING);
    }

    private void setUp(int portNumber) {
        serverPort = portNumber;
        server = null;
        snl = null;
        serverLogger.log(Level.INFO, serverLoggerPrefix + "Setting up the server on port " + serverPort + ".");
        server = new Server(16384, 16384);

        snl = new ServerNetworkListener();
        snl.setMpServer(this);
        server.addListener(snl);
        try {
            server.bind(serverPort);
        } catch (IOException e) {
            throw new RuntimeException(e + String.format(" Unable to bind port %d.", serverPort));
        }
        Packets.RegisterPackets(server.getKryo());
        server.start();
        lobbiesMap = new HashMap<>();
    }

    // Called by the ServerNetworkListener when a player connects. Creates a new Player instance, sends its generated playerID to the client and broadcasts the players list.
    public void playerConnected(int connectionId, String lobbyId, String playerName, boolean isAI) {
        if (server != null && !lobbiesMap.containsKey(lobbyId)) {
            new ArrayList<>(List.of(server.getConnections())).stream().filter(connection -> connection.getID() == connectionId).findAny().get().close();
            return;
        }
        GameNetworkingServerLogic lobbyGameNetworkingServerLogic = lobbiesMap.get(lobbyId);

        Player newPlayer = new Player();
        if (playerName == null || playerName.isEmpty()) {
            playerName = "FooBar " + (lobbyGameNetworkingServerLogic.getPlayers().size() + 1);
        }
        String finalPlayerName = playerName;
        if (lobbyGameNetworkingServerLogic.getPlayers().stream().map(player -> player.getName().toLowerCase()).anyMatch(pName -> pName.equals(finalPlayerName.toLowerCase()))) {
            playerName = finalPlayerName + " " + (lobbyGameNetworkingServerLogic.getPlayers().stream().map(player -> player.getName().toLowerCase()).filter(pName -> pName.equals(finalPlayerName.toLowerCase())).count() + 1);
        }
        newPlayer.setName(playerName);
        if (isAI) {
            newPlayer.setAI(true);
        }
        newPlayer.setConnectionId(connectionId);
        lobbyGameNetworkingServerLogic.getPlayers().add(PlayersInitializer.initializeAndReturnPlayer(newPlayer));
        if (!isAI) {
            lobbyGameNetworkingServerLogic.sendAPlayerIdToTheConnectedClient(connectionId, newPlayer.getPlayerId());
            lobbyGameNetworkingServerLogic.getDtoPlayerReadyObjects().add(new DTOPlayerReadyObject(newPlayer.getName(), newPlayer.getPlayerId(), false));
        } else {
            lobbyGameNetworkingServerLogic.getDtoPlayerReadyObjects().add(new DTOPlayerReadyObject(newPlayer.getName(), newPlayer.getPlayerId(), true));
        }
        lobbyGameNetworkingServerLogic.broadcastPlayerReadyStatuses();
    }


    public static void main(String[] args) {
        new MpServer(getPortFromArgs(args));
    }

    public static int getPortFromArgs(String[] args) {
        if (args.length > 1) {
            throw new IllegalArgumentException("Port number needs to be specified by a single argument or be left empty for the default port 25565.");
        }
        if (args.length == 0) {
            return 25565;
        }
        return parsePortArg(args[0]);
    }

    private static int parsePortArg(String portString) {
        if (!portString.matches("[0-9]+") || portString.length() > 5) {
            throw new IllegalArgumentException("Given port number is in invalid format.");
        }
        int port =  Integer.parseInt(portString);
        if (port < 1 || port > 65535) {
            throw new IllegalArgumentException("Given port number is invalid.");
        }
        return port;
    }


    public void allConnectionsClosedInLobby(String lobbyId) {
        serverLogger.log(Level.INFO, serverLoggerPrefix + "All connections closed in lobby: " + lobbyId);
        GameNetworkingServerLogic lobbyGameNetworkingServerLogic = lobbiesMap.get(lobbyId);
        lobbyGameNetworkingServerLogic.endGameGameInProgress();
    }


    public String createNewLobby(int connectionId) {
        String newLobbyId = String.valueOf(new Random().nextInt((999999 - 100000) + 1) + 100000);
        while (lobbiesMap.containsKey(newLobbyId)) newLobbyId = String.valueOf(new Random().nextInt((999999 - 100000) + 1) + 100000);
        GameNetworkingServerLogic newLobbyGameNetworkingServerLogic = new GameNetworkingServerLogic(this, newLobbyId);
        lobbiesMap.put(newLobbyId, newLobbyGameNetworkingServerLogic);
        Packets.PacketServerSendLobbyCreated packet = new Packets.PacketServerSendLobbyCreated();
        packet.setLobbyId(newLobbyId);
        server.sendToTCP(connectionId, packet);
        return newLobbyId;
    }

    public void playerDisconnected(int connectionId) {
        lobbiesMap.values()
                .forEach(gameNetworkingServerLogic -> {
            if (gameNetworkingServerLogic.getPlayers().stream()
                    .map(Player::getConnectionId)
                    .anyMatch(conId -> conId.equals(connectionId))) {
                gameNetworkingServerLogic.playerDisconnected(connectionId);
                if (gameNetworkingServerLogic.getPlayers().stream().allMatch(Player::isAI)) {
                    allConnectionsClosedInLobby(gameNetworkingServerLogic.getLobbyId());
                }
            }
        });
    }

    public void allConnectionsClosed() {
        serverLogger.log(Level.WARNING, serverLoggerPrefix + "All connections closed, restarting the server.");
        server.close();
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setUp(serverPort);
    }

}
