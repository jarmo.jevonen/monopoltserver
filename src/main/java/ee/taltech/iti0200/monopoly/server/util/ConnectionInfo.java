package ee.taltech.iti0200.monopoly.server.util;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class ConnectionInfo {
    String authtoken;
    String ip;
    String port;
}
