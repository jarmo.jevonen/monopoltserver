package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn {
    private String PlayerId;
    private boolean allowRollingTheDice;
    private boolean allowPaying;
    private boolean allowUsingACard;
}
