package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.fromserver;

import ee.taltech.iti0200.monopoly.game.card.CardType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsCardOfferSwitchACardOrPay {
    private String playerId;
    private CardType currentCardType;
    private CardType anotherCardType;
    private double amountToPay;
}
