package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.toserver;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOatsGameStatePlayerBankruptcyAcknowledgement {
    private String PlayerId;
}
