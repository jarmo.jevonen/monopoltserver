package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOatsPropertyPurchaseDecision {
    private String playerId;
    private boolean confirmPurchase;
}
