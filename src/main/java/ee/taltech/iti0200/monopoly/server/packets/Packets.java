package ee.taltech.iti0200.monopoly.server.packets;

import com.esotericsoftware.kryo.Kryo;
import lombok.Data;

public class Packets {

    // Register all the packages that are to be sent or received. NB: Keep in sync with the equivalent class in the client.
    public static void RegisterPackets(Kryo kryo) {
        kryo.register(PacketServerReceiveCreateLobby.class);
        kryo.register(PacketServerSendLobbyCreated.class);
        kryo.register(PacketSimpleMessage.class);
        kryo.register(PacketRegisterPlayerFromClient.class);
        kryo.register(PacketRegisterAIPlayerFromClient.class);
        kryo.register(PacketRegisterPlayerFromServer.class);
        kryo.register(PacketServerSendPlayerReadyObjectsJson.class);
        kryo.register(PacketServerReceivePlayerReadyObject.class);
        kryo.register(PacketServerSendDTOGameplayAction.class);
        kryo.register(PacketServerReceiveDTOGameplayAction.class);
        kryo.register(PacketServerSendDTOGameboardData.class);
        kryo.register(PacketServerSendGameStart.class);
    }

    @Data
    public static class PacketServerReceiveCreateLobby {
    }

    @Data
    public static class PacketServerSendLobbyCreated {
        private String lobbyId;
    }

    @Data
    public static class PacketSimpleMessage {
        private String chatMessage;
        private String lobbyId;
    }

    @Data
    public static class PacketRegisterPlayerFromClient {
        private String playerName;
        private String lobbyId;
    }

    @Data
    public static class PacketRegisterAIPlayerFromClient {
        private String lobbyId;
    }

    @Data
    public static class PacketRegisterPlayerFromServer {
        private String playerId;
        private String lobbyId;
    }

    @Data
    public static class PacketServerSendGameStart {
        private String gameStatus;
        private String lobbyId;
    }

    @Data
    public static class PacketServerSendPlayerReadyObjectsJson {
        private String playerReadyObjectsJsonString;
        private String lobbyId;
    }

    @Data
    public static class PacketServerReceivePlayerReadyObject {
        private String playerReadyObjectDTOString;
        private String lobbyId;
    }


    @Data
    public static class PacketServerSendDTOGameplayAction {
        private String typeOfDTOGameplayAction;
        private String DTOGameplayActionJsonString;
        private String lobbyId;
    }

    @Data
    public static class PacketServerReceiveDTOGameplayAction {
        private String typeOfDTOGameplayAction;
        private String DTOGameplayActionJsonString;
        private String lobbyId;
    }

    @Data
    public static class PacketServerSendDTOGameboardData {
        private String typeOfDTOGameboardData;
        private String DTOGameboardDataJsonString;
        private String lobbyId;
    }
}
