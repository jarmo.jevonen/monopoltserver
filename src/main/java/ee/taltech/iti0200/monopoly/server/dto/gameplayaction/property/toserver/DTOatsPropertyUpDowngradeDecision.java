package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOatsPropertyUpDowngradeDecision {
    private String playerId;
    private int propertyIndex;
    // true for upgrading, false for downgrading
    private boolean upgrade;
}
