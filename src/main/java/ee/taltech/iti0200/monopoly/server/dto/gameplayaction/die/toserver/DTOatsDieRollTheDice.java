package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.die.toserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOatsDieRollTheDice {
    private String playerId;
}
