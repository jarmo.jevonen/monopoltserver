package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOatsCardUseGetOutOfJailCard {
    private String playerId;
    private boolean useCard;
}
