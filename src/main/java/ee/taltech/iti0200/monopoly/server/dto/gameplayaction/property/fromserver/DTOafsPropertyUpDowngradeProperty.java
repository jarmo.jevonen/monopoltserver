package ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.fromserver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class DTOafsPropertyUpDowngradeProperty {
    private int propertyIndex;
    private int upDowngradeToLevel;
}
