package ee.taltech.iti0200.monopoly.server.dto;

import ee.taltech.iti0200.monopoly.game.logic.gameenigne.GameNetworkingServerLogic;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver.DTOatsCardChoice;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver.DTOatsCardUseGetOutOfJailCard;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.die.toserver.DTOatsDieRollTheDice;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.toserver.DTOatsGameStatePlayerBankruptcyAcknowledgement;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.toserver.DTOatsGameStatePlayerEndTurn;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.toserver.DTOatsMoneyPayToGetOutOfJail;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver.DTOatsPropertyPurchaseDecision;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver.DTOatsPropertyUpDowngradeDecision;
import ee.taltech.iti0200.monopoly.server.packets.Packets;
import ee.taltech.iti0200.monopoly.server.serv.MpServer;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

public class DTOGameDTOHandler {

    private GameNetworkingServerLogic gameNetworkingServerLogic;

    private ObjectMapper objectMapper = new ObjectMapper();
    private static final String SERVER_GAME_DTO_SENDER_LOGGER_PREFIX = "SgDTO-Sender-> ";

    public DTOGameDTOHandler(GameNetworkingServerLogic gameNetworkingServerLogic) {
        this.gameNetworkingServerLogic = gameNetworkingServerLogic;
    }

    public void sendDTOGameplayPacket(String typeOfDTOGameplayAction, String dTOGameplayActionJsonString, int connectionId, boolean broadcastToAll) {
        MpServer.getServerLogger().log(Level.INFO, SERVER_GAME_DTO_SENDER_LOGGER_PREFIX + "Broadcasting: '" + broadcastToAll + "', Connection ID: " + connectionId + ", DTO type: " + typeOfDTOGameplayAction + ", ActionString: " + dTOGameplayActionJsonString);
        Packets.PacketServerSendDTOGameplayAction packet = new Packets.PacketServerSendDTOGameplayAction();
        packet.setTypeOfDTOGameplayAction(typeOfDTOGameplayAction);
        packet.setDTOGameplayActionJsonString(dTOGameplayActionJsonString);
        packet.setLobbyId(gameNetworkingServerLogic.getLobbyId());
        if (connectionId == 0 && broadcastToAll) {
            //gameNetworkingServerLogic.getMpServer().getServer().sendToAllTCP(packet);
            sendToAll(gameNetworkingServerLogic.getPlayers(), packet);

        } else {
            gameNetworkingServerLogic.getMpServer().getServer().sendToTCP(connectionId, packet);
        }
    }

    public void sendDTOGameDataPacket(String typeOfDTOGameboardData, String DTOGameboardDataJsonString) {
        MpServer.getServerLogger().log(Level.CONFIG, SERVER_GAME_DTO_SENDER_LOGGER_PREFIX + "DTO type: " + typeOfDTOGameboardData + ", BoardDataString: " + DTOGameboardDataJsonString);
        Packets.PacketServerSendDTOGameboardData packet = new Packets.PacketServerSendDTOGameboardData();
        packet.setTypeOfDTOGameboardData(typeOfDTOGameboardData);
        packet.setDTOGameboardDataJsonString(DTOGameboardDataJsonString);
        packet.setLobbyId(gameNetworkingServerLogic.getLobbyId());
        //gameNetworkingServerLogic.getMpServer().getServer().sendToAllTCP(packet);
        sendToAll(gameNetworkingServerLogic.getPlayers(), packet);

    }

    public void sendDTOPlayerReadyObject(List<DTOPlayerReadyObject> dtoPlayerReadyObjects) {
        Packets.PacketServerSendPlayerReadyObjectsJson packetServerSendPlayerReadyObjectsJson = new Packets.PacketServerSendPlayerReadyObjectsJson();
        try {
            packetServerSendPlayerReadyObjectsJson.setPlayerReadyObjectsJsonString(objectMapper.writeValueAsString(dtoPlayerReadyObjects));
            packetServerSendPlayerReadyObjectsJson.setLobbyId(gameNetworkingServerLogic.getLobbyId());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //gameNetworkingServerLogic.getMpServer().getServer().sendToAllTCP(packetServerSendPlayerReadyObjectsJson);
        sendToAll(gameNetworkingServerLogic.getPlayers(), packetServerSendPlayerReadyObjectsJson);
    }


    public void sendToAll(List<Player> players, Object packet) {
        for (Player player : players) {
            gameNetworkingServerLogic.getMpServer().getServer().sendToTCP(player.getConnectionId(), packet);
        }
    }


    public void receiveDTOGameplayJsonStrings(String typeOfDTOGameplayAction, String dTOGameplayActionJsonString, int connectionId) throws IOException {
        MpServer.getServerLogger().log(Level.INFO, SERVER_GAME_DTO_SENDER_LOGGER_PREFIX + "Received: " + typeOfDTOGameplayAction + ", with JSON: " + dTOGameplayActionJsonString);

        // dto.gameplayaction.card.toserver
        if (DTOatsCardChoice.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOatsCardChoice dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOatsCardChoice.class);
            gameNetworkingServerLogic.receiveCardChoice(dto, connectionId);
        }
        else if (DTOatsCardUseGetOutOfJailCard.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOatsCardUseGetOutOfJailCard dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOatsCardUseGetOutOfJailCard.class);
            gameNetworkingServerLogic.receiveCardUseGetOutOfJailCard(dto, connectionId);
        }

        // dto.gameplayaction.die.toserver
        else if (DTOatsDieRollTheDice.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOatsDieRollTheDice dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOatsDieRollTheDice.class);
            gameNetworkingServerLogic.receiveDieRollTheDice(dto, connectionId);
        }

        // dto.gameplayaction.gamestate.toserver
        else if (DTOatsGameStatePlayerBankruptcyAcknowledgement.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOatsGameStatePlayerBankruptcyAcknowledgement dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOatsGameStatePlayerBankruptcyAcknowledgement.class);
            gameNetworkingServerLogic.receiveGameStatePlayerBankruptcyAcknowledgement(dto, connectionId);
        }
        else if (DTOatsGameStatePlayerEndTurn.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOatsGameStatePlayerEndTurn dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOatsGameStatePlayerEndTurn.class);
            gameNetworkingServerLogic.receiveGameStatePlayerEndTurn(dto, connectionId);
        }

        // dto.gameplayaction.money.toserver
        else if (DTOatsMoneyPayToGetOutOfJail.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOatsMoneyPayToGetOutOfJail dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOatsMoneyPayToGetOutOfJail.class);
            gameNetworkingServerLogic.receiveMoneyPayToGetOutOfJail(dto, connectionId);
        }

        // dto.gameplayaction.property.toserver
        else if (DTOatsPropertyPurchaseDecision.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOatsPropertyPurchaseDecision dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOatsPropertyPurchaseDecision.class);
            gameNetworkingServerLogic.receivePropertyPurchaseDecision(dto, connectionId);
        }
        else if (DTOatsPropertyUpDowngradeDecision.class.getSimpleName().equals(typeOfDTOGameplayAction)) {
            DTOatsPropertyUpDowngradeDecision dto = objectMapper.readValue(dTOGameplayActionJsonString, DTOatsPropertyUpDowngradeDecision.class);
            gameNetworkingServerLogic.receivePropertyUpDowngradeDecision(dto, connectionId);
        }
    }
}
