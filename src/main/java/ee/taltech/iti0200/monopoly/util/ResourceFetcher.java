package ee.taltech.iti0200.monopoly.util;

import ee.taltech.iti0200.monopoly.game.gamedataloader.TileLoader;

import java.io.InputStream;

public class ResourceFetcher {

    public static InputStream getResourceFileAsInputStream(String fileName) {
        ClassLoader classLoader = TileLoader.class.getClassLoader();
        return classLoader.getResourceAsStream(fileName);
    }

}
