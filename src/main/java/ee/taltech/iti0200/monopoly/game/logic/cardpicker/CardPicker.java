package ee.taltech.iti0200.monopoly.game.logic.cardpicker;

import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.card.Deck;

import java.util.Random;

public class CardPicker implements ICardPicker {

    @Override
    public Card pickACard(Deck deck, CardType cardType) {
        if (cardType.equals(CardType.CHANCE)) {
            return deck.getChanceCards().get(new Random().nextInt(deck.getChanceCards().size()));
        }
        return deck.getCommunityChestCards().get(new Random().nextInt(deck.getCommunityChestCards().size()));
    }
}
