package ee.taltech.iti0200.monopoly.game.player;

import ee.taltech.iti0200.monopoly.game.board.tile.PropertyTile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.List;

@Data
public class Player {
    String playerId;
    double balance;
    @JsonIgnore
    List<PropertyTile> ownedPropertyTiles;
    List<Card> ownedCards;
    int sequentialDoublesCount;
    int position;
    String name;
    boolean inJail;
    int connectionId;
    boolean isAI;
    boolean hasGoneBankrupt;
}
