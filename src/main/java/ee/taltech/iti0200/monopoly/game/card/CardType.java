package ee.taltech.iti0200.monopoly.game.card;

public enum CardType {
    CHANCE,
    COMMUNITY_CHEST
}
