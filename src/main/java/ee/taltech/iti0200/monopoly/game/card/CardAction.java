package ee.taltech.iti0200.monopoly.game.card;

public enum CardAction {
    GO_TO_JAIL,
    GO_TO_POS,
    GO_STEPS,
    GET_OUT_OF_JAIL,
    PAY_OR_SWITCH,
    PAY_BANK,
    RECEIVE_MONEY_FROM_PLAYERS,
    RECEIVE_MONEY_FROM_BANK,
    MAINTENANCE_OR_REPAIRS
}
