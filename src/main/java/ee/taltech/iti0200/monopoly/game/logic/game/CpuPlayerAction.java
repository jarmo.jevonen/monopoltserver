package ee.taltech.iti0200.monopoly.game.logic.game;

public enum CpuPlayerAction {
    PURCHASE_PROPERTY,
    MORTGAGE_PROPERTY,
    UNMORTGAGE_PROPERTY,
    UPGRADE_PROPERTY,
    DOWNGRADE_PROPERTY,
    GET_OUT_OF_JAIL_PAY,
    GET_OUT_OF_JAIL_USE_CARD,
    GET_OUT_OF_JAIL_ROLL,
    CARD_SWITCH,
    ROLL
}
