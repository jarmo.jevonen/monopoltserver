package ee.taltech.iti0200.monopoly.game.board.tile;

public enum PropertyColor {
    BROWN,
    LIGHT_BLUE,
    MAGENTA,
    ORANGE,
    RED,
    YELLOW,
    GREEN,
    DARK_BLUE,
    RAILROAD,
    UTILITY
}
