package ee.taltech.iti0200.monopoly.game.board.tile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class SpecialTile extends Tile {

    private SpecialTileType specialTileType;

    @Override
    public String toString() {
        return String.format("%d) %s, %s", getPosition(), getTileName(), getSpecialTileType());
    }

}
