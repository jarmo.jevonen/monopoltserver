package ee.taltech.iti0200.monopoly.game.gamedataloader;

import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static ee.taltech.iti0200.monopoly.util.ResourceFetcher.getResourceFileAsInputStream;

public class CardLoader {

    private static final String CHANCE_CARD_DATA_FILE = "chancecarddata.txt";
    private static final String COMMUNITY_CHEST_CARD_DATA_FILE = "communitychestcarddata.txt";

    public static List<Card> loadCardsFromFiles(CardType cardType) {
        InputStream inputStream;
        if (cardType.equals(CardType.CHANCE)) {
            inputStream = getResourceFileAsInputStream(CHANCE_CARD_DATA_FILE);
        } else if (cardType.equals(CardType.COMMUNITY_CHEST)) {
            inputStream = getResourceFileAsInputStream(COMMUNITY_CHEST_CARD_DATA_FILE);
        } else {
            throw new RuntimeException("Unknown card type requested.");
        }

        if (inputStream == null) {
            throw new RuntimeException("Could not find cards data file.");
        }
        Scanner s = new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
        String tilesString = s.hasNext() ? s.next() : "";
        String[] lines = tilesString.split("\\r?\\n");

        List<Card> cards = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (String line : lines) {
            try {
                cards.add(objectMapper.readValue(line, Card.class));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return cards;
    }

    public static String getCardsStringFromFiles(CardType cardType) {
        InputStream inputStream;
        if (cardType.equals(CardType.CHANCE)) {
            inputStream = getResourceFileAsInputStream(CHANCE_CARD_DATA_FILE);
        } else if (cardType.equals(CardType.COMMUNITY_CHEST)) {
            inputStream = getResourceFileAsInputStream(COMMUNITY_CHEST_CARD_DATA_FILE);
        } else {
            throw new RuntimeException("Unknown card type requested.");
        }

        if (inputStream == null) {
            throw new RuntimeException("Could not find cards data file.");
        }
        Scanner s = new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}
