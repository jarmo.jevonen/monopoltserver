package ee.taltech.iti0200.monopoly.game.logic.diceroller;

import ee.taltech.iti0200.monopoly.game.dice.Dice;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Random;

@Data
public class FakeDiceRoller implements IDiceRoller {
    private static final int DIE_FACES_COUNT = 6;
    private List<int[]> diceRolls;
    private Random random = new Random();

    public FakeDiceRoller(List<int[]> diceRolls, int randomMovesCountAfterPredefinedRolls) {
        this.diceRolls = diceRolls;
        addRandomRollsToTheEnd(randomMovesCountAfterPredefinedRolls);
    }

    private void addRandomRollsToTheEnd(int randomRollsCount) {
        for (int i = 0; i < randomRollsCount; i++) {
            diceRolls.add(new int[] {random.nextInt(DIE_FACES_COUNT) + 1, random.nextInt(DIE_FACES_COUNT) + 1});
        }
    }

    @Override
    public Dice rollAndReturnDice(Dice dice) {
        int[] diceRoll = diceRolls.remove(0);

        dice.setDie1(diceRoll[0]);
        dice.setDie2(diceRoll[1]);
        if (dice.getDie1() == dice.getDie2()) {
            dice.setDoubles(true);
        } else {
            dice.setDoubles(false);
        }
        return dice;
    }
}
