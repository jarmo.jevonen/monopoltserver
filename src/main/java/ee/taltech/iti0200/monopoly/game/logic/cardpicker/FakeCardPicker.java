package ee.taltech.iti0200.monopoly.game.logic.cardpicker;

import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.card.Deck;

import java.util.List;
import java.util.Random;

public class FakeCardPicker implements ICardPicker {

    public FakeCardPicker(List<Integer> cardIndices) {
        this.cardIndices = cardIndices;
    }

    private List<Integer> cardIndices;

    @Override
    public Card pickACard(Deck deck, CardType cardType) {
        if (cardIndices.isEmpty()) {
            if (cardType.equals(CardType.CHANCE)) {
                return deck.getChanceCards().get(new Random().nextInt(deck.getChanceCards().size()));
            }
            return deck.getCommunityChestCards().get(new Random().nextInt(deck.getChanceCards().size()));
        } else {
            if (cardType.equals(CardType.CHANCE)) {
                return deck.getChanceCards().get(cardIndices.remove(0));
            }
            return deck.getCommunityChestCards().get(cardIndices.remove(0));
        }
    }
}
