package ee.taltech.iti0200.monopoly.game.logic.initializer;

import ee.taltech.iti0200.monopoly.game.bank.Bank;

public class BankInitializer {
    private static final int DEFAULT_HOUSES_COUNT = 32;
    private static final int DEFAULT_HOTELS_COUNT = 12;

    public static Bank initializeAndReturnBank() {
        Bank bank = new Bank();
        bank.setTotalHousesCount(DEFAULT_HOUSES_COUNT);
        bank.setTotalHotelsCount(DEFAULT_HOTELS_COUNT);
        bank.setAvailableHousesCount(DEFAULT_HOUSES_COUNT);
        bank.setAvailableHotelsCount(DEFAULT_HOTELS_COUNT);
        return bank;
    }

}
