package ee.taltech.iti0200.monopoly.game.logic.cardpicker;

import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.card.Deck;

public interface ICardPicker {
    Card pickACard(Deck deck, CardType cardType);
}
