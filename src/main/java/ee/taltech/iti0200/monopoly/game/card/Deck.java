package ee.taltech.iti0200.monopoly.game.card;

import lombok.Data;

import java.util.List;

@Data
public class Deck {
    private List<Card> chanceCards;
    private List<Card> communityChestCards;
}
