package ee.taltech.iti0200.monopoly.game.logic.initializer;

import ee.taltech.iti0200.monopoly.game.board.Board;

public class BoardInitializer {

    public static Board initializeAndReturnBoard() {
        Board board = new Board();
        board.setTiles(TilesInitializer.initializeAndGetTiles());
        return board;
    }

}
