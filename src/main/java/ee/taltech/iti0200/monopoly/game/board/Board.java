package ee.taltech.iti0200.monopoly.game.board;

import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import lombok.Data;

import java.util.List;

@Data
public class Board {

    private List<Tile> tiles;


}
