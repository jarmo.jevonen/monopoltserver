package ee.taltech.iti0200.monopoly.game.board.tile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class RegularPropertyTile extends PropertyTile {


    private double baseRent;
    private double rentWithOneHouse;
    private double rentWithTwoHouses;
    private double rentWithThreeHouses;
    private double rentWithFourHouses;
    private double rentWithAHotel;
    private int buildingsCount;
    private double upgradePrice;

    @Override
    public String toString() {
        return String.format("%d) %s, %s", getPosition(), getTileName(), getPropertyColor());
    }
}
