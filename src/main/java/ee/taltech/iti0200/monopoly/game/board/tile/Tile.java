package ee.taltech.iti0200.monopoly.game.board.tile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

@Data
@NoArgsConstructor
@SuperBuilder
@JsonTypeInfo(use= JsonTypeInfo.Id.CLASS)
@JsonSubTypes({
        @JsonSubTypes.Type(value=PropertyTile.class),
        @JsonSubTypes.Type(value=SpecialTile.class),
        @JsonSubTypes.Type(value=TaxTile.class)
})
public abstract class Tile {

    private String tileName;
    private int position;
    private boolean ownable;

}
