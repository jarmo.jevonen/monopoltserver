package ee.taltech.iti0200.monopoly.game.board.tile;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
public class PropertyTileGroup {

    private PropertyColor propertyColor;
    private List<PropertyTile> propertyTiles;
    private double housePrice;



}