package ee.taltech.iti0200.monopoly.game.logic.diceroller;

import ee.taltech.iti0200.monopoly.game.dice.Dice;

public interface IDiceRoller {
    Dice rollAndReturnDice(Dice dice);
}
