package ee.taltech.iti0200.monopoly.game.board.tile;

import ee.taltech.iti0200.monopoly.game.player.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@JsonTypeInfo(use= JsonTypeInfo.Id.CLASS)
@JsonSubTypes({
        @JsonSubTypes.Type(value=RegularPropertyTile.class),
        @JsonSubTypes.Type(value=RailroadPropertyTile.class),
        @JsonSubTypes.Type(value=UtilityPropertyTile.class)
})
public abstract class PropertyTile extends Tile{

    private Player owner;
    private PropertyColor propertyColor;
    private double price;
    private double mortgageValue;
    private boolean mortgaged;
}
