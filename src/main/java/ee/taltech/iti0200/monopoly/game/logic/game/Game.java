package ee.taltech.iti0200.monopoly.game.logic.game;

import ee.taltech.iti0200.monopoly.game.bank.Bank;
import ee.taltech.iti0200.monopoly.game.board.Board;
import ee.taltech.iti0200.monopoly.game.board.tile.*;
import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardAction;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.card.Deck;
import ee.taltech.iti0200.monopoly.game.dice.Dice;
import ee.taltech.iti0200.monopoly.game.jail.Jail;
import ee.taltech.iti0200.monopoly.game.logic.cardpicker.CardPicker;
import ee.taltech.iti0200.monopoly.game.logic.cardpicker.FakeCardPicker;
import ee.taltech.iti0200.monopoly.game.logic.cardpicker.ICardPicker;
import ee.taltech.iti0200.monopoly.game.logic.diceroller.DiceRoller;
import ee.taltech.iti0200.monopoly.game.logic.diceroller.FakeDiceRoller;
import ee.taltech.iti0200.monopoly.game.logic.diceroller.IDiceRoller;
import ee.taltech.iti0200.monopoly.game.logic.gameenigne.GameNetworkingServerLogic;
import ee.taltech.iti0200.monopoly.game.logic.initializer.BankInitializer;
import ee.taltech.iti0200.monopoly.game.logic.initializer.BoardInitializer;
import ee.taltech.iti0200.monopoly.game.logic.initializer.CardDeckInitializer;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.game.valuecalc.ValueCalculator;
import lombok.Data;

import java.util.*;

@Data
public class Game {
    private GameNetworkingServerLogic gameNetworkingServerLogic;

    private int turnCount = 1;

    private Board board;
    private List<Player> players;
    private Bank bank;
    private Jail jail;
    private Dice dice;
    private ICardPicker cardPicker;
    private IDiceRoller diceRoller;
    private Deck opportunityDeck;
    private ValueCalculator valueCalculator;
    private boolean gameInProgress;

    private Player currentTurnPlayer;
    private boolean canCurrentTurnPlayerRollTheDice = true;
    private boolean canCurrentTurnPlayerEndTheTurn = false;
    private Card takenCard = null;

    protected MoneyService moneyService;
    protected PropertyService propertyService;

    private CpuPlayerLogic cpuPlayerLogic;

    private Map<Integer, Double> propertyTileBaseValueMap;

    public Game(GameNetworkingServerLogic gameNetworkingServerLogic) {
        this.gameNetworkingServerLogic = gameNetworkingServerLogic;
        this.moneyService = new MoneyService();
        this.propertyService = new PropertyService(this);
    }

    public void initializeGame(List<Player> players) {
        this.board = BoardInitializer.initializeAndReturnBoard();
        this.opportunityDeck = CardDeckInitializer.initializeAndReturnDeck();
        this.players = players;
        this.bank = BankInitializer.initializeAndReturnBank();
        this.jail = new Jail();
        this.dice = new Dice();
        // Use fake diceroller for testing.
        this.diceRoller = new DiceRoller();
        populatePropertyTileBaseValueMap();
        this.valueCalculator = new ValueCalculator(this);

        /*List<int[]> predefinedRolls = new ArrayList<>(List.of(
                new int[] {1, 1},
                new int[] {1, 1},
                new int[] {1, 1},
                new int[] {1, 1},
                new int[] {1, 1},
                new int[] {1, 1}
        ));
        this.diceRoller = new FakeDiceRoller(predefinedRolls, 100);*/

        this.cardPicker = new CardPicker();

        /*List<Integer> predefinedCardIndices = new ArrayList<>(List.of(14, 14, 14, 14, 14));
        this.cardPicker = new FakeCardPicker(predefinedCardIndices);*/

        jail.setPlayersInJailMap(new HashMap<>());

        this.cpuPlayerLogic = new CpuPlayerLogic(this);

    }

    public void startGame() {
        gameNetworkingServerLogic.broadcastGameStart();
        chooseStartingPlayer();
        gameNetworkingServerLogic.broadcastGameStateSetPlayerTurn(currentTurnPlayer);
        gameInProgress = true;
    }

    public void endTurn(Player player, boolean forceBankruptcy) {
        if (!gameInProgress) return;
        if (!player.equals(currentTurnPlayer)) return;
        if (!canCurrentTurnPlayerEndTheTurn) return;

        if (currentTurnPlayer.getBalance() < 0 && !forceBankruptcy) {
            if (currentTurnPlayer.isAI()) cpuPlayerLogic.handlePossibleBankruptcy(player);
            else gameNetworkingServerLogic.sendGameStateWarnBankruptcy(player);
            return;
        }

        currentTurnPlayer = getNextTurnPlayer();
        while (currentTurnPlayer.isHasGoneBankrupt()) {
            currentTurnPlayer = getNextTurnPlayer();
        }

        if (checkForWinAndBroadcast()) return;

        takenCard = null;
        canCurrentTurnPlayerRollTheDice = true;
        canCurrentTurnPlayerEndTheTurn = false;
        gameNetworkingServerLogic.broadcastGameStateSetPlayerTurn(currentTurnPlayer);
        forceChooseActionAtTheBeginningOfTheTurn(currentTurnPlayer);
        if (currentTurnPlayer.isAI()) {
            cpuPlayerLogic.doAIAction(currentTurnPlayer);
        }
    }

    private boolean checkForWinAndBroadcast() {
        if (players.stream().filter(player -> !player.isHasGoneBankrupt()).count() == 1) {
            Player winner = players.stream().filter(player -> !player.isHasGoneBankrupt()).findAny().get();
            gameNetworkingServerLogic.broadcastGameStateWinner(winner);
            return true;
        }
        return false;
    }

    private Player getNextTurnPlayer() {
        return players.get((players.indexOf(currentTurnPlayer) + 1) % players.size());
    }

    private void chooseStartingPlayer() {
        currentTurnPlayer = players.get(new Random().nextInt(players.size()));
        if (currentTurnPlayer.isAI()) {
            cpuPlayerLogic.doAIAction(currentTurnPlayer);
        }
    }

    private void forceChooseActionAtTheBeginningOfTheTurn(Player player) {
        if (!player.isInJail()) return;
        System.out.println(player + " is in jail at the beginning of their turn.");

        boolean canRoll = false;
        boolean canUseCard = false;

        if (player.getOwnedCards().size() > 0) canUseCard = true;
        if (jail.getPlayersInJailMap().get(player.getPlayerId()) < 3) canRoll = true;
        if (currentTurnPlayer.isAI()) {
            cpuPlayerLogic.handleGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(player, canRoll, true, canUseCard);
        } else {
            gameNetworkingServerLogic.sendGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(player, canRoll, true, canUseCard);
        }
    }

    public void rollDice(Player player) {
        // If wrong player or current player shouldn't be able to roll the dice then exit.
        if (!player.equals(currentTurnPlayer) || !canCurrentTurnPlayerRollTheDice) return;

        // Roll the dice and broadcast dice dto.
        dice = diceRoller.rollAndReturnDice(dice);
        gameNetworkingServerLogic.broadcastDieDiceHasBeenRolled(dice);

        // If player is in jail, has rolled less than three times and the current roll wasn't doubles then increase jail-time count.
        if (player.isInJail() && jail.getPlayersInJailMap().get(player.getPlayerId()) < 3 && !dice.isDoubles()) {
            canCurrentTurnPlayerRollTheDice = false;
            jail.getPlayersInJailMap().put(player.getPlayerId(), jail.getPlayersInJailMap().get(player.getPlayerId()) + 1);
            // If player is in jail, has rolled three times and the third roll wasn't doubles then player has to pay or use get out of jail card.
            if (jail.getPlayersInJailMap().get(player.getPlayerId()) == 3) {
                boolean canUseCard = player.getOwnedCards().size() > 0;
                if (currentTurnPlayer.isAI()) {
                    cpuPlayerLogic.handleGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(player, false, true, canUseCard);
                } else {
                    gameNetworkingServerLogic.sendGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(player, false, true, canUseCard);
                }

            }
            if (currentTurnPlayer.isAI()) cpuPlayerLogic.doAIAction(currentTurnPlayer);
            canCurrentTurnPlayerEndTheTurn = true;
            return;
        }
        if (dice.isDoubles()) {
            // If player rolled doubles while in jail then release player and move.
            if (player.isInJail()) {
                player.setSequentialDoublesCount(0);
                canCurrentTurnPlayerRollTheDice = true;
                releasePlayerFromJail(player);
                movePlayerByNumberOfTiles(player, dice.getDie1() + dice.getDie2(), false);
                return;
            }
            // If player rolled doubles while not in jail then increase sequential doubles count.
            player.setSequentialDoublesCount(player.getSequentialDoublesCount() + 1);
            // If the roll was the third doubles in a row then
            if (player.getSequentialDoublesCount() == 3) {
                goToJail(player);
                canCurrentTurnPlayerEndTheTurn = true;
                if (player.isAI()) cpuPlayerLogic.doAIAction(player);
                return;
            }
            movePlayerByNumberOfTiles(player, dice.getDie1() + dice.getDie2(), false);
            return;
            // Normal roll, move player, update ui.
        } else {
            player.setSequentialDoublesCount(0);
            canCurrentTurnPlayerRollTheDice = false;
            canCurrentTurnPlayerEndTheTurn = true;
            movePlayerByNumberOfTiles(player, dice.getDie1() + dice.getDie2(), false);

        }
    }

    private int calculateNextTileIndexFromCurrentPosAndDistToMove(int fromIndex, int movePlayerBy) {
        return Math.floorMod(fromIndex + movePlayerBy, 40);
    }

    private int calculateCountOfTilesToMove(int fromIndex, int toIndex, boolean forward) {
        if (forward) {
            return Math.floorMod(40 - fromIndex + toIndex, 40);
        } else {
            return Math.floorMod(40 - fromIndex + toIndex, 40) - 40;
        }
    }

    private void movePlayerByNumberOfTiles(Player player, int numberOfTiles, boolean toJail) {
        if (toJail) {
            player.setPosition(10);
            gameNetworkingServerLogic.broadcastMovingMovePlayerToTile(player, 10, true, true);
            canCurrentTurnPlayerRollTheDice = false;
            if (currentTurnPlayer.isAI()) cpuPlayerLogic.doAIAction(currentTurnPlayer);
            return;
        }
        int moveToTileIndex = calculateNextTileIndexFromCurrentPosAndDistToMove(player.getPosition(), numberOfTiles);

        boolean moveBackwards = false;
        if (numberOfTiles < 0) {
            moveBackwards = true;
        }

        // Calculate whether passes or goes to GO tile.
        if (!moveBackwards && player.getPosition() + numberOfTiles >= 40) {
            moneyService.balanceAddAmount(player, 20);
            gameNetworkingServerLogic.broadcastMoneyGetMoneyFromBank(player, 20);
        }

        player.setPosition(moveToTileIndex);

        gameNetworkingServerLogic.broadcastMovingMovePlayerToTile(player, moveToTileIndex, toJail, moveBackwards);

        decideTileAction();
    }

    public void tryToSetMortgageToPropertyTile(Player player, PropertyTile propertyTile, boolean toBeMortgaged) {
        if (propertyTile.getOwner() != player || propertyTile.isMortgaged() && toBeMortgaged || !propertyTile.isMortgaged() && !toBeMortgaged)
            return;
        double propertyTileMortgageValue = propertyTile.getMortgageValue();
        if (toBeMortgaged) {
            propertyTile.setMortgaged(true);
            moneyService.balanceAddAmount(player, propertyTileMortgageValue);
            gameNetworkingServerLogic.broadcastPropertySetPropertyMortgagedStatus(propertyTile);
            gameNetworkingServerLogic.broadcastMoneyGetMoneyFromBank(player, propertyTileMortgageValue);
        } else if (player.getBalance() >= propertyTileMortgageValue * 1.1) {
            propertyTile.setMortgaged(false);
            moneyService.balanceDeductAmount(player, propertyTileMortgageValue * 1.1);
            gameNetworkingServerLogic.broadcastPropertySetPropertyMortgagedStatus(propertyTile);
            gameNetworkingServerLogic.broadcastMoneyPayMoneyToBank(player, propertyTileMortgageValue * 1.1);
        }
    }



    public void receivePurchaseDecision(Player player, PropertyTile propertyTile, boolean decision) {
        if (!currentTurnPlayer.equals(player) || currentTurnPlayer.getPosition() != propertyTile.getPosition() || propertyTile.getOwner() != null || !decision)
            return;
        purchaseTile(currentTurnPlayer, propertyTile);
    }

    private void purchaseTile(Player player, PropertyTile propertyTile) {
        if (!moneyService.playerHasEnoughMoney(player, propertyTile.getPrice())) return;
        moneyService.balanceDeductAmount(player, propertyTile.getPrice());
        propertyService.purchaseTile(player, propertyTile);
        gameNetworkingServerLogic.broadcastMoneyPayMoneyToBank(player, propertyTile.getPrice());
        gameNetworkingServerLogic.broadcastPropertySetPropertyOwnership(player, propertyTile);
    }

    private void payRent(Player player, PropertyTile propertyTile) {
        double rentAmount = propertyService.getRentAmountForProperty(propertyTile, dice);
        moneyService.balanceDeductAmount(player, rentAmount);
        moneyService.balanceAddAmount(propertyTile.getOwner(), rentAmount);
        gameNetworkingServerLogic.broadcastMoneyTransferMoneyBetweenPlayers(player, propertyTile.getOwner(), rentAmount);
    }

    private void goToJail(Player player) {
        player.setInJail(true);
        jail.getPlayersInJailMap().put(player.getPlayerId(), 0);
        player.setSequentialDoublesCount(0);
        canCurrentTurnPlayerRollTheDice = false;
        movePlayerByNumberOfTiles(player, 0, true);
        canCurrentTurnPlayerEndTheTurn = true;
    }

    private void takeCard(CardType cardType) {
        takenCard = cardPicker.pickACard(opportunityDeck, cardType);

        System.out.println("Player " + currentTurnPlayer.getName() + " took a card: " + takenCard);

        gameNetworkingServerLogic.broadcastCardTakeACard(currentTurnPlayer, takenCard);

        CardAction takenCardAction = takenCard.getAction();

        if (takenCardAction.equals(CardAction.GO_TO_JAIL)) {
            goToJail(currentTurnPlayer);
        } else if (takenCardAction.equals(CardAction.GO_TO_POS)) {

            System.out.println("TakeACard: currentPos: " + currentTurnPlayer.getPosition() + ", takenCardGetMoveToTile: " + takenCard.getMoveToPos());

            // Represents 'go back to ...' card
            if (takenCard.getMoveSteps() == -1) {
                int stepsToMove = calculateCountOfTilesToMove(currentTurnPlayer.getPosition(), takenCard.getMoveToPos(), false);
                movePlayerByNumberOfTiles(currentTurnPlayer, stepsToMove, false);
            } else {
                int stepsToMove = calculateCountOfTilesToMove(currentTurnPlayer.getPosition(), takenCard.getMoveToPos(), true);
                movePlayerByNumberOfTiles(currentTurnPlayer, stepsToMove, false);
            }
        } else if (takenCardAction.equals(CardAction.GO_STEPS)) {
            movePlayerByNumberOfTiles(currentTurnPlayer, takenCard.getMoveSteps(), false);
        } else if (takenCardAction.equals(CardAction.GET_OUT_OF_JAIL)) {
            currentTurnPlayer.getOwnedCards().add(takenCard);
            gameNetworkingServerLogic.sendCardSetPlayerOwnedGetOutOfJailCardsCount(currentTurnPlayer);
        } else if (takenCardAction.equals(CardAction.PAY_OR_SWITCH)) {
            CardType anotherCardType = cardType.equals(CardType.CHANCE) ? CardType.COMMUNITY_CHEST : CardType.CHANCE;
            if (currentTurnPlayer.isAI()) {
                cpuPlayerLogic.handleCardSwitchACardOrPay(currentTurnPlayer, cardType, anotherCardType, takenCard.getPayMoneyToBank());
            } else {
                gameNetworkingServerLogic.sendCardSwitchACardOrPay(currentTurnPlayer, cardType, anotherCardType, takenCard.getPayMoneyToBank());
            }
        } else if (takenCardAction.equals(CardAction.PAY_BANK)) {
            moneyService.balanceDeductAmount(currentTurnPlayer, takenCard.getPayMoneyToBank());
            gameNetworkingServerLogic.broadcastMoneyPayMoneyToBank(currentTurnPlayer, takenCard.getPayMoneyToBank());
        } else if (takenCardAction.equals(CardAction.RECEIVE_MONEY_FROM_PLAYERS)) {
            double amount = takenCard.getReceiveMoneyFromPlayers();
            for (Player player : players) {
                moneyService.balanceDeductAmount(player, amount);
                moneyService.balanceAddAmount(currentTurnPlayer, amount);
                gameNetworkingServerLogic.broadcastMoneyTransferMoneyBetweenPlayers(player, currentTurnPlayer, amount);
            }
        } else if (takenCardAction.equals(CardAction.RECEIVE_MONEY_FROM_BANK)) {
            moneyService.balanceAddAmount(currentTurnPlayer, takenCard.getReceiveMoneyFromBank());
            gameNetworkingServerLogic.broadcastMoneyGetMoneyFromBank(currentTurnPlayer, takenCard.getReceiveMoneyFromBank());
        } else if (takenCardAction.equals(CardAction.MAINTENANCE_OR_REPAIRS)) {
            double amount = propertyService.getMaintenanceCostForProperty(takenCard.getMaintenancePerHouse(), takenCard.getMaintenancePerHotel());
            moneyService.balanceDeductAmount(currentTurnPlayer, amount);
            gameNetworkingServerLogic.broadcastMoneyPayMoneyToBank(currentTurnPlayer, amount);
        }
        if (currentTurnPlayer.isAI()) cpuPlayerLogic.doAIAction(currentTurnPlayer);
    }

    public void receiveCardChoice(Player player, boolean choicePay) {
        if (!currentTurnPlayer.equals(player) || takenCard == null || !takenCard.getAction().equals(CardAction.PAY_OR_SWITCH))
            return;
        if (choicePay) {
            double amount = takenCard.getPayMoneyToBank();
            moneyService.balanceDeductAmount(currentTurnPlayer, amount);
            gameNetworkingServerLogic.broadcastMoneyPayMoneyToBank(currentTurnPlayer, amount);
        } else {
            if (takenCard.getType().equals(CardType.CHANCE)) {
                takeCard(CardType.COMMUNITY_CHEST);
            } else {
                takeCard(CardType.CHANCE);
            }
        }
    }

    public void receiveCardUseGetOutOfJailCard(Player player, boolean useCard) {
        if (!jail.getPlayersInJailMap().containsKey(player.getPlayerId()) || player.getOwnedCards().size() < 1 || !useCard) return;
        player.getOwnedCards().remove(0);
        releasePlayerFromJail(player);
        gameNetworkingServerLogic.sendCardSetPlayerOwnedGetOutOfJailCardsCount(currentTurnPlayer);
    }

    public void receiveMoneyPayToGetOutOfJail(Player player) {
        if (!jail.getPlayersInJailMap().containsKey(player.getPlayerId())) return;
        moneyService.balanceDeductAmount(player, 5);
        gameNetworkingServerLogic.broadcastMoneyPayMoneyToBank(player, 5);
        releasePlayerFromJail(player);
    }



    public void receivePropertyUpDowngradeDecision(Player player, PropertyTile propertyTile, boolean toBeUpgraded) {
        if (propertyTile.getOwner() != player) return;

        if (propertyTile instanceof RegularPropertyTile) {
            RegularPropertyTile regularPropertyTile = (RegularPropertyTile) propertyTile;
            int currentBuildingCount = regularPropertyTile.getBuildingsCount();

            if (toBeUpgraded) {
                if (currentBuildingCount == 5) return;
                if (propertyTile.isMortgaged()) {
                    tryToSetMortgageToPropertyTile(player, propertyTile, false);
                    return;
                }

                if (!board.getTiles().stream().filter(tile -> tile instanceof PropertyTile).map(tile -> (PropertyTile) tile).filter(pt -> pt.getPropertyColor().equals(propertyTile.getPropertyColor())).allMatch(pt -> pt.getOwner() != null && pt.getOwner().equals(player))) {
                    return;
                }

                if (board.getTiles().stream().filter(tile -> tile instanceof PropertyTile).map(tile -> (PropertyTile) tile).filter(pt -> pt.getPropertyColor().equals(propertyTile.getPropertyColor())).anyMatch(PropertyTile::isMortgaged)) {
                    return;
                }

                double upgradeCost = regularPropertyTile.getUpgradePrice();
                if (player.getBalance() < upgradeCost) return;
                regularPropertyTile.setBuildingsCount(regularPropertyTile.getBuildingsCount() + 1);
                moneyService.balanceDeductAmount(player, upgradeCost);
                gameNetworkingServerLogic.broadcastMoneyPayMoneyToBank(player, upgradeCost);
            } else {
                if (currentBuildingCount == 0) {
                    tryToSetMortgageToPropertyTile(player, propertyTile, true);
                    return;
                }
                double amountToGetForDowngrading = regularPropertyTile.getUpgradePrice() * 0.5;
                regularPropertyTile.setBuildingsCount(regularPropertyTile.getBuildingsCount() - 1);
                moneyService.balanceAddAmount(player, amountToGetForDowngrading);
                gameNetworkingServerLogic.broadcastMoneyGetMoneyFromBank(player, amountToGetForDowngrading);
            }
            gameNetworkingServerLogic.broadcastPropertyUpDowngradeProperty(regularPropertyTile);
        } else {
            if (toBeUpgraded) {
                if (propertyTile.isMortgaged()) {
                    tryToSetMortgageToPropertyTile(player, propertyTile, false);
                }
            } else {
                tryToSetMortgageToPropertyTile(player, propertyTile, true);
            }
        }



    }


    private void releasePlayerFromJail(Player player) {
        jail.getPlayersInJailMap().remove(player.getPlayerId());
        player.setInJail(false);
        gameNetworkingServerLogic.broadcastMovingMovePlayerToTile(player, 10, false, false);
    }


    private void decideTileAction() {
        Tile currentTile = board.getTiles().get(currentTurnPlayer.getPosition());
        // If player is on a propertyTile.
        if (currentTile instanceof PropertyTile) {
            PropertyTile currentPropertyTile = (PropertyTile) currentTile;
            // if nobody owns this tile
            if (currentPropertyTile.getOwner() == null) {
                if (moneyService.playerHasEnoughMoney(currentTurnPlayer, currentPropertyTile.getPrice())) {
                    if (currentTurnPlayer.isAI()) {
                        cpuPlayerLogic.sendPropertyOfferToBuy(currentTurnPlayer, currentPropertyTile);
                    } else {
                        gameNetworkingServerLogic.sendPropertyOfferToBuy(currentTurnPlayer, currentPropertyTile);
                    }
                }
                // Someone else owns this tile, check whether the tile is mortgaged.
            } else if (!currentPropertyTile.getOwner().equals(currentTurnPlayer)) {
                // If the current propertyTile is owned by someone else and is not mortgaged then let the current player pay the owner.
                if (!currentPropertyTile.isMortgaged()) {
                    payRent(currentTurnPlayer, currentPropertyTile);
                }
                // Current player owns this tile.
            } else {
                System.out.println("Current player owns this tile");
            }
        }

        // If player is on a specialTile
        else if (currentTile instanceof SpecialTile) {
            SpecialTile currentSpecialTile = (SpecialTile) currentTile;
            if (currentSpecialTile.getSpecialTileType().equals(SpecialTileType.GO)) {
                System.out.println("Current player is on go-tile.");
            } else if (currentSpecialTile.getSpecialTileType().equals(SpecialTileType.CHANCE)) {
                takeCard(CardType.CHANCE);
            } else if (currentSpecialTile.getSpecialTileType().equals(SpecialTileType.COMMUNITY_CHEST)) {
                takeCard(CardType.COMMUNITY_CHEST);
            } else if (currentSpecialTile.getSpecialTileType().equals(SpecialTileType.JAIL)) {
                if (jail.getPlayersInJailMap().containsKey(currentTurnPlayer.getPlayerId())) {
                    forceChooseActionAtTheBeginningOfTheTurn(currentTurnPlayer);
                } // else just visiting

            } else if (currentSpecialTile.getSpecialTileType().equals(SpecialTileType.FREE_PARKING)) {
                System.out.println("Current player is just parking.");
            } else if (currentSpecialTile.getSpecialTileType().equals(SpecialTileType.GO_TO_JAIL)) {
                goToJail(currentTurnPlayer);
            }
        }

        // If player is on a taxTile
        else if (currentTile instanceof TaxTile) {
            TaxTile currentTaxTile = (TaxTile) currentTile;
            moneyService.balanceDeductAmount(currentTurnPlayer, currentTaxTile.getTaxAmount());
            gameNetworkingServerLogic.broadcastMoneyPayMoneyToBank(currentTurnPlayer, currentTaxTile.getTaxAmount());
        }

        if (currentTurnPlayer.isAI()) cpuPlayerLogic.doAIAction(currentTurnPlayer);
    }


    private void populatePropertyTileBaseValueMap(){
        propertyTileBaseValueMap = Map.ofEntries(
                new AbstractMap.SimpleEntry<>(1, -49.0),
                new AbstractMap.SimpleEntry<>(3, 7.0),
                new AbstractMap.SimpleEntry<>(5, -9.0),
                new AbstractMap.SimpleEntry<>(6, 3.0),
                new AbstractMap.SimpleEntry<>(8, 7.0),
                new AbstractMap.SimpleEntry<>(9, 15.0),

                new AbstractMap.SimpleEntry<>(11, 6.0),
                new AbstractMap.SimpleEntry<>(12, -38.0),
                new AbstractMap.SimpleEntry<>(13, -12.0),
                new AbstractMap.SimpleEntry<>(14, 8.0),
                new AbstractMap.SimpleEntry<>(15, -12.0),
                new AbstractMap.SimpleEntry<>(16, 19.0),
                new AbstractMap.SimpleEntry<>(18, 24.0),
                new AbstractMap.SimpleEntry<>(19, 36.0),

                new AbstractMap.SimpleEntry<>(21, 1.0),
                new AbstractMap.SimpleEntry<>(23, -2.0),
                new AbstractMap.SimpleEntry<>(24, 22.0),
                new AbstractMap.SimpleEntry<>(25, -10.0),
                new AbstractMap.SimpleEntry<>(26, 4.0),
                new AbstractMap.SimpleEntry<>(27, 3.0),
                new AbstractMap.SimpleEntry<>(28, -42.0),
                new AbstractMap.SimpleEntry<>(29, 3.0),

                new AbstractMap.SimpleEntry<>(31, -8.0),
                new AbstractMap.SimpleEntry<>(32, -10.0),
                new AbstractMap.SimpleEntry<>(34, -6.0),
                new AbstractMap.SimpleEntry<>(35, -38.0),
                new AbstractMap.SimpleEntry<>(37, 9.0),
                new AbstractMap.SimpleEntry<>(39, 67.0)
        );
    }


    public void playerDisconnected(Player disconnectedPlayer) {
        disconnectedPlayer.setAI(true);
        if (currentTurnPlayer.equals(disconnectedPlayer) && players.stream().anyMatch(player -> !player.isAI())) {
            cpuPlayerLogic.doAIAction(disconnectedPlayer);
        }
    }

    public void playerDeclareBankruptcy(Player player) {
        for (PropertyTile propertyTile : player.getOwnedPropertyTiles()) {
            propertyTile.setMortgaged(false);
            if (propertyTile instanceof RegularPropertyTile) {
                ((RegularPropertyTile) propertyTile).setBuildingsCount(0);
            }
            propertyTile.setOwner(null);
        }
        player.getOwnedPropertyTiles().clear();
        jail.getPlayersInJailMap().remove(player.getPlayerId());
        player.setHasGoneBankrupt(true);
        gameNetworkingServerLogic.broadcastGameStatePlayerWentBankrupt(player);
        endTurn(player, true);
    }
}
