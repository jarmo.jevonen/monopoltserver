package ee.taltech.iti0200.monopoly.game.logic.diceroller;

import ee.taltech.iti0200.monopoly.game.dice.Dice;

import java.util.Random;

public class DiceRoller implements IDiceRoller{

    private static final int DIE_FACES_COUNT = 6;
    private Random random = new Random();


    @Override
    public Dice rollAndReturnDice(Dice dice) {
        dice.setDie1(random.nextInt(DIE_FACES_COUNT) + 1);
        dice.setDie2(random.nextInt(DIE_FACES_COUNT) + 1);
        if (dice.getDie1() == dice.getDie2()) {
            dice.setDoubles(true);
        } else {
            dice.setDoubles(false);
        }
        return dice;
    }
}
