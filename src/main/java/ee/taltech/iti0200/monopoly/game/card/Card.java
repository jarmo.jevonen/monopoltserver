package ee.taltech.iti0200.monopoly.game.card;

import ee.taltech.iti0200.monopoly.game.player.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
public class Card {
    private CardType type;
    private CardAction action;
    private String text1;
    private String text2;
    private String text3;
    private String text4;
    private boolean collectible;
    private int moveToPos;
    private int moveSteps;
    private Player owner;
    private double receiveMoneyFromBank;
    private double payMoneyToBank;
    private double receiveMoneyFromPlayers;
    private double maintenancePerHotel;
    private double maintenancePerHouse;
    private int cardIndex;
}
