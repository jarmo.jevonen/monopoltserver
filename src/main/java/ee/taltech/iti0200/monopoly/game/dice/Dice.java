package ee.taltech.iti0200.monopoly.game.dice;

import lombok.Data;

@Data
public class Dice {
    private int die1;
    private int die2;
    private boolean doubles;
}
