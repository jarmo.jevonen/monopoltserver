package ee.taltech.iti0200.monopoly.game.valuecalc;

import ee.taltech.iti0200.monopoly.game.board.tile.PropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.logic.game.Game;
import ee.taltech.iti0200.monopoly.game.player.Player;

public class ValueCalculator {
    private Game game;

    public ValueCalculator(Game game) {
        this.game = game;
    }

    public double calculatePlayerValueBySellingBuildingsAndTakingMortgages(Player player) {
        double totalValue = player.getBalance();
        for (PropertyTile propertyTile : player.getOwnedPropertyTiles()) {
            if (propertyTile instanceof RegularPropertyTile) {
                RegularPropertyTile regularPropertyTile = (RegularPropertyTile) propertyTile;
                totalValue += regularPropertyTile.getUpgradePrice() * 0.5 * regularPropertyTile.getBuildingsCount();
            }
            totalValue += propertyTile.getMortgageValue();
        }
        return totalValue;
    }
}
