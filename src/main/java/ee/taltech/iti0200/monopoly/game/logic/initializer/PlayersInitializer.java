package ee.taltech.iti0200.monopoly.game.logic.initializer;

import ee.taltech.iti0200.monopoly.game.player.Player;

import java.util.*;

public class PlayersInitializer {

    private static final double STARTING_BALANCE = 150;

    public static Player initializeAndReturnPlayer(Player player) {
        player.setPlayerId(UUID.randomUUID().toString());
        player.setBalance(STARTING_BALANCE);
        player.setPosition(0);
        player.setOwnedCards(new ArrayList<>());
        player.setOwnedPropertyTiles(new ArrayList<>());
        player.setSequentialDoublesCount(0);
        player.setInJail(false);
        return player;
    }
}
