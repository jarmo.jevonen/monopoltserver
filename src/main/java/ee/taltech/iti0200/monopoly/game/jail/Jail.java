package ee.taltech.iti0200.monopoly.game.jail;

import ee.taltech.iti0200.monopoly.game.player.Player;
import lombok.Data;

import java.util.Map;

@Data
public class Jail {
    private Map<String, Integer> playersInJailMap;
}
