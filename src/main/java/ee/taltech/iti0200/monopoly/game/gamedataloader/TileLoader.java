package ee.taltech.iti0200.monopoly.game.gamedataloader;

import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static ee.taltech.iti0200.monopoly.util.ResourceFetcher.getResourceFileAsInputStream;

public class TileLoader {

    private static final String TILE_DATA_FILE = "tiledata.txt";

    public static List<Tile> loadTilesFromFiles() {

        InputStream inputStream = getResourceFileAsInputStream(TILE_DATA_FILE);
        if (inputStream == null) {
            throw new RuntimeException("Could not find tiledata file.");
        }
        Scanner s = new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
        String tilesString = s.hasNext() ? s.next() : "";
        String[] lines = tilesString.split("\\r?\\n");

        List<Tile> tiles = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        for (String line : lines) {
            try {
                tiles.add(objectMapper.readValue(line, Tile.class));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return tiles;

    }

    public static String getTileStringFromFiles() {
        InputStream inputStream = getResourceFileAsInputStream(TILE_DATA_FILE);
        if (inputStream == null) {
            throw new RuntimeException("Could not find tiledata file.");
        }
        Scanner s = new Scanner(inputStream, "UTF-8").useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}
