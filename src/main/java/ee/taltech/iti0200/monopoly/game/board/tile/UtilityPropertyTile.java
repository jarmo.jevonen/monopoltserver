package ee.taltech.iti0200.monopoly.game.board.tile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UtilityPropertyTile extends PropertyTile{

    private PropertyTileGroup propertyTileGroup;

    private int coefficientWithOneUtility;
    private int coefficientWithBothUtilities;

    @Override
    public String toString() {
        return String.format("%d) %s, %s", getPosition(), getTileName(), getPropertyColor());
    }

}
