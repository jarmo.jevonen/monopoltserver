package ee.taltech.iti0200.monopoly.game.logic.game;

import ee.taltech.iti0200.monopoly.game.board.tile.PropertyColor;
import ee.taltech.iti0200.monopoly.game.board.tile.PropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.card.CardAction;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.player.Player;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class CpuPlayerLogic {
    private Game game;

    private List<CpuPlayerAction> cpuPlayerActions;

    public CpuPlayerLogic(Game game) {
        this.game = game;
    }

    private void populatePossibleActions() {
        cpuPlayerActions = new ArrayList<>(List.of(
                CpuPlayerAction.PURCHASE_PROPERTY,
                CpuPlayerAction.MORTGAGE_PROPERTY,
                CpuPlayerAction.UNMORTGAGE_PROPERTY,
                CpuPlayerAction.UPGRADE_PROPERTY,
                CpuPlayerAction.DOWNGRADE_PROPERTY,
                CpuPlayerAction.GET_OUT_OF_JAIL_PAY,
                CpuPlayerAction.GET_OUT_OF_JAIL_USE_CARD,
                CpuPlayerAction.GET_OUT_OF_JAIL_ROLL,
                CpuPlayerAction.CARD_SWITCH,
                CpuPlayerAction.ROLL
        ));
    }

    private void calculatePossibleActionsForPlayer(Player player) {
        populatePossibleActions();
        if (!keepActionPurchaseProperty(player)) cpuPlayerActions.remove(CpuPlayerAction.PURCHASE_PROPERTY);
        if (!keepActionMortgageProperty(player)) cpuPlayerActions.remove(CpuPlayerAction.MORTGAGE_PROPERTY);
        if (!keepActionUnMortgageProperty(player)) cpuPlayerActions.remove(CpuPlayerAction.UNMORTGAGE_PROPERTY);
        if (!keepActionUpgradeProperty(player)) cpuPlayerActions.remove(CpuPlayerAction.UPGRADE_PROPERTY);
        if (!keepActionDowngradeProperty(player)) cpuPlayerActions.remove(CpuPlayerAction.DOWNGRADE_PROPERTY);
        if (!keepActionGetOutOfJailPay(player)) cpuPlayerActions.remove(CpuPlayerAction.GET_OUT_OF_JAIL_PAY);
        if (!keepActionGetOutOfJailUseCard(player)) cpuPlayerActions.remove(CpuPlayerAction.GET_OUT_OF_JAIL_USE_CARD);
        if (!keepActionGetOutOfJailRoll(player)) cpuPlayerActions.remove(CpuPlayerAction.GET_OUT_OF_JAIL_ROLL);
        if (!keepActionCardSwitch(player)) cpuPlayerActions.remove(CpuPlayerAction.CARD_SWITCH);
        if (!keepActionRoll(player)) cpuPlayerActions.remove(CpuPlayerAction.ROLL);
    }

    public void doAIAction(Player player) {
        calculatePossibleActionsForPlayer(player);
        System.out.println("Possible actions for " + player.getName() + " are: " + cpuPlayerActions);
        if (cpuPlayerActions.contains(CpuPlayerAction.UPGRADE_PROPERTY)) {
            PropertyTile tileToUpgrade = findTileToUpgrade(player);
            if (tileToUpgrade != null) {
                game.receivePropertyUpDowngradeDecision(player, tileToUpgrade, true);
            }
        }
        if (cpuPlayerActions.contains(CpuPlayerAction.ROLL)) {
            game.rollDice(player);
        } else {
            game.endTurn(player, false);
        }
        if (cpuPlayerActions.isEmpty()) {
            game.rollDice(player);
            game.endTurn(player, false);

        }
    }


    public void handleGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(Player player, boolean canRoll, boolean allowPaying, boolean allowUsingCard) {
        if (allowUsingCard) {
            game.receiveCardUseGetOutOfJailCard(player, true);
        } else {
            game.receiveMoneyPayToGetOutOfJail(player);
        }
    }

    public void handleCardSwitchACardOrPay(Player player, CardType currentCardType, CardType anotherCardType, double payMoneyToBank) {
        game.receiveCardChoice(player, false);
    }

    public void handlePossibleBankruptcy(Player player) {
        for (PropertyTile propertyTile : player.getOwnedPropertyTiles()) {
            game.receivePropertyUpDowngradeDecision(player, propertyTile, false);
            if (player.getBalance() >= 0) {
                game.endTurn(player, false);
                return;
            }
        }
        game.playerDeclareBankruptcy(player);
    }

    public void sendPropertyOfferToBuy(Player player, PropertyTile propertyTile) {
        game.receivePurchaseDecision(player, propertyTile, calculatePropertyTilePurchaseValue(player, propertyTile) > 15);
    }


    private double calculatePropertyTilePurchaseValue(Player player, PropertyTile propertyTile) {
        PropertyColor color = propertyTile.getPropertyColor();
        List<Player> otherPlayers = game.getPlayers().stream().filter(p -> !p.equals(player)).collect(Collectors.toList());
        List<PropertyTile> sameColorTiles = game.getBoard().getTiles().stream().filter(tile -> tile instanceof PropertyTile).map(tile -> (PropertyTile) tile).filter(pt -> pt.getPropertyColor().equals(propertyTile.getPropertyColor())).collect(Collectors.toList());
        int numberOfSameColorTilesOwned = game.propertyService.getNumberOfOwnedSameColorProperties(player, propertyTile.getPropertyColor());
        double currentlyHighestRentOnBoard = game.propertyService.calculateHighestRentOnBoard();

        double tileBaseValue = game.getPropertyTileBaseValueMap().get(propertyTile.getPosition());
        double delta = 0;

        //System.out.println("Base value on " + propertyTile.getTileName() + " is " + tileBaseValue);

        // if the player owns over half of the same color properties
        if (numberOfSameColorTilesOwned >= sameColorTiles.size() * 0.5) {
            delta += 35;
            //System.out.println("player owns over half of the same color properties, adding 35, total: " + (tileBaseValue + delta));
        }

        // if someone else owns over half of the same color properties
        for (Player otherPlayer : otherPlayers) {
            int otherPlayerSameColorOwnedCount = game.propertyService.getNumberOfOwnedSameColorProperties(otherPlayer, propertyTile.getPropertyColor());
            if (otherPlayerSameColorOwnedCount >= sameColorTiles.size() * 0.5) {
                delta += 15;
                //System.out.println("someone else owns over half of the same color properties, adding 15, total: " + (tileBaseValue + delta));
            }
        }

        // if at least two other players own at least one of the same color properties
        int otherPlayersSameColorCount = 0;
        for (Player otherPlayer : otherPlayers) {
            if (otherPlayer.getOwnedPropertyTiles().stream().map(PropertyTile::getPropertyColor).anyMatch(propColor -> propColor.equals(color))) {
                otherPlayersSameColorCount++;
            }
        }
        if (otherPlayersSameColorCount >= 2) {
            delta -= 15;
            //System.out.println("at least two other players own at least one of the same color properties, adding -25, total: " + (tileBaseValue + delta));
        }

        // if none of the other players own any of the same color properties
        if (otherPlayers.stream().flatMap(p -> p.getOwnedPropertyTiles().stream()).map(PropertyTile::getPropertyColor).noneMatch(pc -> pc.equals(color))) {
            delta += 25;
            //System.out.println("none of the other players own any of the same color properties, adding 25, total: " + (tileBaseValue + delta));
        }

        // if the player's balance is at least twice the property price
        if (player.getBalance() >= 2 * propertyTile.getPrice()) {
            delta += 3 * (player.getBalance() / propertyTile.getPrice());
            //System.out.println("the player's balance is at least twice the property price, adding" + (3 * (player.getBalance() / propertyTile.getPrice()) + ", total: " + (tileBaseValue + delta)));
        }

        // if the player's balance is less than twice the property price
        if (player.getBalance() < 2 * propertyTile.getPrice()) {
            delta -= 5;
            //System.out.println("the player's balance is less than twice the property price, adding -5, total: " + (tileBaseValue + delta));
        }

        // if the player's total value excluding the price of the property is at least currently highest rent on board
        if (game.getValueCalculator().calculatePlayerValueBySellingBuildingsAndTakingMortgages(player) - propertyTile.getPrice() >= currentlyHighestRentOnBoard) {
            delta += 4;
            //System.out.println("the player's total value excluding the price of the property is at least currently highest rent on board, adding 4, total: " + (tileBaseValue + delta));
        }

        // if the player's total value excluding the price of the property is less than currently highest rent on board
        if (game.getValueCalculator().calculatePlayerValueBySellingBuildingsAndTakingMortgages(player) - propertyTile.getPrice() < currentlyHighestRentOnBoard) {
            delta -= 10;
            //System.out.println("the player's total value excluding the price of the property is less than currently highest rent on board, adding -10, total: " + (tileBaseValue + delta));
        }

        // if the player's position is at least 30
        if (player.getPosition() >= 30) {
            delta += 4;
            //System.out.println("the player's position is at least 30, adding 4, total: " + (tileBaseValue + delta));
        }

        // if the player's position is less that 20 and balance is less than currently highest rent on board
        if (player.getPosition() <= 20 && player.getBalance() < currentlyHighestRentOnBoard) {
            delta += -15;
            //System.out.println("the player's position is less that 20 and balance is less than currently highest rent on board, adding -15, total: " + (tileBaseValue + delta));
        }

        //System.out.println(player.getName() + " deciding purchasing property " + propertyTile.getTileName() + ". Value is: " + (tileBaseValue + delta));
        return tileBaseValue + delta;
    }

    private PropertyTile findTileToUpgrade(Player player) {
        List<RegularPropertyTile> upgradableTiles = player.getOwnedPropertyTiles().stream()
                .filter(propertyTile -> game.propertyService.playerHasAllSameColorProperties(player, propertyTile))
                .filter(propertyTile -> !propertyTile.isMortgaged())
                .filter(propertyTile -> propertyTile instanceof RegularPropertyTile)
                .map(propertyTile -> (RegularPropertyTile) propertyTile)
                .filter(regularPropertyTile -> regularPropertyTile.getBuildingsCount() < 5)
                .collect(Collectors.toList());

        for (RegularPropertyTile regularPropertyTile : upgradableTiles) {
            if (otherPlayerWithinRangeOfTile(regularPropertyTile, 12)) {
                return regularPropertyTile;
            }
        }
        return null;

    }

    public boolean otherPlayerWithinRangeOfTile(PropertyTile propertyTile, int range) {
        List<Player> otherPlayers = game.getPlayers().stream().filter(p -> !p.equals(propertyTile.getOwner())).collect(Collectors.toList());
        for (Player otherPlayer : otherPlayers) {
            System.out.println("propTile: " + propertyTile + ", range: " + range + ", player: " + otherPlayer);
            if (otherPlayer.getPosition() > range && propertyTile.getPosition() > range) {
                if (otherPlayer.getPosition() >= propertyTile.getPosition()) continue;
            }
            if (otherPlayer.getPosition() < range && propertyTile.getPosition() < range) {
                if (otherPlayer.getPosition() >= propertyTile.getPosition()) continue;
                return true;
            }
            if (Math.floorMod(propertyTile.getPosition() - range, 40) <= otherPlayer.getPosition()) {
                return true;
            }
        }
        return false;
    }




    private boolean keepActionPurchaseProperty(Player player) {
        if (!(game.getBoard().getTiles().get(player.getPosition()) instanceof PropertyTile)) return false;
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(player.getPosition());
        if (propertyTile.getOwner() != null) return false;
        if (propertyTile.getPrice() > player.getBalance()) return false;
        return true;
    }

    private boolean keepActionMortgageProperty(Player player) {
        if (!(game.getBoard().getTiles().get(player.getPosition()) instanceof PropertyTile)) return false;
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(player.getPosition());
        if (propertyTile.getOwner() == null || !propertyTile.getOwner().equals(player)) return false;
        return true;
    }

    private boolean keepActionUnMortgageProperty(Player player) {
        if (!(game.getBoard().getTiles().get(player.getPosition()) instanceof PropertyTile)) return false;
        PropertyTile propertyTile = (PropertyTile) game.getBoard().getTiles().get(player.getPosition());
        if (propertyTile.getOwner() == null || !propertyTile.getOwner().equals(player)) return false;
        if (propertyTile.getMortgageValue() * 1.1 > player.getBalance()) return false;
        return true;
    }

    private boolean keepActionUpgradeProperty(Player player) {
        if (playerHasAllSameColorRegularPropertyTiles(player)) return false;
        if (player.getOwnedPropertyTiles().stream().filter(propertyTile -> propertyTile instanceof RegularPropertyTile).filter(propertyTile -> !propertyTile.isMortgaged()).map(propertyTile -> (RegularPropertyTile) propertyTile).allMatch(regPropTile -> regPropTile.getBuildingsCount() == 5)) return false;

        return true;
    }

    private boolean keepActionDowngradeProperty(Player player) {
        if (playerHasAllSameColorRegularPropertyTiles(player)) return false;
        if (player.getOwnedPropertyTiles().stream().filter(propertyTile -> propertyTile instanceof RegularPropertyTile).map(propertyTile -> (RegularPropertyTile) propertyTile).allMatch(regPropTile -> regPropTile.getBuildingsCount() == 0)) return false;
        return true;
    }

    private boolean keepActionGetOutOfJailPay(Player player) {
        if (!player.isInJail()) return false;
        if (player.getBalance() < 5) return false;
        return true;
    }

    private boolean keepActionGetOutOfJailUseCard(Player player) {
        if (!player.isInJail()) return false;
        if (player.getOwnedCards().isEmpty()) return false;
        return true;
    }

    private boolean keepActionGetOutOfJailRoll(Player player) {
        if (!player.isInJail()) return false;
        if (game.getJail().getPlayersInJailMap().get(player.getPlayerId()) >= 3) return false;
        return true;
    }

    private boolean keepActionCardSwitch(Player player) {
        if (game.getTakenCard() == null || !game.getTakenCard().getAction().equals(CardAction.PAY_OR_SWITCH)) return false;
        return true;
    }

    private boolean keepActionRoll(Player player) {
        if (!game.isCanCurrentTurnPlayerRollTheDice()) return false;
        return true;
    }


    private boolean playerHasAllSameColorRegularPropertyTiles(Player player) {
        if (!(game.getBoard().getTiles().get(player.getPosition()) instanceof RegularPropertyTile)) return true;
        RegularPropertyTile regularPropertyTile = (RegularPropertyTile) game.getBoard().getTiles().get(player.getPosition());
        if (regularPropertyTile.getOwner() == null || !regularPropertyTile.getOwner().equals(player)) return true;
        if (player.getOwnedPropertyTiles().stream().filter(propertyTile -> propertyTile instanceof RegularPropertyTile).noneMatch(propertyTile -> game.propertyService.playerHasAllSameColorProperties(player, propertyTile)))
            return true;
        return false;
    }



}
