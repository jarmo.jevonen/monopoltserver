package ee.taltech.iti0200.monopoly.game.logic.initializer;

import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.card.Deck;
import ee.taltech.iti0200.monopoly.game.gamedataloader.CardLoader;

import java.util.Collections;

public class CardDeckInitializer {

    public static Deck initializeAndReturnDeck() {
        Deck deck = new Deck();
        deck.setChanceCards(CardLoader.loadCardsFromFiles(CardType.CHANCE));
        deck.setCommunityChestCards(CardLoader.loadCardsFromFiles(CardType.COMMUNITY_CHEST));
        //Collections.shuffle(deck.getChanceCards());
        //Collections.shuffle(deck.getCommunityChestCards());
        return deck;
    }
}
