package ee.taltech.iti0200.monopoly.game.logic.game;

import ee.taltech.iti0200.monopoly.game.board.tile.*;
import ee.taltech.iti0200.monopoly.game.dice.Dice;
import ee.taltech.iti0200.monopoly.game.player.Player;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class PropertyService {
    private Game game;

    public PropertyService(Game game) {
        this.game = game;
    }

    protected void purchaseTile(Player player, PropertyTile propertyTile) {
        propertyTile.setOwner(player);
        player.getOwnedPropertyTiles().add(propertyTile);
    }

    protected double getRentAmountForProperty(PropertyTile propertyTile, Dice dice) {
        PropertyColor propertyColor = propertyTile.getPropertyColor();
        Player propertyOwner = propertyTile.getOwner();
        int numberOfOwnedSameColorProperties = getNumberOfOwnedSameColorProperties(propertyOwner, propertyColor);
        boolean propertyOwnerHasAllSameColorProperties = propertyOwnerHasAllSameColorProperties(numberOfOwnedSameColorProperties, propertyColor);
        if (propertyTile instanceof RegularPropertyTile) {
            RegularPropertyTile regularPropertyTile = (RegularPropertyTile) propertyTile;
            int numberOfHousesOnProperty = regularPropertyTile.getBuildingsCount();
            if (numberOfHousesOnProperty > 0) {
                return getRentValueOfPropertyWithBuildings(numberOfHousesOnProperty, regularPropertyTile);
            } else {
                if (propertyOwnerHasAllSameColorProperties) {
                    return 2 * regularPropertyTile.getBaseRent();
                } else {
                    return regularPropertyTile.getBaseRent();
                }
            }
        }
        if (propertyTile instanceof UtilityPropertyTile) {
            if (propertyOwnerHasAllSameColorProperties) {
                return dice.getDie1() + dice.getDie2();
            } else {
                return (dice.getDie1() + dice.getDie2()) * 4 * 0.1;
            }
        }
        RailroadPropertyTile railroadPropertyTile = (RailroadPropertyTile) propertyTile;
        return getRentValueOfPropertyWithStationsCount(numberOfOwnedSameColorProperties, railroadPropertyTile);
    }

    private double getRentValueOfPropertyWithStationsCount(int numberOfStationsOwned, RailroadPropertyTile railroadPropertyTile) {
        if (numberOfStationsOwned == 1) {
            return railroadPropertyTile.getRentWithOneStation();
        }
        if (numberOfStationsOwned == 2) {
            return railroadPropertyTile.getRentWithTwoStations();
        }
        if (numberOfStationsOwned == 3) {
            return railroadPropertyTile.getRentWithThreeStations();
        }
        if (numberOfStationsOwned == 4) {
            return railroadPropertyTile.getRentWithFourStations();
        }
        throw new RuntimeException(String.format("RailRoad property rent could not be calculated for %s with number of stations owned %d.", railroadPropertyTile, numberOfStationsOwned));
    }

    private double getRentValueOfPropertyWithBuildings(int numberOfOwnedBuildings, RegularPropertyTile regularPropertyTile) {
        if (numberOfOwnedBuildings == 1) {
            return regularPropertyTile.getRentWithOneHouse();
        }
        if (numberOfOwnedBuildings == 2) {
            return regularPropertyTile.getRentWithTwoHouses();
        }
        if (numberOfOwnedBuildings == 3) {
            return regularPropertyTile.getRentWithThreeHouses();
        }
        if (numberOfOwnedBuildings == 4) {
            return regularPropertyTile.getRentWithFourHouses();
        }
        if (numberOfOwnedBuildings == 5) {
            return regularPropertyTile.getRentWithAHotel();
        }
        throw new RuntimeException(String.format("Regular property tile rent could not be calculated for %s with number of buildings owned %d.", regularPropertyTile, numberOfOwnedBuildings));
    }

    public int getNumberOfOwnedSameColorProperties(Player player, PropertyColor propertyColor) {
        return (int) player.getOwnedPropertyTiles().stream()
                .map(PropertyTile::getPropertyColor)
                .filter(color -> color.equals(propertyColor))
                .count();
    }

    private boolean propertyOwnerHasAllSameColorProperties(int numberOfOwnedSameColorProperties, PropertyColor propertyColor) {
        if (numberOfOwnedSameColorProperties == 2 && (propertyColor.equals(PropertyColor.BROWN) || propertyColor.equals(PropertyColor.DARK_BLUE) || propertyColor.equals(PropertyColor.UTILITY))) {
            return true;
        }
        if (numberOfOwnedSameColorProperties == 4 && propertyColor.equals(PropertyColor.RAILROAD)) {
            return true;
        }
        if (numberOfOwnedSameColorProperties == 3 && !propertyColor.equals(PropertyColor.RAILROAD)) {
            return true;
        }
        return false;
    }

    public double getMaintenanceCostForProperty(double maintenancePerHouse, double maintenancePerHotel) {
        Player player = game.getCurrentTurnPlayer();
        double maintenanceCostTotal = 0;
        List<Integer> hotelsAndHousesList = player.getOwnedPropertyTiles().stream()
                .filter(propertyTile -> propertyTile instanceof  RegularPropertyTile)
                .map(regularPropertyTile -> ((RegularPropertyTile) regularPropertyTile).getBuildingsCount())
                .filter(buildingsOnProperty -> buildingsOnProperty > 0)
                .collect(Collectors.toList());
        for (Integer buildingsCountOnProperty: hotelsAndHousesList) {
            if (buildingsCountOnProperty == 5) {
                maintenanceCostTotal += maintenancePerHotel;
            } else {
                maintenanceCostTotal += maintenancePerHouse * buildingsCountOnProperty;
            }
        }
        return maintenanceCostTotal;
    }

    public boolean playerHasAllSameColorProperties(Player player, PropertyTile propertyTile) {
        if (propertyTile.getOwner() == null || !propertyTile.getOwner().equals(player)) return false;
        return propertyOwnerHasAllSameColorProperties(getNumberOfOwnedSameColorProperties(player, propertyTile.getPropertyColor()), propertyTile.getPropertyColor());
    }

    public double calculateHighestRentOnBoard() {
        double highestRent = 0;
        List<PropertyTile> ownedProperties = game.getBoard().getTiles().stream().filter( tile -> tile instanceof PropertyTile).map(tile -> (PropertyTile) tile).filter(propertyTile -> propertyTile.getOwner() != null).collect(Collectors.toList());

        for (PropertyTile ownedProperty : ownedProperties) {
            Dice diceForUtilityCalculation = new Dice();
            diceForUtilityCalculation.setDie1(3);
            diceForUtilityCalculation.setDie1(3);
            double rentAmountForOwnedProperty = getRentAmountForProperty(ownedProperty, diceForUtilityCalculation);
            if (rentAmountForOwnedProperty > highestRent) {
                highestRent = rentAmountForOwnedProperty;
            }
        }
        return highestRent;
    }

}
