package ee.taltech.iti0200.monopoly.game.logic.game;

import ee.taltech.iti0200.monopoly.game.player.Player;

public class MoneyService {

    protected boolean playerHasEnoughMoney(Player player, double amountNeeded) {
        return player.getBalance() >= amountNeeded;
    }

    protected void balanceDeductAmount(Player player, double amountToDeduct) {
        player.setBalance(player.getBalance() - amountToDeduct);
    }
    protected void balanceAddAmount(Player player, double amountToAdd) {
        player.setBalance(player.getBalance() + amountToAdd);
    }
}
