package ee.taltech.iti0200.monopoly.game.logic.gameenigne;

import ee.taltech.iti0200.monopoly.game.board.tile.PropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.RegularPropertyTile;
import ee.taltech.iti0200.monopoly.game.board.tile.Tile;
import ee.taltech.iti0200.monopoly.game.card.Card;
import ee.taltech.iti0200.monopoly.game.card.CardType;
import ee.taltech.iti0200.monopoly.game.dice.Dice;
import ee.taltech.iti0200.monopoly.game.gamedataloader.CardLoader;
import ee.taltech.iti0200.monopoly.game.gamedataloader.TileLoader;
import ee.taltech.iti0200.monopoly.game.logic.game.Game;
import ee.taltech.iti0200.monopoly.game.player.Player;
import ee.taltech.iti0200.monopoly.server.dto.DTOPlayerReadyObject;
import ee.taltech.iti0200.monopoly.server.dto.DTOGameDTOHandler;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.fromserver.DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.fromserver.DTOafsCardOfferSwitchACardOrPay;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.fromserver.DTOafsCardTakeACard;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver.DTOatsCardChoice;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.card.toserver.DTOatsCardUseGetOutOfJailCard;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.die.fromserver.DTOafsDieDiceHasBeenRolled;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.die.toserver.DTOatsDieRollTheDice;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.fromserver.*;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.toserver.DTOatsGameStatePlayerBankruptcyAcknowledgement;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.gamestate.toserver.DTOatsGameStatePlayerEndTurn;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.fromserver.DTOafsMoneyGetMoneyFromBank;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.fromserver.DTOafsMoneyPayMoneyToBank;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.fromserver.DTOafsMoneyTransferMoneyBetweenPlayers;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.money.toserver.DTOatsMoneyPayToGetOutOfJail;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.moving.fromserver.DTOafsMovingMovePlayerToTile;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.fromserver.*;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver.DTOatsPropertyPurchaseDecision;
import ee.taltech.iti0200.monopoly.server.dto.gameplayaction.property.toserver.DTOatsPropertyUpDowngradeDecision;
import ee.taltech.iti0200.monopoly.server.packets.Packets;
import ee.taltech.iti0200.monopoly.server.serv.MpServer;
import lombok.Data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Data
public class GameNetworkingServerLogic {

    private Game game;
    private List<DTOPlayerReadyObject> dtoPlayerReadyObjects = new ArrayList<>();
    private List<Player> players = new ArrayList<>();
    private MpServer mpServer;
    private DTOGameDTOHandler dtoGameDTOHandler;
    private String lobbyId;

    public GameNetworkingServerLogic(MpServer mpServer, String lobbyId) {
        this.mpServer = mpServer;
        this.lobbyId = lobbyId;
        this.dtoGameDTOHandler = new DTOGameDTOHandler(this);

        this.game = new Game(this);
    }


    // Called by the ServerNetworkListener when any client changes their ready-status. Broadcasts new players list and ready-statuses to all connected players.
    public void receivePlayerReadyStatus(int connectionId, String playerReadyObjectDTOString) {
        DTOPlayerReadyObject dTOPlayerReadyObject = null;
        try {
            dTOPlayerReadyObject = mpServer.objectMapper.readValue(playerReadyObjectDTOString, DTOPlayerReadyObject.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DTOPlayerReadyObject finalDTOPlayerReadyObject = dTOPlayerReadyObject;
        Player player = players.stream().filter(gamePlayer -> gamePlayer.getConnectionId() == connectionId || gamePlayer.getPlayerId().equals(finalDTOPlayerReadyObject.getPlayerId())).findAny().get();
        dtoPlayerReadyObjects.stream().filter(pRo -> pRo.getPlayerId().equals(player.getPlayerId())).findAny().get().setReadyStatus(dTOPlayerReadyObject.isReadyStatus());
        broadcastPlayerReadyStatuses();
        if (dtoPlayerReadyObjects.stream().allMatch(DTOPlayerReadyObject::isReadyStatus)) {
            game.setGameInProgress(true);
            game.initializeGame(players);
            // Send tiles etc here.
            gatherGameDataAndSendToServer();
            try {
                TimeUnit.MILLISECONDS.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // Should also start the game here.
            game.startGame();
        }
    }

    // Called by this to broadcast new players list and ready-statuses.
    public void broadcastPlayerReadyStatuses() {
        dtoGameDTOHandler.sendDTOPlayerReadyObject(dtoPlayerReadyObjects);
    }

    // Called to announce game start and tell the clients to change the scene.
    public void broadcastGameStart() {
        Packets.PacketServerSendGameStart packet = new Packets.PacketServerSendGameStart();
        packet.setGameStatus("gip");
        packet.setLobbyId(lobbyId);
        for (Player player : players) {
            mpServer.getServer().sendToTCP(player.getConnectionId(), packet);
        }
        //mpServer.getServer().sendToAllTCP(packet);
    }

    // Sends a playerID to a particular client based on their connection ID;
    public void sendAPlayerIdToTheConnectedClient(int connectionId, String playerId) {
        Packets.PacketRegisterPlayerFromServer packetRegisterPlayerFromServer = new Packets.PacketRegisterPlayerFromServer();
        packetRegisterPlayerFromServer.setPlayerId(playerId);
        packetRegisterPlayerFromServer.setLobbyId(lobbyId);
        mpServer.getServer().sendToTCP(connectionId, packetRegisterPlayerFromServer);
    }

    private void gatherGameDataAndSendToServer() {
        String playersString = getPlayersAsJsonString();
        String tilesString = TileLoader.getTileStringFromFiles();
        String cardsChanceString = CardLoader.getCardsStringFromFiles(CardType.CHANCE);
        String cardsCommunityChestString = CardLoader.getCardsStringFromFiles(CardType.COMMUNITY_CHEST);

        dtoGameDTOHandler.sendDTOGameDataPacket(Player.class.getSimpleName(), playersString);
        dtoGameDTOHandler.sendDTOGameDataPacket(Tile.class.getSimpleName(), tilesString);
        dtoGameDTOHandler.sendDTOGameDataPacket(Card.class.getSimpleName() + "Chance", cardsChanceString);
        dtoGameDTOHandler.sendDTOGameDataPacket(Card.class.getSimpleName() + "CommunityChest", cardsCommunityChestString);
    }

    private String getPlayersAsJsonString() {
        try {
            return mpServer.objectMapper.writeValueAsString(players);
        } catch (IOException e) {
            throw new RuntimeException("There was an error trying to map players into a json string: " + e);
        }
    }

    public void playerDisconnected(int connectionId) {
        Player disconnectedPlayer = players.stream().filter(player -> player.getConnectionId() == connectionId).findAny().orElse(null);
        if (game.isGameInProgress() && disconnectedPlayer != null) {
            game.playerDisconnected(disconnectedPlayer);
        } else {
            dtoPlayerReadyObjects.remove(dtoPlayerReadyObjects.stream().filter(dtoPlayerReadyObject -> dtoPlayerReadyObject.getPlayerId().equals(disconnectedPlayer.getPlayerId())).findAny().get());
            players.remove(disconnectedPlayer);
            broadcastPlayerReadyStatuses();
        }
    }

    public boolean getGameGameInProgress() {
        return game.isGameInProgress();
    }

    public void endGameGameInProgress() {
        game.setGameInProgress(false);
    }


    // Real gameplay action thingies

    private Player findPlayerById(String playerId) {
        return players.stream().filter(player -> player.getPlayerId().equals(playerId)).findAny().orElse(null);
    }
    private PropertyTile findPropertyTileByIndex(int propertyIndex) {
        return (PropertyTile) game.getBoard().getTiles().stream().filter(tile -> tile instanceof PropertyTile).filter(tile -> tile.getPosition() == propertyIndex).findAny().orElse(null);
    }
    private boolean isSendingClientForbiddenToSendGivenDTO(String playerIdFromDTO, int connectionId) {
        if (findPlayerById(playerIdFromDTO) == null) return false;
        return findPlayerById(playerIdFromDTO).getConnectionId() != connectionId;
    }


    // =========  Game --> GameNetworkingServerLogic --> DTOGameDTOHandler

    public void sendCardSetPlayerOwnedGetOutOfJailCardsCount(Player player) {
        DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount dto = new DTOafsCardSetPlayerOwnedGetOutOfJailCardsCount();
        dto.setPlayerId(player.getPlayerId());
        dto.setGetOutOfJailCardsCount(player.getOwnedCards().size());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), player.getConnectionId(), false);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void sendCardSwitchACardOrPay(Player player, CardType currentCardType, CardType anotherCardType, double amountToPay) {
        DTOafsCardOfferSwitchACardOrPay dto = new DTOafsCardOfferSwitchACardOrPay();
        dto.setPlayerId(player.getPlayerId());
        dto.setCurrentCardType(currentCardType);
        dto.setAnotherCardType(anotherCardType);
        dto.setAmountToPay(amountToPay);
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), player.getConnectionId(), false);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastCardTakeACard(Player player, Card card) {
        DTOafsCardTakeACard dto = new DTOafsCardTakeACard();
        dto.setPlayerId(player.getPlayerId());
        dto.setCardType(card.getType());
        dto.setCardIndex(card.getCardIndex());

        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastDieDiceHasBeenRolled(Dice dice) {
        DTOafsDieDiceHasBeenRolled dto = new DTOafsDieDiceHasBeenRolled();
        dto.setDie1Dots(dice.getDie1());
        dto.setDie2Dots(dice.getDie2());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void sendGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn(Player player, boolean allowRollingTheDice, boolean allowPaying, boolean allowUsingACard) {
        DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn dto = new DTOafsGameStateForceChooseActionToGetOutOfJailAtTheBeginningOfTheTurn();
        dto.setPlayerId(player.getPlayerId());
        dto.setAllowRollingTheDice(allowRollingTheDice);
        dto.setAllowPaying(allowPaying);
        dto.setAllowUsingACard(allowUsingACard);
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), player.getConnectionId(), false);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void sendGameStateWarnBankruptcy(Player player) {
        DTOafsGameStateWarnBankruptcy dto = new DTOafsGameStateWarnBankruptcy();
        dto.setPlayerId(player.getPlayerId());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), player.getConnectionId(), false);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastGameStatePlayerWentBankrupt(Player player) {
        DTOafsGameStatePlayerWentBankrupt dto = new DTOafsGameStatePlayerWentBankrupt();
        dto.setPlayerId(player.getPlayerId());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastGameStateSetPlayerTurn(Player player) {
        DTOafsGameStateSetPlayerTurn dto = new DTOafsGameStateSetPlayerTurn();
        dto.setPlayerId(player.getPlayerId());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastGameStateWinner(Player player) {
        DTOafsGameStateWinner dto = new DTOafsGameStateWinner();
        dto.setPlayerId(player.getPlayerId());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastMoneyGetMoneyFromBank(Player player, double receivedAmount) {
        DTOafsMoneyGetMoneyFromBank dto = new DTOafsMoneyGetMoneyFromBank();
        dto.setPlayerId(player.getPlayerId());
        dto.setReceivedAmount(receivedAmount);
        dto.setPlayerNewBalance(player.getBalance());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastMoneyPayMoneyToBank(Player player, double paidAmount) {
        DTOafsMoneyPayMoneyToBank dto = new DTOafsMoneyPayMoneyToBank();
        dto.setPlayerId(player.getPlayerId());
        dto.setPaidAmount(paidAmount);
        dto.setPlayerNewBalance(player.getBalance());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastMoneyTransferMoneyBetweenPlayers(Player payer, Player payee, double amount) {
        DTOafsMoneyTransferMoneyBetweenPlayers dto = new DTOafsMoneyTransferMoneyBetweenPlayers();
        dto.setPayerPlayerId(payer.getPlayerId());
        dto.setPayerPlayerNewBalance(payer.getBalance());
        dto.setPayeePlayerId(payee.getPlayerId());
        dto.setPayeePlayerNewBalance(payee.getBalance());
        dto.setTransferredAmount(amount);
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastMovingMovePlayerToTile(Player player, int movePlayerToTileIndex, boolean toJail, boolean moveBackwards) {
        DTOafsMovingMovePlayerToTile dto = new DTOafsMovingMovePlayerToTile();
        dto.setPlayerId(player.getPlayerId());
        dto.setMoveBackwards(moveBackwards);
        dto.setToJail(toJail);
        dto.setTileIndex(movePlayerToTileIndex);
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void sendPropertyOfferToBuy(Player player, PropertyTile propertyTile) {
        DTOafsPropertyOfferToBuy dto = new DTOafsPropertyOfferToBuy();
        dto.setPlayerId(player.getPlayerId());
        dto.setPropertyIndex(propertyTile.getPosition());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), player.getConnectionId(), false);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastPropertyRemovePropertyOwnership(Player player, List<PropertyTile> propertyTilesToRemove) {
        DTOafsPropertyRemovePropertyOwnership dto = new DTOafsPropertyRemovePropertyOwnership();
        dto.setPlayerId(player.getPlayerId());
        dto.setPropertyIndices(propertyTilesToRemove.stream().mapToInt(Tile::getPosition).boxed().collect(Collectors.toList()));
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastPropertySetPropertyMortgagedStatus(PropertyTile propertyTile) {
        DTOafsPropertySetPropertyMortgagedStatus dto = new DTOafsPropertySetPropertyMortgagedStatus();
        dto.setPropertyIndex(propertyTile.getPosition());
        dto.setMortgagedStatus(propertyTile.isMortgaged());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastPropertySetPropertyOwnership(Player player, PropertyTile propertyTile) {
        DTOafsPropertySetPropertyOwnership dto = new DTOafsPropertySetPropertyOwnership();
        dto.setPlayerId(player.getPlayerId());
        dto.setPropertyIndex(propertyTile.getPosition());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }

    public void broadcastPropertyUpDowngradeProperty(RegularPropertyTile regularPropertyTile) {
        DTOafsPropertyUpDowngradeProperty dto = new DTOafsPropertyUpDowngradeProperty();
        dto.setPropertyIndex(regularPropertyTile.getPosition());
        dto.setUpDowngradeToLevel(regularPropertyTile.getBuildingsCount());
        try {
            dtoGameDTOHandler.sendDTOGameplayPacket(dto.getClass().getSimpleName(), mpServer.objectMapper.writeValueAsString(dto), 0, true);
        } catch (IOException e) {
            throw new RuntimeException(String.format("There was an error trying to map %s to json. %s", dto.getClass().getSimpleName(), e));
        }
    }






    // =========  DTOGameDTOHandler --> GameNetworkingServerLogic --> Game
    // Called by DTOHandler when server receives these gameplay action dto-s from a client.

    public void receiveCardChoice(DTOatsCardChoice dto, int connectionId) {
        if (isSendingClientForbiddenToSendGivenDTO(dto.getPlayerId(), connectionId)) return;
        game.receiveCardChoice(findPlayerById(dto.getPlayerId()), dto.isPay());
    }

    public void receiveCardUseGetOutOfJailCard(DTOatsCardUseGetOutOfJailCard dto, int connectionId) {
        if (isSendingClientForbiddenToSendGivenDTO(dto.getPlayerId(), connectionId)) return;
        game.receiveCardUseGetOutOfJailCard(findPlayerById(dto.getPlayerId()), dto.isUseCard());
    }

    public void receiveDieRollTheDice(DTOatsDieRollTheDice dto, int connectionId) {
        if (isSendingClientForbiddenToSendGivenDTO(dto.getPlayerId(), connectionId)) return;
        game.rollDice(findPlayerById(dto.getPlayerId()));
    }

    public void receiveGameStatePlayerBankruptcyAcknowledgement(DTOatsGameStatePlayerBankruptcyAcknowledgement dto, int connectionId) {
        if (isSendingClientForbiddenToSendGivenDTO(dto.getPlayerId(), connectionId)) return;
        game.playerDeclareBankruptcy(findPlayerById(dto.getPlayerId()));
    }

    public void receiveGameStatePlayerEndTurn(DTOatsGameStatePlayerEndTurn dto, int connectionId) {
        if (isSendingClientForbiddenToSendGivenDTO(dto.getPlayerId(), connectionId)) return;
        game.endTurn(findPlayerById(dto.getPlayerId()), false);
    }

    public void receiveMoneyPayToGetOutOfJail(DTOatsMoneyPayToGetOutOfJail dto, int connectionId) {
        if (isSendingClientForbiddenToSendGivenDTO(dto.getPlayerId(), connectionId)) return;
        game.receiveMoneyPayToGetOutOfJail(findPlayerById(dto.getPlayerId()));
    }

    public void receivePropertyPurchaseDecision(DTOatsPropertyPurchaseDecision dto, int connectionId) {
        if (isSendingClientForbiddenToSendGivenDTO(dto.getPlayerId(), connectionId)) return;
        game.receivePurchaseDecision(findPlayerById(dto.getPlayerId()), findPropertyTileByIndex(game.getCurrentTurnPlayer().getPosition()), dto.isConfirmPurchase());
    }

    public void receivePropertyUpDowngradeDecision(DTOatsPropertyUpDowngradeDecision dto, int connectionId) {
        if (isSendingClientForbiddenToSendGivenDTO(dto.getPlayerId(), connectionId)) return;
        PropertyTile propertyTile = findPropertyTileByIndex(dto.getPropertyIndex());
        game.receivePropertyUpDowngradeDecision(findPlayerById(dto.getPlayerId()), propertyTile, dto.isUpgrade());
    }



}
