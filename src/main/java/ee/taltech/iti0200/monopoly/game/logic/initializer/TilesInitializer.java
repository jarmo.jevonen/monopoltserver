package ee.taltech.iti0200.monopoly.game.logic.initializer;

import ee.taltech.iti0200.monopoly.game.board.tile.*;
import ee.taltech.iti0200.monopoly.game.gamedataloader.TileLoader;

import java.util.ArrayList;
import java.util.List;

public class TilesInitializer {

    public static List<Tile> initializeAndGetTiles() {
        return TileLoader.loadTilesFromFiles();
    }
}
