package ee.taltech.iti0200.monopoly.game.board.tile;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TaxTile extends Tile {

    private double taxAmount;

    @Override
    public String toString() {
        return String.format("%d) %s", getPosition(), getTileName());
    }

}
