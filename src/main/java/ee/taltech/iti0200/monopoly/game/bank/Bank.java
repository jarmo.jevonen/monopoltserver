package ee.taltech.iti0200.monopoly.game.bank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bank {
    private int totalHousesCount;
    private int totalHotelsCount;

    private int availableHousesCount;
    private int availableHotelsCount;
}
